/*
 * main_mesoplastic.cu
 *
 *  started on: May 10, 2013
 *      Author: eze
 * .....based on: main.cu by Kirsten
 */

// Hardware parameters for Tesla c2075 (GF100) cc 2.0
#define WARP_SIZE 32
#define THREADS_PER_BLOCK 1024
#define BLOCKS_PER_GRID 65535
#define SHARED_PER_BLOCK 49152
//#ifndef CUDA_DEVICE
//#define CUDA_DEVICE 0
//#endif

//TODO: Tunne this to optimize
#define TILE_X 32
#define TILE_Y 4
#define TILE 128
#define TILE_X3D 8
#define TILE_Y3D 4
#define TILE_Z 32
#define TILEB 1024

//TODO: defines in Makefile
#ifndef LX
#define LX 256
#endif
#ifndef LY
#define LY LX 
#endif
//TODO: check for rectangular geometries //DONE, seems that it works //not so sure for Sq

#define NN (LX*LY)
#define SCALE (1.0/NN)

#ifndef TIME_STEP
#define TIME_STEP 0.01
#endif

#ifndef STRESS_THRESHOLD
#define STRESS_THRESHOLD 1.0
#endif

#ifndef PLASTIC_STRAIN
#define PLASTIC_STRAIN 1.0
#endif

#ifndef TAU_ELAST
#define TAU_ELAST 1.0
#endif

#ifndef TAU_PLAST
#define TAU_PLAST 1.0
#endif

// Functions
#define MAX(a,b) (((a)<(b))?(b):(a))	// maximum
#define MIN(a,b) (((a)<(b))?(a):(b))	// minimum
//#define ABS(a) (((a)<0)?(-a):(a))	// absolute value

#ifdef TRACERS
	#ifndef NTW
	#define NTW 4096
	#endif
	#ifndef DISPLFIELDMAXCOUNT
	#define DISPLFIELDMAXCOUNT 5000000 // This is to prevent PofU file to grow much avobe a GB
	#endif
	#ifndef NTRACERS
	#define NTRACERS MIN(LX*LY,8192)
	#endif
#endif

#ifdef SQ
	//Maximum Q different values for sizes Lx=Ly=L are: {128,5839}
	#define NQXY 64 //96 //16

	#ifdef ALLQ
		#define QTILE_X TILE_X
		#define QTILE_Y TILE_Y
		#define QNX LX
		#define QNY LY
	#else
		#define QTILE_X 16
		#define QTILE_Y 8
		#define QNX NQXY
		#define QNY NQXY
	#endif
	#define QSMAX (2*(QNX-1)*(QNY-1))
	#define QNN (QNX*QNY)
#endif

#ifndef DATA_SIZE
#define DATA_SIZE 131072
#endif

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

extern void init_genrand(unsigned long s);
extern double genrand_real3(void);

#include "mesoplastic_library.cuh"
#include "../common/timers.hpp"

bool logtimes(unsigned long int t, unsigned int logincrement){
//	bool a = ( (t % logincrement == 0) || ((t) % (logincrement/2) == 0 ) || \
			((t) % (logincrement/3) == 0) || ((t) % (logincrement/4) == 0 ) || \
			((t) % (logincrement/6) == 0 ) || ((t) % (logincrement/8) == 0 ) );
	bool a = ( (t % logincrement == 0) || ((t) % (logincrement/2) == 0 ) || \
			((t) % (logincrement/4) == 0 ) || ((t) % (logincrement/8) == 0 ) );
	return a;
}

REAL TimeLoop(mesoplastic_model &T, const REAL temperature, const REAL gamma_p, const unsigned long int t_transient, \
	const unsigned long int t_transient2, const unsigned long int t_waiting, const unsigned long int t_run, const unsigned long int t_data){

	assert(t_waiting%t_data==0);
	assert(t_run%t_data==0);
	assert(t_run/t_data<=DATA_SIZE);

	//Transients: first with shear rate, then without
	for(unsigned long int i=0; i<t_transient; i++){
		#ifdef ACTIVATED_DYNAMICS
		T.UpdateStateVariablesActivated(i, temperature);
		#else
		T.UpdateStateVariables(i);
		#endif
		T.ComputePlasticStrain();
		T.TransformToFourierSpace();
		T.Convolution();
		T.AntitransformFromFourierSpace();
		T.EulerIntegrationStep(gamma_p);
	}

	for(unsigned long int i=0; i<t_transient2; i++){
		#ifdef ACTIVATED_DYNAMICS
		T.UpdateStateVariablesActivated(i, temperature);
		#else
		T.UpdateStateVariables(i);
		#endif
		T.ComputePlasticStrain();
		T.TransformToFourierSpace();
		T.Convolution();
		T.AntitransformFromFourierSpace();
		T.EulerIntegrationStep(0.0);
	}

	//Start of main time loop with measurements
	unsigned long int t=0;
	double time=0;
	unsigned int logincrement=10;
	unsigned long int count=0;
	double ssstress=0;
	#ifdef GLOBAL_WAITING_TIMES
	unsigned int t_aux=0;
	unsigned int t_counter=0;
	#endif
	#ifdef TRACERS
	unsigned int tw_counter=0;
	#endif

	while(t<t_run+1){

	//Measurements
		if(t==0){
			cout << "## 1:time 2:stress 4:activity 3:steps" << '\n';
		}

		//Average stress and activity, raw-data and visualization
		#ifdef LOGARITHMIC_SPACING
		if(logtimes(t,logincrement)){
		#else
		if(t%t_data==0){
		#endif
			//Calculate and print instantaneous average stress and activity
			REAL average_stress = T.ComputeSsstress();
			REAL average_activity = T.ComputeActivity();
			ssstress+= average_stress;
			count++;
			cout << setprecision(15) << std::scientific << (time*TIME_STEP) << std::fixed \
			<< " " << average_stress << " " << average_activity << " "  << t << '\n';
		}
		if(t % (logincrement*10) == 0){
			#ifdef VISUALIZATION
			#warning VISUALIZATION ON HERE
			T.Visualization(t);
			#endif

			//DEBUG: T.PrintRawLocalStressAndState(t, time); //WARNING: this prints in cout
			#ifdef RAW_DATA
			T.PrintSigmaAndState(t);
			#endif

			#ifdef WAITING_TIMES
			T.PrintPofTW(t); //Includes cudaMemCpy
			#endif
		}

		//Tracers related quantities, MSD and Structure Factor
		#ifdef TRACERS
		if( (t%t_waiting==0) && (tw_counter<NTW) ){
			T.SetTracersAtTW(tw_counter);
			tw_counter++;
		}

		if( (t%t_data==0) && (tw_counter-1<NTW)){
			T.CalculateAccumulateMeanSquareDisplacement(t/t_data,t/t_waiting,t_waiting/t_data);
			#ifdef SQ
			T.CalculateAccumulateTrajectoriesStructureFactor(t/t_data,t/t_waiting,t_waiting/t_data);
			#endif

			#ifdef PRINT_TRACERS
			T.PrintTracers(t,64);
			#endif
			//T.CalculatePrintTrajectoriesStructureFactor(file1, t, t_waiting);

			if(logtimes(t,logincrement)){
				T.PrintMeanSquareDisplacement(0, t_data, (unsigned long int)(t/t_data)+1);
				#ifdef SQ
				T.PrintTrajectoriesStructureFactor_vs_Q(0, t_data, (unsigned long int)(t/t_data)+1);
				T.PrintTrajectoriesStructureFactor_vs_t(0, t_data, (unsigned long int)(t/t_data)+1);
				#endif
				#ifdef RAW_DATA
				T.PrintDsigmaAndDu(t); //Includes Copy
				#endif
			}
		}
		#endif
		if (t == logincrement*10) logincrement*=10;

		#ifdef WAITING_TIMES
			T.ComputeWaitingTimeDistribution(t);
		#endif
		#ifdef GLOBAL_WAITING_TIMES
			T.ComputePrintGlobalWaitingTimeDistribution(t, t_counter, t_aux);
		#endif

//Dynamics
		#ifdef ACTIVATED_DYNAMICS
			#ifdef KMC
			int firstplasticactive = T.CheckForPlasticActivity(); 
			// It will return the position of the first 1 or the size of the array (NN) if there are none
			if(firstplasticactive==NN){
				//cout << "### KMC move, at time" << t*TIME_STEP << '\n';
				time+=(T.ConstructActivatedProbabilityTowerChooseOneEventAndUpdate(temperature))/TIME_STEP;
				t = floor(time); //t++;
			}else{
				//cout << "### Normal setp, at time" << t << '\n';
				T.UpdateStateVariablesActivated(t, temperature);
				time+=1;
				t++;
			}
			#else
				//cout << "### Normal setp, at time" << t << '\n';
				T.UpdateStateVariablesActivated(t, temperature);
				time+=1;
				t++;
			#endif
		#else
			// Update n(x,z;t) state variables: 0/1 corresponds to elastic/plastic
			T.UpdateStateVariables(t);
			time+=1;
			t++;
		#endif

		// Calculation of the plastic strain "epsilon_dot" ~ n*sigma
		T.ComputePlasticStrain();
		// Calculation of the convolution with the propagator and the plastic strain
		// we transform the plastic strain epsilon_dot
		T.TransformToFourierSpace();
		//convolution
		T.Convolution();
		#ifdef TRACERS
		//displacement field u
		T.CalculateDisplacementField();
		#endif
		// we antitransform the result of the convolution "delta_sigma" (and eventually delta_u_x/y)
		T.AntitransformFromFourierSpace();
		// Update of the shear stress "sigma" with a simple Euler integration using delta_sigma and a \
		global elastic loading, the shear rate "gamma_dot" 
		#ifdef LANGEVIN
			T.EulerIntegrationStepTemp(0.0, temperature, t);
		#else
			T.EulerIntegrationStep(0.0);
		#endif
		#ifdef TRACERS
		//Update tracers positions preserving PBC
		T.UpdateTracers();
		#endif
	}
	cout << " " << endl;

//Last printings
	#ifdef TRACERS
	T.PrintMeanSquareDisplacement(1, t_data, (unsigned long int)(t_run/t_data)+1);
	#ifdef SQ
	T.PrintTrajectoriesStructureFactor_vs_Q(1, t_data, (unsigned long int)(t_run/t_data)+1);
	T.PrintTrajectoriesStructureFactor_vs_t(1, t_data, (unsigned long int)(t_run/t_data)+1);
	#endif
	#endif

	return ssstress/(REAL) count;
}

// main routine
int main(int argc, char **argv){

	/*------------INPUT----------------*/
	if(argc < 8) {
		cerr << "\n\n\t\tUsage: " << argv[0] << " TEMPERATURE SHEAR_RATE TRANSIENT_SHEARED TRANSIENT_UNSHEARED WAITING_T RUN_TIME DATA_DTIME [GPU_ID] [SEED_RANDOM] [SEED_PHILOX] [stored_local_stresses (name with quotes)] [stored_local_states (name with quotes)] \n\n";
		return 1;
	}

	const REAL TEMPERATURE = atof(argv[1]);
	const REAL SHEAR_RATE = atof(argv[2]);
	const unsigned int TRANSIENT_TIME = atoi(argv[3]);
	const unsigned int TRANSIENT_TIME2 = atoi(argv[4]);
	const unsigned int WAITING_T = atoi(argv[5]);
	const unsigned int RUN_TIME = atoi(argv[6]);
	const float DATA_DTIME = atof(argv[7]);
	int GPU_ID = 0;
	if(argc > 8) GPU_ID = atoi(argv[8]);
	// Set the GPGPU computing device
	#ifdef CUDA_DEVICE
	CUDA_SAFE_CALL(cudaSetDevice(CUDA_DEVICE));
	#else
	CUDA_SAFE_CALL(cudaSetDevice(GPU_ID));
	#endif
	unsigned int RANDOM_SEED = ((unsigned int) time(NULL));
	if(argc > 9) RANDOM_SEED = atoi(argv[9]);
	unsigned int h_philox_seed = ((unsigned int) time(NULL)); //PHILOX_SEED;
	if(argc > 10) h_philox_seed = atoi(argv[10]);
	CUDA_SAFE_CALL(cudaMemcpyToSymbol(philox_seed, &h_philox_seed, sizeof(unsigned int), 0, cudaMemcpyHostToDevice));

	ifstream stored_local_stresses;
	if(argc > 11) stored_local_stresses.open(argv[11]);
	ifstream stored_local_states;
	if(argc > 12) stored_local_states.open(argv[12]);


	//TODO: complete asserts
	assert(TILE_X%2==0);
	assert(TILE_Y%2==0);
	assert(LX%TILE_X==0);
	assert(LY%TILE_Y==0);
	assert(TEMPERATURE>0);
	//We've assumed a small temporal step in the integration scheme
	assert(TIME_STEP<=0.1);
	assert(DATA_DTIME/TIME_STEP>=1);
	#ifdef TRACERS
	assert(NN%NTRACERS==0);
	assert(NTRACERS%TILE==0);
	assert(NTRACERS%TILE_Z==0);
	assert(NTRACERS%(TILE_X*TILE_Y)==0);
	assert(LX==LY); //TODO: Solve this. The thing is the definiton of the q-squares and their ordering.
	#endif

	int datasize = int(RUN_TIME/(float)DATA_DTIME+1e-3)+1;
	int datawindows = int(RUN_TIME/(float)WAITING_T+1e-3)+1;

	assert(datasize<=DATA_SIZE); // &&"You're asking too big data arrays, change DATA_SIZE and compile again");
	#ifdef TRACERS
	assert(datawindows<=NTW); // &&"You're asking too big average windows, change NTW and compile again");
	#endif
	assert((WAITING_T*10)%10 == 0); // &&"Sorry, tw should be an integer"); //TODO: get rid of this

	// Choosing less "shared memory", more cache.
	//CUDA_SAFE_CALL(cudaThreadSetCacheConfig(cudaFuncCachePreferL1)); 

	#ifdef ACTIVATED_DYNAMICS
		//Initialize random(), it will be used for the KMC Poissonian time
		init_genrand(RANDOM_SEED);
	#endif

	std::setprecision(15);

	//Print simulation parameters
	cout << "### LX ................................: " << LX                << '\n';
	cout << "### LY ................................: " << LY                << '\n';
	cout << "### Time Step..........................: " << TIME_STEP         << '\n';
	cout << "### Stress Thershold...................: " << STRESS_THRESHOLD  << '\n';
	cout << "### Recovery rate unit (tau_elast).....: " << TAU_ELAST         << '\n';
	cout << "### Activation rate unit (tau_plast)...: " << TAU_PLAST         << '\n';
	cout << "### Constant plastic strain per event..: " << PLASTIC_STRAIN    << '\n';
	cout << "### Shear rate ........................: " << SHEAR_RATE        << '\n';
	cout << "### Transient time sheared.............: " << TRANSIENT_TIME    << '\n';
	cout << "### Transient time unsheared...........: " << TRANSIENT_TIME2   << '\n';
	cout << "### Waiting time ......................: " << WAITING_T         << '\n';
	cout << "### Running time ......................: " << RUN_TIME          << '\n';
	cout << "### Data aquiring time.................: " << DATA_DTIME        << '\n';
	cout << "### SEED Philox .......................: " << h_philox_seed     << '\n';
	#ifdef ACTIVATED_DYNAMICS
	cout << "### SEED MT............................: " << RANDOM_SEED       << '\n';
	cout << "### Effective temperature .............: " << TEMPERATURE       << '\n';
	#endif
	#ifdef RANDOM_EVENTS
	cout << "### RANDOM EVENTS: temp == activity....: " << '\n';
	#endif
	#ifdef DOUBLE_PRECISION
	cout << "### DOUBLE PRECISION...................: " << '\n';
	#else
	cout << "### SINGLE PRECISION...................: " << '\n';
	#endif
	#ifdef TRACERS
	cout << "### Number of TRACERS .................: " << NTRACERS << '\n';
	cout << "### Number of TW windows...............: " << datawindows << '\n';
	cout << "### Max Allowed Number of TW windows...: " << NTW << '\n';
	#endif
	cout << "### Size for data arrays...............: " << datasize << '\n';
	cout << "### Max Allowed size for data arrays ..: " << DATA_SIZE << '\n';
	cout.flush();

	// Our class
	mesoplastic_model T(datasize, datawindows);

	// Turn on cronometer
	cpu_timer clock;
	clock.tic();

	// Initialization
	//T.Initialize();
	T.InitializeOnDevice(RANDOM_SEED);
	#ifdef TRACERS
	T.InitializeTracersAndAll();
	#endif

	if(argc > 11) T.InitializeLocalStressesFromFile(stored_local_stresses);
	if(argc > 12) T.InitializeLocalStatesFromFile(stored_local_states);

	//Set integer times //HARDCODED, if chaged take care of h_msd.
	unsigned long int t_transient = (unsigned long int)(TRANSIENT_TIME/TIME_STEP+1e-6);
	unsigned long int t_transient2 = (unsigned long int)(TRANSIENT_TIME2/TIME_STEP+1e-6);
	unsigned long int t_waiting = (unsigned long int)(WAITING_T/TIME_STEP+1e-6);
	unsigned long int t_run = (unsigned long int)(RUN_TIME/TIME_STEP+1e-6);
	unsigned long int t_data = (unsigned long int)(DATA_DTIME/TIME_STEP+1e-6); //DATA_DTIME;

	//Declaring File Names
	T.DeclareFileNames(t_transient, t_transient2, t_waiting, t_run, t_data, TEMPERATURE);

	//Time loop call, returns averaged stress
	REAL sigma_av = TimeLoop(T, TEMPERATURE, SHEAR_RATE, t_transient, t_transient2, t_waiting, t_run, t_data);

	T.PrintLastStates(t_run);

	cout << "### final global stress " << sigma_av << '\n';
	cout << "### ------ CUDA MESO_PLASTIC MODEL------ " << '\n';
	// Turn off cronometer
	clock.tac(); //clock.print();
	REAL total_time = clock.cpu_elapsed();
	cout << "### Total time    : " << total_time/1000.  << " seg" << endl;

	return 0;
}

