
#ifdef DOUBLE_PRECISION
typedef double REAL;
typedef double2 REAL2;
typedef cufftDoubleComplex COMPLEX;
#else
typedef float REAL;
typedef float2 REAL2;
typedef cufftComplex COMPLEX;
#endif

/*--------RNG ROUTINES--------*/
__device__
REAL DeviceBoxMuller(const REAL u1, const REAL u2)
{
	REAL r = sqrt( -2.0*log(u1) );
	REAL theta = 2.0*M_PI*u2;
	return r*sin(theta);
}

//TODO: downgrade REAL to float here, who needs DP random numbers?!
__device__
void PhiloxRandomQuartet(const unsigned int index, const unsigned int time, float *r1, float *r2, float *r3, float *r4)
{
	RNG4 rng;
	RNG4::ctr_type c_pair={{}};
	RNG4::key_type k_pair={{}};
	RNG4::ctr_type r_quartet;

		// keys = threadid
		k_pair[0]= philox_seed; // eventually do: PHILOX_SEED + sample;
		// time counter
		c_pair[0]= time;
		c_pair[1]= index;
		// random number generation
		r_quartet = rng(c_pair, k_pair);

		*r1= u01_open_closed_32_53(r_quartet[0]);
		*r2= u01_open_closed_32_53(r_quartet[1]);
		*r3= u01_open_closed_32_53(r_quartet[2]);
		*r4= u01_open_closed_32_53(r_quartet[3]);
}

__device__
void PhiloxRandomPair(const unsigned int index, const unsigned int time, float *r1, float *r2)
{
	RNG2 rng;
	RNG2::ctr_type c_pair={{}};
	RNG2::key_type k_pair={{}};
	RNG2::ctr_type r_pair;

		// keys = threadid
		k_pair[0]= philox_seed; // eventually do: PHILOX_SEED + sample;
		// time counter
		c_pair[0]= time;
		c_pair[1]= index;
		// random number generation
		r_pair = rng(c_pair, k_pair);

		*r1= u01_open_closed_32_53(r_pair[0]);
		*r2= u01_open_closed_32_53(r_pair[1]);
}


//---------- DEVICE FUNCTIONS -----------//

//////////-----------------------Initialize Kernels----------------///////////////////////////

__global__ void Kernel_InitRandomGaussian(REAL* d_sigma, bool* d_state, REAL scale, unsigned int tt){
	int idx = blockIdx.x*blockDim.x+threadIdx.x;
	int idy = blockIdx.y*blockDim.y+threadIdx.y;
	if ( idx < LX && idy < LY){
		int index = idx*LY + idy;
		float r1, r2;
		PhiloxRandomPair(index, tt, &r1, &r2);
		d_sigma[index] = DeviceBoxMuller(r1, r2)/scale;
		d_state[index] = (abs(d_sigma[index])>1);
	}
}

__global__ void Kernel_InitRandomExponential(REAL* d_sigma_ran, unsigned int tt){
	int idx = blockIdx.x*blockDim.x+threadIdx.x;
	int idy = blockIdx.y*blockDim.y+threadIdx.y;
	if ( idx < LX && idy < LY){
		int index = idx*LY + idy;
		float r1, r2;
		PhiloxRandomPair(index, tt, &r1, &r2);
		d_sigma_ran[index] = -log(r1);
	}
}

__global__ void Kernel_SetPropagator(REAL *g, int nx, int ny){
	unsigned int i = blockIdx.x*blockDim.x+threadIdx.x;
	unsigned int j = blockIdx.y*blockDim.y+threadIdx.y;
	if ( i < nx && j < ny){
			int iprime = (i <= LX/2) ? i : i - LX;
			REAL qx=2.0*M_PI*iprime/(REAL)LX;
			REAL qy=2.0*M_PI*j/(REAL)LY;
			REAL q2=qx*qx+qy*qy;
			if(i!=0 || j!=0) g[i*ny+j] = -4*qx*qx*qy*qy/(q2*q2);
			if (i==0 && j==0) g[i*ny+j] = -1.0;
	}
}

#ifdef TRACERS
__global__ void Kernel_InitTracers(REAL2 *tracers, int ntracers){
	const unsigned int tid = blockIdx.x*blockDim.x+threadIdx.x;
	if (tid<ntracers){
		//float r1, r2;
		//PhiloxRandomPair(tid, 123, &r1, &r2);
		//tracers[tid].x = r1*LX;
		//tracers[tid].y = r2*LY;

		//FIXME:Attempt to have tracers homogeneously distributed in space
		int increment = NN/NTRACERS;
		tracers[tid].x = 1.*((tid*increment)%LX);
		tracers[tid].y = 1.*((tid*increment)/LX);

		//tracers[tid].x = (REAL)(tid%LX);
		//tracers[tid].y = (REAL)(tid/LX);
	}
}

__global__ void Kernel_SetOseenTensor(REAL2 *o, int nx, int ny){
	unsigned int i = blockIdx.x*blockDim.x+threadIdx.x;
	unsigned int j = blockIdx.y*blockDim.y+threadIdx.y;
	if ( i < nx && j < ny){
		int iprime = (i <= LX/2) ? i : i - LX;
		REAL qx=2.0*M_PI*iprime/(REAL)LX;
		REAL qy=2.0*M_PI*j/(REAL)LY;
		REAL q2=qx*qx+qy*qy;
		if(i!=0 || j!=0){
		o[i*ny+j].x = 2*qy*(1-2*qx*qx/q2)/q2;
		o[i*ny+j].y = 2*qx*(1-2*qy*qy/q2)/q2;
		}
		if (i==0 && j==0){
		o[i*ny+j].x = 0.0;
		o[i*ny+j].y = 0.0;
		}
	}
}

__global__ void Kernel_FillQSquares(unsigned int* map, int nx, int ny)
{
	unsigned int i = blockIdx.x*blockDim.x+threadIdx.x;
	unsigned int j = blockIdx.y*blockDim.y+threadIdx.y;
	if ( i < nx && j < ny){
		unsigned int tid = i*ny+j;
		map[tid]=(i*i)+(j*j);
	}
}
#endif

//////////-------------------------Update Kernels------------------///////////////////////////

__global__ void Kernel_UpdateStateVariables(const REAL* d_sigma, bool* d_state, const unsigned int time)
{
	int idx = blockIdx.x*blockDim.x+threadIdx.x;
	int idy = blockIdx.y*blockDim.y+threadIdx.y;
	if ( idx < LX/2 && idy < LY){
		int index = idx*LY + idy;
		int index1 = 2*index;
		int index2 = 2*index+1;
		const bool actualstate1 = d_state[index1];
		const bool actualstate2 = d_state[index2];
		float r1, r2;
		PhiloxRandomPair(index, time, &r1, &r2);
		d_state[index1]= ((actualstate1==0 && abs(d_sigma[index1])>=STRESS_THRESHOLD && r1<TIME_STEP/TAU_PLAST)|| \
		(actualstate1==1 && r1>TIME_STEP/TAU_ELAST));
		d_state[index2]= ((actualstate2==0 && abs(d_sigma[index2])>=STRESS_THRESHOLD && r2<TIME_STEP/TAU_PLAST)|| \
		(actualstate2==1 && r2>TIME_STEP/TAU_ELAST));
	}
}

//PhiloxQuadVersion: TODO: bad performance, why?
__global__ void Kernel_UpdateStateVariablesQuad(const REAL* d_sigma, bool* d_state, int time)
{
	// compute idx and idy, the location of the element in the original LX*LY array 
	int idx = blockIdx.x*blockDim.x+threadIdx.x;
	int idy = blockIdx.y*blockDim.y+threadIdx.y;
	if ( idx < LX/4 && idy < LY){
		int index = idx*LY + idy;
		bool actualstate[4]={0,0,0,0};
		for (int k=0;k<4;k++) actualstate[k]=d_state[4*index+k];
		float r[4]={0,0,0,0};
		PhiloxRandomQuartet(index, time, &r[0], &r[1],&r[2], &r[3]);
		for (int k=0;k<4;k++) d_state[4*index+k] = \
		( (actualstate[k]==0 && abs(d_sigma[4*index+k])>=STRESS_THRESHOLD && r[k]<TIME_STEP/TAU_PLAST)|| \
		(actualstate[k]==1 && r[k]>TIME_STEP/TAU_ELAST) );
	}
}

// COMPLEX pointwise multiplication
static __global__ void Kernel_ComplexPointwiseMulAndScale(const COMPLEX* a, const REAL* b, COMPLEX* c, REAL scale)
{
	// compute idx and idy, the location of the element in the original LX*LY array 
	int idx = blockIdx.x*blockDim.x+threadIdx.x;
	int idy = blockIdx.y*blockDim.y+threadIdx.y;
	if ( idx < LX && idy < (LY/2+1)){
		int index = idx*(LY/2+1) + idy;
		c[index].x = scale*(a[index].x * b[index]);
		c[index].y = scale*(a[index].y * b[index]);
	}
} 

static __global__ void Kernel_EulerIntegrationStep(REAL *a, const REAL *b, REAL gamma, REAL dt)
{
	// compute idx and idy, the location of the element in the original LX*LY array 
	int idx = blockIdx.x*blockDim.x+threadIdx.x;
	int idy = blockIdx.y*blockDim.y+threadIdx.y;
	if ( idx < LX && idy < LY){
		int index = idx*LY + idy;
		//There is a factor of 2 missing here (multipling b), that would cancell with the factor of \
		1/2 that is missing in the calculus of b as well. Everyone happy.
		a[index]+=(gamma+b[index])*dt;
	}
}

static __global__ void Kernel_EulerIntegrationStepTemp(REAL *a, const REAL *b, REAL gamma, REAL temp, unsigned int time, REAL dt)
{
	int idx = blockIdx.x*blockDim.x+threadIdx.x;
	int idy = blockIdx.y*blockDim.y+threadIdx.y;
	if ( idx < LX/2 && idy < LY){
		int index = idx*LY + idy;
		float r1, r2;
		PhiloxRandomPair(index, time, &r1, &r2);
		for (int j=0;j<2;j++)
		{
		unsigned int index_aux = 2*index+j;
		REAL rannum = r1*(j==0)+r2*(j==1);
		//There is a factor of 2 missing here (multipling b), that would cancell with the factor of \
		1/2 that is missing in the calculus of b as well. Everyone happy.
		a[index_aux]+=(gamma+b[index_aux])*dt+(rannum-0.5)*(sqrt(24.*temp*dt));
		}
	}
}

#ifdef ACTIVATED_DYNAMICS
__global__ void Kernel_UpdateStateVariablesActivated(const REAL* d_sigma, bool* d_strain_sign, bool* d_state, const REAL temp, unsigned int time)
{
	int idx = blockIdx.x*blockDim.x+threadIdx.x;
	int idy = blockIdx.y*blockDim.y+threadIdx.y;
	if ( idx < LX/2 && idy < LY){
		int index = idx*LY + idy;
		bool actualstate;
		REAL actualsigma, lelast;
		#ifndef RANDOM_EVENTS
		REAL lplastpos, lplastneg;
		#endif
		float r1, r2;
		PhiloxRandomPair(index, time, &r1, &r2);

		for (int j=0;j<2;j++)
		{
			REAL rannum = r1*(j==0)+r2*(j==1);
			unsigned int index_aux = 2*index+j;
			actualstate = d_state[index_aux];
			actualsigma = d_sigma[index_aux];
			lelast = 1/TAU_ELAST;

			#ifdef RANDOM_EVENTS
			if(actualstate==0 && ( rannum<temp*TIME_STEP )){
				d_state[index_aux]=1;
				d_strain_sign[index_aux]=(actualsigma>0);
			}
			#else
			lplastpos = (actualsigma>=0)*exp(-0.5*(STRESS_THRESHOLD*STRESS_THRESHOLD-actualsigma*actualsigma)/temp)/TAU_PLAST+(actualsigma<0)*exp(-0.5*(STRESS_THRESHOLD*STRESS_THRESHOLD+actualsigma*actualsigma)/temp)/TAU_PLAST;
			lplastneg = (actualsigma>=0)*exp(-0.5*(STRESS_THRESHOLD*STRESS_THRESHOLD+actualsigma*actualsigma)/temp)/TAU_PLAST+(actualsigma<0)*exp(-0.5*(STRESS_THRESHOLD*STRESS_THRESHOLD-actualsigma*actualsigma)/temp)/TAU_PLAST;

			if(actualstate==0 && (rannum<(lplastpos+lplastneg)*TIME_STEP || (actualsigma>=STRESS_THRESHOLD) || (actualsigma<=-STRESS_THRESHOLD))){
				d_state[index_aux]=1;
				d_strain_sign[index_aux]=(rannum<lplastpos*TIME_STEP || (actualsigma>=STRESS_THRESHOLD));
			}
			#endif
			if(actualstate==1 && rannum<lelast*TIME_STEP){ d_state[index_aux]=0;}
		}
	}
}

#ifdef WAITING_TIMES
__global__ void Kernel_UpdateStateVariablesActivatedWithTW(const REAL* d_sigma, bool* d_strain_sign, bool* d_state, bool* d_arn, const REAL temp, unsigned int time)
{
	int idx = blockIdx.x*blockDim.x+threadIdx.x;
	int idy = blockIdx.y*blockDim.y+threadIdx.y;
	if ( idx < LX/2 && idy < LY){
		int index = idx*LY + idy;
		bool actualstate;
		REAL actualsigma, lelast;
		#ifndef RANDOM_EVENTS
		REAL lplastpos, lplastneg;
		#endif
		float r1, r2;
		PhiloxRandomPair(index, time, &r1, &r2);

		for (int j=0;j<2;j++)
		{
			REAL rannum = r1*(j==0)+r2*(j==1);
			unsigned int index_aux = 2*index+j;
			actualstate = d_state[index_aux];
			actualsigma = d_sigma[index_aux];
			lelast = 1/TAU_ELAST;

			#ifdef RANDOM_EVENTS
			if(actualstate==0 && ( rannum<temp*TIME_STEP )){
				d_arn[index_aux]=1;
				d_state[index_aux]=1;
				d_strain_sign[index_aux]=(actualsigma>0);
			}
			#else
			lplastpos = (actualsigma>=0)*exp(-0.5*(STRESS_THRESHOLD*STRESS_THRESHOLD-actualsigma*actualsigma)/temp)/TAU_PLAST+(actualsigma<0)*exp(-0.5*(STRESS_THRESHOLD*STRESS_THRESHOLD+actualsigma*actualsigma)/temp)/TAU_PLAST;
			lplastneg = (actualsigma>=0)*exp(-0.5*(STRESS_THRESHOLD*STRESS_THRESHOLD+actualsigma*actualsigma)/temp)/TAU_PLAST+(actualsigma<0)*exp(-0.5*(STRESS_THRESHOLD*STRESS_THRESHOLD-actualsigma*actualsigma)/temp)/TAU_PLAST;
			if(actualstate==0 && (rannum<(lplastpos+lplastneg)*TIME_STEP || (actualsigma>=STRESS_THRESHOLD) || (actualsigma<=-STRESS_THRESHOLD))){
				d_arn[index_aux]=1;
				d_state[index_aux]=1;
				d_strain_sign[index_aux]=(rannum<lplastpos*TIME_STEP || (actualsigma>=STRESS_THRESHOLD));
			}
			#endif

			if(actualstate==1 && rannum<lelast*TIME_STEP){ d_state[index_aux]=0;}
		}
	}
}
#endif

#ifdef KMC
__global__ void Kernel_ConstructActivatedPorbabilityTower(REAL *d_tower, const REAL *d_sigma, const REAL temp){
	int idx = blockIdx.x*blockDim.x+threadIdx.x;
	int idy = blockIdx.y*blockDim.y+threadIdx.y;
	if ( idx < LX && idy < LY){
		int index = idx*LY + idy;
		const REAL actualsigma = d_sigma[index];
		const REAL lplastpos = (actualsigma>=0)*exp(-0.5*(STRESS_THRESHOLD*STRESS_THRESHOLD-actualsigma*actualsigma)/temp)/TAU_PLAST+(actualsigma<0)*exp(-0.5*(STRESS_THRESHOLD*STRESS_THRESHOLD+actualsigma*actualsigma)/temp)/TAU_PLAST;
		const REAL lplastneg = (actualsigma>=0)*exp(-0.5*(STRESS_THRESHOLD*STRESS_THRESHOLD+actualsigma*actualsigma)/temp)/TAU_PLAST+(actualsigma<0)*exp(-0.5*(STRESS_THRESHOLD*STRESS_THRESHOLD-actualsigma*actualsigma)/temp)/TAU_PLAST;
		d_tower[2*index] = lplastpos;
		d_tower[2*index+1] = lplastneg;
	}
}

__global__ void Kernel_UpdateChosenSite(bool *d_state, bool *d_strain_sign, const unsigned int chosen){
	int tid = blockIdx.x*blockDim.x+threadIdx.x;
	if(tid==0){
	d_state[chosen/2]=1;
	//This depends strongly in the way the tower was built. \
	Notice that if the chosen site is even it correspond to a strain equal to the positive \
	threshold, and if it is odd, correspond to a strain equal to the negative threshold.
	d_strain_sign[chosen/2]=(chosen%2==0); //(chosen%2==0)-(chosen%2!=0);
	}
}
#endif

#endif


#ifdef TRACERS
__global__ void Kernel_CalculateDisplacementField(COMPLEX *ux, COMPLEX *uy, const REAL2 *o, const COMPLEX *epsdot, const REAL scale)
{
	int idx = blockIdx.x*blockDim.x+threadIdx.x;
	int idy = blockIdx.y*blockDim.y+threadIdx.y;
	if ( idx < LX && idy < (LY/2+1)){
		int index = idx*(LY/2+1) + idy;
		ux[index].y = o[index].x*epsdot[index].x*scale;
		uy[index].y = o[index].y*epsdot[index].x*scale;
		ux[index].x = -o[index].x*epsdot[index].y*scale;
		uy[index].x = -o[index].y*epsdot[index].y*scale;
	}
}

__global__ void Kernel_UpdateTracersPositions(const REAL *ux, const REAL *uy, COMPLEX *tracer, const REAL dt)
{
	int tid = blockIdx.x*blockDim.x+threadIdx.x;
	if ( tid < NTRACERS){
		//Put the thing into the box
		//TODO: Improove this workaround
		int actualx = (tracer[tid].x < 0) ? \
			int(tracer[tid].x+0.5)+(int(abs(tracer[tid].x)/LX)+2)*LX:int(tracer[tid].x+0.5);
		int actualy = (tracer[tid].y < 0) ? \
			int(tracer[tid].y+0.5)+(int(abs(tracer[tid].y)/LY)+2)*LY:int(tracer[tid].y+0.5);
		int tracerlocation = ((actualx)%LX)*LY + (actualy)%LY;

		tracer[tid].x+=ux[tracerlocation]*dt;
		tracer[tid].y+=uy[tracerlocation]*dt;
	}
}
#endif

//////////-------------------------Calculate Functions---------------///////////////////////////

#ifdef TRACERS
__global__ void Kernel_CalculateMeanSquareDisplacement(const REAL2 *a, const REAL2 *b, REAL *r){
	const unsigned int tid = blockIdx.x*blockDim.x+threadIdx.x;
	if (tid < NTRACERS){
		r[tid] = (a[tid].x-b[tid].x)*(a[tid].x-b[tid].x) + (a[tid].y-b[tid].y)*(a[tid].y-b[tid].y);
	}
}

//TODO: Not working
__global__ void Kernel_SFparallelReduce(REAL *sf_aux, REAL *sf_aux2, int nx, int ny){
	unsigned int k = threadIdx.x;
	unsigned int i = blockIdx.y*blockDim.y+threadIdx.y;
	unsigned int j = blockIdx.z*blockDim.z+threadIdx.z;
	unsigned int index = i*ny+j;
	if ( i < nx && j < ny && k<NTRACERS/TILEB){

		__shared__ REAL s_sf[NTRACERS/TILEB];
		__syncthreads();

		s_sf[k] = sf_aux2[index*TILEB+k];
		__syncthreads();

		int p = blockDim.x/2;
		while (p != 0) {
			if (k < p) s_sf[k] += s_sf[k + p];
			__syncthreads();
			p /= 2;
		}
		if (k == 0) sf_aux[index] = s_sf[k]/(REAL)NTRACERS;

	}
}


//TODO: Not working
__global__ void Kernel_CalculateTrajectoriesSFparallel(const REAL2 *tracer, const REAL2 *tracertw, \
			REAL *sf_aux2, int nx, int ny){
	unsigned int k = blockIdx.x*blockDim.x+threadIdx.x;
	unsigned int i = blockIdx.y*blockDim.y+threadIdx.y;
	unsigned int j = blockIdx.z*blockDim.z+threadIdx.z;
	unsigned int cacheindex = threadIdx.x;
	unsigned int index = i*ny+j;
	if ( i < nx && j < ny && k<NTRACERS){
		REAL qx=2.0*M_PI*i/(REAL)LX;
		REAL qy=2.0*M_PI*j/(REAL)LY;

		__shared__ REAL s_sf[TILEB];
		__syncthreads();

		REAL dotproduct = qx*(tracer[k].x - tracertw[k].x)+qy*(tracer[k].y - tracertw[k].y);
		s_sf[cacheindex] = cos(dotproduct);
		__syncthreads();

		int p = blockDim.x/2;
		while (p != 0) {
			if (cacheindex < p) s_sf[cacheindex] += s_sf[cacheindex + p];
			__syncthreads();
			p /= 2;
		}
		if (cacheindex == 0) sf_aux2[index*TILEB+blockIdx.x] = s_sf[cacheindex];

	}
}


__global__ void Kernel_CalculateTrajectoriesSF(const REAL2 *tracer, const REAL2 *tracertw, \
			REAL *structure_factor, int nx, int ny){
	unsigned int i = blockIdx.x*blockDim.x+threadIdx.x;
	unsigned int j = blockIdx.y*blockDim.y+threadIdx.y;
	unsigned int index = i*ny+j;
	if ( i < nx && j < ny){
		REAL qx=2.0*M_PI*i/(REAL)LX;
		REAL qy=2.0*M_PI*j/(REAL)LY;

		double sf=0.0;
		//TODO:Parallel reduction of this stuff
		//Still can paralelize this loop with  thrird dimension in the kernel
		//Unroll the loop and use sharedMem to store tracers
		for(unsigned int k=0; k<NTRACERS; k++){
			REAL dotproduct = qx*(tracer[k].x - tracertw[k].x)+qy*(tracer[k].y - tracertw[k].y);
			sf += cos(dotproduct);
			//sf += fabsf(cosf(dotproduct));
			//REAL s, c;
			//sincosf(dotproduct, &s, &c);
			//sf += s*s+c*c; // This gives one IDIOT
		}
		structure_factor[index]=sf/(REAL)NTRACERS;
	}
}

#ifdef VISUALIZATION
__global__ void CUDAkernel_ComputeStructureFactor(const REAL2 *tracer, const REAL2 *tracertw, \
						REAL *d_Sq2d, int nx, int ny){
	int i = blockIdx.x*blockDim.x+threadIdx.x;
	int j = blockIdx.y*blockDim.y+threadIdx.y;
	if ( i < nx  && j < ny){
		unsigned int k=i*(LY/2+1)+j;
		unsigned int kk=i*LY+j;
		unsigned int kkk=(i+1)*LY-j;

		REAL qx=2.0*M_PI*double(i)/LX;
		REAL qy=2.0*M_PI*double(j)/LY;
		//REAL q2=qx*qx+qy*qy;
		d_Sq2d[kk] = 0;
		for(unsigned int i=0; i<NTRACERS; i++){
			REAL dotproduct = qx*(tracer[k].x - tracertw[k].x)+qy*(tracer[k].y - tracertw[k].y);
			d_Sq2d[kk] += cos(dotproduct);
		}
		d_Sq2d[kk] /=NTRACERS;
		d_Sq2d[kkk] = d_Sq2d[kk];
	}
}

__global__ void CUDAkernel_ComputeStructureFactorImage(const REAL *d_Sq2d, REAL *d_Sq2dimage, int nx, int ny){
	int idx = blockIdx.x*blockDim.x+threadIdx.x;
	int idy = blockIdx.y*blockDim.y+threadIdx.y;
	if ( idx < nx  && idy < ny){
		unsigned int k=idx*LY+idy;
		int ii=(idx>LX/2)?(idx-LX/2):(idx+LX/2);
		int jj=(idy>LY/2)?(idy-LY/2):(idy+LY/2);
		unsigned int kk=ii*LY+jj;
		d_Sq2dimage[kk]=d_Sq2d[k];
	}
}
#endif


#endif

#ifdef WAITING_TIMES
__global__ void Kernel_ComputeWaitingTimeDistribution(const bool *d_arn, unsigned int *d_p_of_tw, unsigned int *d_actual_tw, const unsigned int time)
{
	int idx = blockIdx.x*blockDim.x+threadIdx.x;
	int idy = blockIdx.y*blockDim.y+threadIdx.y;
	if ( idx < LX  && idy < LY){
		unsigned int index=idx*LY+idy;
		d_p_of_tw[index] = (d_arn[index])*(time-d_actual_tw[index]) + !(d_arn[index])*d_p_of_tw[index];
		d_actual_tw[index] = (d_arn[index])*time + !(d_arn[index])*d_actual_tw[index];
	}
}
#endif

