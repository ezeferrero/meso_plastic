/*
 * mesoplastic_library.cuh
 *
 *  started on: May 10, 2013
 *      Author: eze
 * .....based on: main.cu by Kirsten
 */

#ifndef MESOPLASTIC_LIBRARY_CUH
#define MESOPLASTIC_LIBRARY_CUH

// RNG: PHILOX
#include "../common/RNGcommon/Random123/philox.h"
#include "../common/RNGcommon/Random123/u01.h"

#include <iostream>		/* std::cout, std::fixed */
#include <iomanip>		/* std::setprecision */
#include <cmath>
#include <fstream>
#include <string>

#include <assert.h>
#include <stdint.h>		/* uint8_t */
#include <cufft.h>
#include <cstdlib>
#include <time.h>

//Define random() Seed
//#define RANDOM_SEED 7536951

//Define Philox & Seed
typedef r123::Philox2x32 RNG2;
typedef r123::Philox4x32 RNG4;
#ifndef PHILOX_SEED
#define PHILOX_SEED 125522117
#endif

__device__ unsigned int philox_seed = PHILOX_SEED;

#include "../common/cuda_util.h"
#include "mesoplastic_kernels.cuh"
#include "../common/writeppm.h"
#include "../common/histo.h"

using namespace std;
using namespace thrust::placeholders;

template<typename T>
struct modified_multiplies : public binary_function<T,T,T>
{
//__host__ __device__ T operator()(const T &lhs, const T &rhs) const {return ((lhs>=0)-(lhs<0))*rhs;}
__host__ __device__ T operator()(const T &lhs, const T &rhs) const {return (2*(lhs)-1)*rhs;}
}; // end modified_multiplies

template<typename T>
struct euler_distance : public binary_function<T,REAL2,REAL2>
{
__host__ __device__ 
T operator()(const REAL2 &a, const REAL2 &b) const {return sqrt((a.x-b.x)*(a.x-b.x)+(a.y-b.y)*(a.y-b.y));}
};

struct ElementWiseProduct : public thrust::binary_function<COMPLEX,REAL,COMPLEX>
{
	REAL scale;
	ElementWiseProduct(REAL scale_) : scale(scale_){}

	__host__ __device__
	COMPLEX operator()(const COMPLEX& v1, const REAL& v2) const
	{
		COMPLEX res;
		res.x = scale * v1.x * v2;
		res.y = scale * v1.y * v2;
		return res;
	}
};


//////////////////////////////////////////////////////////////////////
class mesoplastic_model
{
	private:
	cufftHandle plan_c2r;
	cufftHandle plan_r2c;
	int datasize, datawindows;

	public:
	// Pointer to host arrays (realspace)
	bool *h_state;
	REAL *h_sigma;
	REAL *h_propagator;
	#ifdef TRACERS
	double *h_msd;
	unsigned int *h_msd_counter;
	#ifdef SQ
		double *h_sf;
		unsigned int *h_sf_counter;
		int qqssize;
	#endif
	REAL2 *h_tracers; //TODO: Move this to a SOA
	REAL *h_dsigma;
	REAL *h_ux_r;
	REAL *h_uy_r;
	#endif
	#ifdef VISUALIZATION
	REAL *h_Sq2dimage;
	#endif
	#ifdef WAITING_TIMES
	unsigned int *h_p_of_tw;
	#endif
	char filename_msd[128]; char filename_sf_q[128];char filename_sf_t[128];char filename_tr[128]; char filename_cr[128];
	char filename_msd_tmp[128]; char filename_sf_q_tmp[128];char filename_sf_t_tmp[128]; char root[96];

	// Pointer to device arrays (realspace and fourierspace)
	bool *d_state;
	REAL *d_sigma;
	bool *d_strain_sign;
	COMPLEX *d_dsigma;
	REAL *d_dsigma_r;
	COMPLEX *d_epsilon_dot;
	REAL *d_epsilon_dot_r;
	REAL *d_propagator;
	#ifdef KMC
	REAL *d_tower;
	#endif
	#ifdef TRACERS
	REAL2 *d_tracers;  //TODO: Move this to a SOA
	REAL2 *d_tracerstw; //TODO: Move this to a SOA
	COMPLEX *d_ux; //TODO: Move this to a SOA
	COMPLEX *d_uy; //TODO: Move this to a SOA
	REAL *d_ux_r;
	REAL *d_uy_r;
	REAL2 *d_oseen;
		#ifdef SQ
		REAL *d_sf_aux;
		REAL *d_sf_out;
		unsigned int *d_qsquares;
		unsigned int *d_qsquares_out;
		unsigned int *d_qsquares_sorted;
		unsigned int *d_qsquares_occurence;
		unsigned int *h_qsquares_out;
		#ifdef VISUALIZATION
		REAL *d_Sq2d, *d_Sq2dimage;
		#endif
		#endif
	#endif
	#ifdef WAITING_TIMES
	bool *d_arn; //activated right now
	unsigned int *d_actual_tw;
	unsigned int *d_p_of_tw;
	#endif
	#ifdef GLOBAL_WAITING_TIMES
	bool *d_state_aux;
		#ifdef TRACERS
		REAL2 *d_tracers_aux;
		#endif
	#endif

	// intializing the class
	//mesoplastic_model(){
	mesoplastic_model(int a, int b) {
		datasize = a; 
		datawindows = b;

		//Array sizes
		#ifdef WAITING_TIMES
		size_t size_ui = NN * sizeof(unsigned int);
		#endif
		size_t size_b  = NN * sizeof(bool);
		size_t size_r  = NN * sizeof(REAL);
		size_t size_c  = LX*(LY/2+1) * sizeof(COMPLEX);
		size_t size_rc = LX*(LY/2+1) * sizeof(REAL);

		#ifdef TRACERS
			#ifdef SQ
			#ifdef ALLQ
			size_t size_rsf  = NN * sizeof(REAL);
			size_t size_uisf = NN * sizeof(unsigned int);
			#else
			size_t size_rsf  = QNX*QNY* sizeof(REAL);
			size_t size_uisf = QNX*QNY* sizeof(unsigned int);
			#endif
			#endif
		#endif

		// Allocate arrays on host
		CUDA_SAFE_CALL(cudaMallocHost((void **) &h_state, size_b));
		CUDA_SAFE_CALL(cudaMallocHost((void **) &h_sigma, size_r));
		CUDA_SAFE_CALL(cudaMallocHost((void **) &h_propagator, size_r));
		#ifdef TRACERS
		CUDA_SAFE_CALL(cudaMallocHost((void **) &h_tracers, NTRACERS*sizeof(REAL2)));
		h_msd = (double *)malloc(datasize*sizeof(double));
		h_msd_counter = (unsigned int *)malloc(datasize*sizeof(unsigned int));
		//#ifdef SQ
		//assert(NN%128==0);
		//h_sf = (double *)malloc((datasize)*NN/128*sizeof(double));
		//h_sf_counter = (unsigned int *)malloc(datasize*sizeof(unsigned int));
		//#endif

		CUDA_SAFE_CALL(cudaMallocHost((void **) &h_dsigma, size_r));
		CUDA_SAFE_CALL(cudaMallocHost((void **) &h_ux_r, size_r));
		CUDA_SAFE_CALL(cudaMallocHost((void **) &h_uy_r, size_r));
		#endif
		#ifdef VISUALIZATION
		CUDA_SAFE_CALL(cudaMallocHost((void **) &h_Sq2dimage, size_r));
		#endif

		// Allocate arrays on device
		CUDA_SAFE_CALL(cudaMalloc((void **) &d_state, size_b));
		CUDA_SAFE_CALL(cudaMalloc((void **) &d_sigma, size_r));
		CUDA_SAFE_CALL(cudaMalloc((void **) &d_strain_sign, size_b));
		CUDA_SAFE_CALL(cudaMalloc((void **) &d_dsigma, size_c));
		CUDA_SAFE_CALL(cudaMalloc((void **) &d_dsigma_r, size_r));
		CUDA_SAFE_CALL(cudaMalloc((void **) &d_epsilon_dot, size_c));
		CUDA_SAFE_CALL(cudaMalloc((void **) &d_epsilon_dot_r, size_r));
		CUDA_SAFE_CALL(cudaMalloc((void **) &d_propagator, size_rc));
		#ifdef KMC
		CUDA_SAFE_CALL(cudaMalloc((void **) &d_tower, 2*size_r));
		#endif
		#ifdef TRACERS
		CUDA_SAFE_CALL(cudaMalloc((void **) &d_tracers, NTRACERS*sizeof(REAL2)));
		CUDA_SAFE_CALL(cudaMalloc((void **) &d_tracerstw, datawindows*NTRACERS*sizeof(REAL2)));
		CUDA_SAFE_CALL(cudaMalloc((void **) &d_ux, size_c));
		CUDA_SAFE_CALL(cudaMalloc((void **) &d_uy, size_c));
		CUDA_SAFE_CALL(cudaMalloc((void **) &d_ux_r, size_r));
		CUDA_SAFE_CALL(cudaMalloc((void **) &d_uy_r, size_r));
		CUDA_SAFE_CALL(cudaMalloc((void **) &d_oseen, LX*(LY/2+1) * sizeof(REAL2)));
			#ifdef SQ
			CUDA_SAFE_CALL(cudaMalloc((void **) &d_sf_aux, size_rsf));
			CUDA_SAFE_CALL(cudaMalloc((void **) &d_qsquares, size_uisf));
			CUDA_SAFE_CALL(cudaMalloc((void **) &d_qsquares_out, size_uisf));
			CUDA_SAFE_CALL(cudaMalloc((void **) &d_qsquares_sorted, size_uisf));
			#ifdef VISUALIZATION
			CUDA_SAFE_CALL(cudaMalloc((void **) &d_Sq2d, size_r));
			CUDA_SAFE_CALL(cudaMalloc((void **) &d_Sq2dimage, size_r));
			#endif
			#endif
		#endif
		#ifdef WAITING_TIMES
		CUDA_SAFE_CALL(cudaMalloc((void **) &d_arn, size_b));
		CUDA_SAFE_CALL(cudaMalloc((void **) &d_actual_tw, size_ui));
		CUDA_SAFE_CALL(cudaMalloc((void **) &d_p_of_tw, size_ui));
		CUDA_SAFE_CALL(cudaMallocHost((void **) &h_p_of_tw, size_ui));
		#endif
		#ifdef GLOBAL_WAITING_TIMES
		CUDA_SAFE_CALL(cudaMalloc((void **) &d_state_aux, size_b));
			#ifdef TRACERS
			CUDA_SAFE_CALL(cudaMalloc((void **) &d_tracers_aux, NTRACERS*sizeof(REAL2)));
			#endif
		#endif

		//Preventive fill everything with 0s //Actually this cudaMemset doesn't work in floats
		for (unsigned int k=0; k<NN; k++){h_state[k]=0; h_sigma[k]=0.0; h_propagator[k]=0.0;}
		CUDA_SAFE_CALL(cudaMemset(d_state, 0, size_b));
		CUDA_SAFE_CALL(cudaMemset(d_sigma, 0, size_r));
		CUDA_SAFE_CALL(cudaMemset(d_strain_sign, 0, size_b));
		CUDA_SAFE_CALL(cudaMemset(d_dsigma, 0, size_c));
		CUDA_SAFE_CALL(cudaMemset(d_dsigma_r, 0, size_r));
		CUDA_SAFE_CALL(cudaMemset(d_epsilon_dot, 0, size_c));
		CUDA_SAFE_CALL(cudaMemset(d_epsilon_dot_r, 0, size_r));
		CUDA_SAFE_CALL(cudaMemset(d_propagator, 0, size_rc));
		#ifdef KMC
		CUDA_SAFE_CALL(cudaMemset(d_tower, 0, 2*size_r));
		#endif
		#ifdef TRACERS
		for (unsigned int k=0; k<NTRACERS; k++){h_tracers[k].x=0.0;h_tracers[k].y=0.0;}
		for (int k=0; k<datasize; k++) {h_msd[k] = 0.0; h_msd_counter[k] = 0;}
		//for (unsigned int k=0; k<NN; k++){h_Sq2dimage[k] = 0.0;}
//		#ifdef SQ
//		for (int k=0; k<(datasize)*NN/128; k++) {h_sf[k] = 0.0;}
//		for (int k=0; k<datasize; k++) { h_sf_counter[k] = 0;}
//		#endif
		CUDA_SAFE_CALL(cudaMemset(d_tracers, 0, NTRACERS*sizeof(REAL2)));
		CUDA_SAFE_CALL(cudaMemset(d_tracerstw, 0, datawindows*NTRACERS*sizeof(REAL2)));
		CUDA_SAFE_CALL(cudaMemset(d_ux, 0, size_c));
		CUDA_SAFE_CALL(cudaMemset(d_uy, 0, size_c));
		CUDA_SAFE_CALL(cudaMemset(d_ux_r, 0, size_r));
		CUDA_SAFE_CALL(cudaMemset(d_uy_r, 0, size_r));
		CUDA_SAFE_CALL(cudaMemset(d_oseen, 0, LX*(LY/2+1) * sizeof(REAL2)));
			#ifdef SQ
			CUDA_SAFE_CALL(cudaMemset(d_sf_aux, 0, size_rsf));
			CUDA_SAFE_CALL(cudaMemset(d_qsquares, 0, size_uisf));
			CUDA_SAFE_CALL(cudaMemset(d_qsquares_out, 0, size_uisf));
			CUDA_SAFE_CALL(cudaMemset(d_qsquares_sorted, 0, size_uisf));
/*			#ifdef ALLQ
			for (int k=0; k<NN; k++) {h_qsquares_out[k] = 0;}
			#else
			for (int k=0; k<NQX*NQY; k++) {h_qsquares_out[k] = 0;}
			#endif*/
			#ifdef VISUALIZATION
			CUDA_SAFE_CALL(cudaMemset(d_Sq2d, 0, size_r));
			CUDA_SAFE_CALL(cudaMemset(d_Sq2dimage, 0, size_r));
			#endif
			#endif
		#endif

		#ifdef WAITING_TIMES
		CUDA_SAFE_CALL(cudaMemset(d_arn, 0, size_b));
		CUDA_SAFE_CALL(cudaMemset(d_actual_tw, 0, size_ui));
		CUDA_SAFE_CALL(cudaMemset(d_p_of_tw, 0, size_ui));
		for (int k=0; k<NN; k++) {h_p_of_tw[k] = 0;}
		#endif
		#ifdef GLOBAL_WAITING_TIMES
			CUDA_SAFE_CALL(cudaMemset(d_state_aux, 0, size_b));
			#ifdef TRACERS
			CUDA_SAFE_CALL(cudaMemset(d_tracers_aux, 0, NTRACERS*sizeof(REAL2)));
			#endif
		#endif

		// cuFFT plan
		#ifdef DOUBLE_PRECISION
		cufftPlan2d(&plan_c2r, LX, LY, CUFFT_Z2D);
		cufftPlan2d(&plan_r2c, LX, LY, CUFFT_D2Z);
		#else
		cufftPlan2d(&plan_c2r, LX, LY, CUFFT_C2R);
		cufftPlan2d(&plan_r2c, LX, LY, CUFFT_R2C);
		#endif
		//cufftSetCompatibilityMode(plan_c2r, CUFFT_COMPATIBILITY_NATIVE);
		//cufftSetCompatibilityMode(plan_r2c, CUFFT_COMPATIBILITY_NATIVE);
	}

////////////////////////////////////////////////////////////////////////////////////////////////

//////////-----------------------Initialize Functions----------------///////////////////////////

	//Initializes on Host and CpyToDevice
	void Initialize(){
		// Initialize trivial host arrays
		for(int i = 0; i<LX; i++)
			for(int j = 0; j<LY; j++)
			{
				int ij = i*LY+j;
				h_sigma[ij] = STRESS_THRESHOLD;
				h_state[ij] = 0;
			}
		h_state[0]=1;
		h_state[LX/2*LY+LY/2]=1;

		// Initialize host array for propagator
		for(int i = 0; i<LX/2+1; i++)
			for(int j = 0; j<(LY/2+1); j++)
			{
			REAL qx=2.0*M_PI*double(i)/LX;
			REAL qy=2.0*M_PI*double(j)/LY;
			REAL q2=qx*qx+qy*qy;
			if(i!=0 || j!=0) h_propagator[i*(LY/2+1)+j] = -4*qx*qx*qy*qy/(q2*q2);
			if(i!=0) h_propagator[(LX-i)*(LY/2+1)+j] = h_propagator[i*(LY/2+1)+j];
			}
		h_propagator[0] =-1.0; // amplitude of the plastic event

		size_t size_b = NN * sizeof(bool);
		size_t size_r = NN * sizeof(REAL);
		size_t size_rc = LX*(LY/2+1) * sizeof(REAL);

		cudaMemcpy(d_sigma, h_sigma, size_r ,cudaMemcpyHostToDevice);
		cudaMemcpy(d_state, h_state, size_b ,cudaMemcpyHostToDevice);
		cudaMemcpy(d_propagator, h_propagator, size_rc, cudaMemcpyHostToDevice);
	}

	void InitializeOnDevice(int ran){
		// Initialize State
		thrust::device_ptr<bool> state_ptr (d_state);
		thrust::fill(state_ptr, state_ptr+NN, 1);

		//Or construct a particular initial state:
//		thrust::device_vector<bool> tmp(NN, 0);
//		tmp[LX/2*LY+LY/2]=1; //(random() & 0x1u);
//		tmp[(LX/2)*LY+LY/2+10]=1; //(random() & 0x1u);
//		tmp[(LX/2-10)*LY+LY/2+5]=1; //(random() & 0x1u);
//		thrust::copy(tmp.begin(),tmp.end(),state_ptr);


		// Initialize Stress Uniform
		thrust::device_ptr<REAL> sigma_ptr(d_sigma);
		thrust::fill(sigma_ptr, sigma_ptr+NN, STRESS_THRESHOLD);
//		thrust::fill(sigma_ptr, sigma_ptr+NN, 0.0);

		// Initialize Stress Gaussian
//		dim3 dimBlock0(TILE_X, TILE_Y);
//		dim3 dimGrid0(LX/TILE_X, LY/TILE_Y);
//		assert(dimBlock0.x*dimBlock0.y<=THREADS_PER_BLOCK);
//		assert(dimGrid0.x<=BLOCKS_PER_GRID && dimGrid0.y<=BLOCKS_PER_GRID);
//		unsigned int ran = time(NULL);
//		REAL scale_gaussian = sqrt(12);
//		Kernel_InitRandomGaussian<<<dimGrid0, dimBlock0>>>(d_sigma, d_state, scale_gaussian, ran);


		// Initialize Strain Sign
		thrust::device_ptr<bool> strain_sign_ptr(d_strain_sign);
		thrust::fill(strain_sign_ptr, strain_sign_ptr+NN, 1);
//		thrust::copy(state_ptr, state_ptr+NN, strain_sign_ptr);

		//Or construct a particular initial strain_sign:
//		thrust::device_vector<bool> tmp2(NN, 0);
//		tmp2[LX/2*LY+LY/2]=1; //(random() & 0x1u);
//		tmp2[(LX/2)*LY+LY/2+10]=1; //(random() & 0x1u);
//		tmp2[(LX/2-10)*LY+LY/2+5]=0; //(random() & 0x1u);
//		thrust::copy(tmp2.begin(),tmp2.end(),strain_sign_ptr);

		// Initialize Propagator
		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(LX/TILE_X, LY/TILE_Y/2+1);
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);
		Kernel_SetPropagator<<<dimGrid, dimBlock>>>(d_propagator,LX,LY/2+1);
		//TODO: Try to fit propagator in constant or texture memory
		//CUDA_SAFE_CALL(cudaBindTexture(NULL, propagatorTex, d_propagator, LX*(LY/2+1)*sizeof(REAL)));
	}

	void InitializeLocalStressesFromFile(ifstream &stored_local_stresses)
	{
		if(stored_local_stresses.good()){
			for(unsigned int i=0; i<NN; i++) stored_local_stresses >> h_sigma[i];
		}else{
		cout << "Problems with stored_local_stresses " << '\n';
		}

		CUDA_SAFE_CALL(cudaMemcpy(d_sigma, &h_sigma[0], sizeof(REAL)*NN, cudaMemcpyHostToDevice));
	}

	void InitializeLocalStatesFromFile(ifstream &stored_local_states)
	{
		if(stored_local_states.good()){
			for(unsigned int i=0; i<NN; i++) stored_local_states >> h_state[i];
		}else{
		cout << "Problems with stored_local_states " << '\n';
		}

		CUDA_SAFE_CALL(cudaMemcpy(d_state, &h_state[0], sizeof(bool)*NN, cudaMemcpyHostToDevice));
	}

	#ifdef TRACERS
	void InitializeTracersAndAll(){

		// Initialize Oseen Tensor
		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(LX/TILE_X, LY/TILE_Y/2+1);
		Kernel_SetOseenTensor<<<dimGrid, dimBlock>>>(d_oseen,LX,LY/2+1);
		//TODO: Bind Oseen tensor to texture

		// Initialize Tracers
		assert(TILE<=THREADS_PER_BLOCK);
		assert(NTRACERS/TILE<=BLOCKS_PER_GRID);
		Kernel_InitTracers<<<NTRACERS/TILE, TILE>>>(d_tracers, NTRACERS);

	#ifdef SQ
		//Compute qsquares

		dim3 dimBlock2(QTILE_X, QTILE_Y);
		dim3 dimGrid2(QNX/QTILE_X, QNY/QTILE_Y);
		assert(dimBlock2.x*dimBlock2.y<=THREADS_PER_BLOCK);
		assert(dimGrid2.x<=BLOCKS_PER_GRID && dimGrid2.y<=BLOCKS_PER_GRID);
		Kernel_FillQSquares<<<dimGrid2, dimBlock2>>>(d_qsquares, QNX, QNY);
		//in d_squares we have the function q2(i*LY+j)= i^2+j^2
		//CUDA_SAFE_CALL(cudaThreadSynchronize()); //TODO: Check if needed
		thrust::device_ptr<unsigned int> qsquares_ptr(d_qsquares);

		//Sort qsquares
		thrust::device_ptr<unsigned int> qsquares_sorted_ptr(d_qsquares_sorted);
		thrust::copy(qsquares_ptr, qsquares_ptr+QNN, qsquares_sorted_ptr);
		thrust::sort(qsquares_sorted_ptr,qsquares_sorted_ptr+QNN);
		//in d_squares_sorted we have the LX*LY values of q2 sorted (with redundance)

		//Compute qsquares occurence
		thrust::device_vector<unsigned int> d_occurence(QNN,1);
		thrust::device_vector<unsigned int> d_occ_out(QNN);
		thrust::device_ptr<unsigned int> qq_ptr(d_qsquares_out);
		thrust::fill(qq_ptr, qq_ptr+QNN, 0); //TODO: Check if needed
		//reduce_by_key CANNOT support in_place keys nor values. Estimated time to realize this: 12hs
		thrust::pair<thrust::device_ptr<unsigned int>, thrust::device_vector<unsigned int>::iterator> new_end = \
		thrust::reduce_by_key(qsquares_sorted_ptr,
				qsquares_sorted_ptr+QNN,
				d_occurence.begin(),
				qq_ptr,
				d_occ_out.begin());
		//in qq_ptr we have the values of q2 sorted (each one appearing only once)
		//in d_occurence we have the corresponding occurence of each value of qq_ptr
		qqssize = (new_end.first-qq_ptr);

		//Let's find the lenght of qq_ptr, we search for the biggest possible q^2
/*		thrust::device_ptr<unsigned int> iter;
		iter = thrust::find(qq_ptr,qq_ptr+QNN, QSMAX);
		qqssize = (iter - qq_ptr)+1; //This is a public variable of the class, so we can use it later
*/

		cout << "### Number of different q values on the lattice : " << qqssize << '\n';

		//Copy d_occurence into the global array d_qsquares_occurence
		CUDA_SAFE_CALL(cudaMalloc((void**) &d_qsquares_occurence, sizeof(unsigned int)*qqssize));
		CUDA_SAFE_CALL(cudaMemset(d_qsquares_occurence, 0, sizeof(unsigned int)*qqssize));
		thrust::device_ptr<unsigned int> occ_qsqr_ptr(d_qsquares_occurence);
		thrust::copy(d_occ_out.begin(),d_occ_out.begin()+qqssize, occ_qsqr_ptr);

		CUDA_SAFE_CALL(cudaMalloc((void**) &d_sf_out, sizeof(REAL)*qqssize));
		thrust::device_ptr<REAL> sf_out_ptr (d_sf_out);
		thrust::fill(sf_out_ptr,sf_out_ptr+qqssize, 0.0);

		//TODO: This can be copied only once at the begining //Indeed is the case
		h_qsquares_out = (unsigned int *)malloc(qqssize*sizeof(unsigned int));
		CUDA_SAFE_CALL(cudaMemcpy(h_qsquares_out, d_qsquares_out, qqssize*sizeof(unsigned int), cudaMemcpyDeviceToHost));

		h_sf = (double *)malloc(datasize*qqssize*sizeof(double));
		h_sf_counter = (unsigned int *)malloc(datasize*sizeof(unsigned int));
		for (int k=0; k<(datasize*qqssize); k++) {h_sf[k] = 0.0;}
		for (int k=0; k<datasize; k++) { h_sf_counter[k] = 0;}

// For check purpouses
/*		thrust::host_vector<unsigned int> A(qqssize);
		thrust::host_vector<unsigned int> B(qqssize);
		thrust::copy(qq_ptr,qq_ptr+qqssize,A.begin());
		thrust::copy(d_occurence.begin(),d_occurence.begin()+qqssize,B.begin());
		for(int i=0; i<qqssize; i++) cout << A[i] << " " << B[i] << endl;
		cout << "## Occ has size " << qqssize << endl;
		system("pause");
*/
	#endif

	}
	#endif

////////////////////////////////////////////////////////////////////////////////////////////////

//////////-------------------------Update Functions------------------///////////////////////////


	void UpdateStateVariables(const unsigned long int t){
		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(LX/2/TILE_X, LY/TILE_Y); //using PhiloxPair
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);
		Kernel_UpdateStateVariables<<<dimGrid, dimBlock>>>(d_sigma, d_state, t);
	}

	void ComputePlasticStrain(){
		//thrust::device_ptr<REAL> sigma_ptr (d_sigma);
		thrust::device_ptr<bool> strain_sign_ptr (d_strain_sign);
		thrust::device_ptr<bool> state_ptr (d_state);
		thrust::device_ptr<REAL> epsilon_dot_ptr (d_epsilon_dot_r);
		//TODO: This can be a transform by key or something takig advantage of the bool
		// epsilon_dot <- sigma * state
		//thrust::transform(sigma_ptr, sigma_ptr+NN, state_ptr, epsilon_dot_ptr, thrust::multiplies<REAL>());
		thrust::transform(strain_sign_ptr, strain_sign_ptr+NN, state_ptr, epsilon_dot_ptr, modified_multiplies<REAL>());
		//NOTICE:There is a factor of 1/2 missing in the calculation of epsilon_dot, that would cancell with the factor of 2 that is missing in the calculus of the d_dsigma later on. Everyone happy.
	}

	void Convolution(){
		//TODO: Do this with Thrust.
	/*	thrust::device_ptr<COMPLEX> epsilon_ptr (d_epsilon_dot);
		thrust::device_ptr<REAL> propagator_ptr (d_propagator);
		thrust::device_ptr<COMPLEX> state_ptr (d_dsigma);
		//AND NOW? SAXPY with complex type???
		thrust::transform(epsilon_ptr, epsilon_ptr+(LX*(LY/2+1)), propagator_ptr, state_ptr, 
				ElementWiseProduct(PLASTIC_STRAIN*SCALE));
	*/
		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(LX/TILE_X, LY/TILE_Y/2+1);
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);
		Kernel_ComplexPointwiseMulAndScale<<<dimGrid, dimBlock>>>(d_epsilon_dot, d_propagator, d_dsigma, PLASTIC_STRAIN*SCALE);
	}

	void EulerIntegrationStep(float shear_rate){
		//TODO: If the single other term  in the equation of motion besides the convolution is the shear rate, we \
		can perform Euler directly in fourier, applying gamma to the 0 mode, and then antitransform the new sigma.
		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(LX/TILE_X, LY/TILE_Y);
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);
		Kernel_EulerIntegrationStep<<<dimGrid, dimBlock>>>(d_sigma, d_dsigma_r, shear_rate, TIME_STEP);
		//TODO: Replace this by a thrust transformation.
	}

	void EulerIntegrationStepTemp(float shear_rate, REAL temp, unsigned int t){
		//TODO: If the single other term  in the equation of motion besides the convolution is the shear rate, we \
		can perform Euler directly in fourier, applying gamma to the 0 mode, and then antitransform the new sigma.
		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(LX/2/TILE_X, LY/TILE_Y);
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);
		Kernel_EulerIntegrationStepTemp<<<dimGrid, dimBlock>>>(d_sigma, d_dsigma_r, shear_rate, temp, t, TIME_STEP);
	}

	#ifdef ACTIVATED_DYNAMICS
	void UpdateStateVariablesActivated(const unsigned long int t, const REAL temp){
		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(LX/2/TILE_X, LY/TILE_Y); //using PhiloxPair
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);

		#ifdef WAITING_TIMES
		CUDA_SAFE_CALL(cudaMemset(d_arn, 0, NN*sizeof(bool)));
		Kernel_UpdateStateVariablesActivatedWithTW<<<dimGrid, dimBlock>>>(d_sigma, d_strain_sign, d_state, d_arn,\
								temp, t);
		#else
		Kernel_UpdateStateVariablesActivated<<<dimGrid, dimBlock>>>(d_sigma, d_strain_sign, d_state, \
								temp, t);
		#endif
	}

	#ifdef KMC
	int CheckForPlasticActivity(){
		thrust::device_ptr<bool> state_ptr (d_state);
		thrust::device_ptr<bool> iter;
		iter = thrust::find(state_ptr, state_ptr+NN,1);
		int position = iter - state_ptr;
		return (position);
	}

	REAL ConstructActivatedProbabilityTowerChooseOneEventAndUpdate(const REAL temp){
		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(LX/TILE_X, LY/TILE_Y);
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);
		Kernel_ConstructActivatedPorbabilityTower<<<dimGrid, dimBlock>>>(d_tower, d_sigma, temp);

		thrust::device_ptr<REAL> tower_ptr (d_tower);

		thrust::inclusive_scan(tower_ptr,tower_ptr+2*NN,tower_ptr); //in-place inclusive scan
		//CPY to recover the total sum
		double totalprobability;
		CUDA_SAFE_CALL(cudaMemcpy(&totalprobability, &d_tower[2*NN-1], sizeof(double), cudaMemcpyDeviceToHost));

		//We choose a random number to search the correspondent position in the table
		REAL rannum = genrand_real3()*totalprobability;

		//With lower_bound we find our candidate
		thrust::device_ptr<REAL> chosen = thrust::lower_bound(tower_ptr,tower_ptr+2*NN,rannum);

		// A 1-thread kernel to update the site
		Kernel_UpdateChosenSite<<<1,1>>>(d_state, d_strain_sign, chosen-tower_ptr);

		REAL timeincrement = ((-1.)*log(genrand_real3()))/totalprobability;
		assert(timeincrement>0);

		return timeincrement;
	}
	#endif

	#endif

	#ifdef TRACERS
	void CalculateDisplacementField(){
		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(LX/TILE_X, LY/TILE_Y/2+1);
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);
		Kernel_CalculateDisplacementField<<<dimGrid, dimBlock>>>(d_ux, d_uy, d_oseen, \
								d_epsilon_dot, SCALE);
	}

	void UpdateTracers(){
		dim3 dimBlock(TILE);
		dim3 dimGrid(NTRACERS/TILE);
		assert(dimBlock.x<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID);
		Kernel_UpdateTracersPositions<<<dimGrid, dimBlock>>>(d_ux_r, d_uy_r, d_tracers, \
								TIME_STEP);
	}
	#endif

	void TransformToFourierSpace(){
		#ifdef DOUBLE_PRECISION
		CUFFT_SAFE_CALL(cufftExecD2Z(plan_r2c, d_epsilon_dot_r, d_epsilon_dot));
		#else
		CUFFT_SAFE_CALL(cufftExecR2C(plan_r2c, d_epsilon_dot_r, d_epsilon_dot));
		#endif
	}

	void AntitransformFromFourierSpace(){
		#ifdef DOUBLE_PRECISION
		CUFFT_SAFE_CALL(cufftExecZ2D(plan_c2r, d_dsigma, d_dsigma_r));
			#ifdef TRACERS
			CUFFT_SAFE_CALL(cufftExecZ2D(plan_c2r, d_ux, d_ux_r));
			CUFFT_SAFE_CALL(cufftExecZ2D(plan_c2r, d_uy, d_uy_r));
			#endif
		#else
		CUFFT_SAFE_CALL(cufftExecC2R(plan_c2r, d_dsigma, d_dsigma_r));
			#ifdef TRACERS
			CUFFT_SAFE_CALL(cufftExecC2R(plan_c2r, d_ux, d_ux_r));
			CUFFT_SAFE_CALL(cufftExecC2R(plan_c2r, d_uy, d_uy_r));
			#endif
		#endif
	}

////////////////////////////////////////////////////////////////////////////////////////////////

//////////-------------------------Calculate Functions---------------///////////////////////////
	//TODO: Force double precision ALL in large sums
	REAL CalculateSigmaAv(){
		thrust::device_ptr<REAL> sigma_ptr (d_sigma);
		return thrust::reduce(sigma_ptr, sigma_ptr+NN);
	}

	REAL ComputeSsstress(){
		thrust::device_ptr<REAL> sigma_ptr (d_sigma);
		return thrust::reduce(sigma_ptr,sigma_ptr+NN, (REAL) 0)/(REAL)NN;
	}

	REAL ComputeActivity(){
		thrust::device_ptr<bool> state_ptr (d_state);
		return thrust::reduce(state_ptr,state_ptr+NN, (int) 0)/(REAL)NN;
	}

	#ifdef TRACERS
	void SetTracersAtTW(unsigned int i){
		CUDA_SAFE_CALL( cudaMemcpy(&d_tracerstw[i*NTRACERS], d_tracers, sizeof(REAL2)*NTRACERS, \
				cudaMemcpyDeviceToDevice) );
	}

	void CalculateAccumulateMeanSquareDisplacement(const unsigned int index_t, const unsigned int index_tw, const unsigned int twtd){
		//call: T.CalculateAccumulateMeanSquareDisplacement(t/t_data,t/t_waiting,t_waiting/t_data);
		dim3 dimBlock(TILE);
		dim3 dimGrid(NTRACERS/TILE);
		assert(dimBlock.x<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID);

		thrust::device_vector<REAL> msd_sum(NTRACERS);
		REAL *msd_sum_ptr = thrust::raw_pointer_cast(msd_sum.data());

		//The minimum value for index_tw is 1 since we start measuring when t>=t_waiting
		for (unsigned int i=0; i<index_tw+1; i++){
			Kernel_CalculateMeanSquareDisplacement<<<dimGrid,dimBlock>>>(d_tracers, \
								&d_tracerstw[i*NTRACERS],msd_sum_ptr);
			double meansquaredisplacement = \
			thrust::reduce(msd_sum.begin(),msd_sum.end(),(REAL) 0,thrust::plus<REAL>())/(REAL)(NTRACERS);

			h_msd[index_t-i*twtd] += meansquaredisplacement;
			h_msd_counter[index_t-i*twtd] += 1;
		}
	}

	void CalculatePrintMeanSquareDisplacement(ofstream &fstr, const unsigned long int t, const unsigned long int tw, \
					const unsigned long int tw_counter){
		dim3 dimBlock(TILE);
		dim3 dimGrid(NTRACERS/TILE);
		assert(dimBlock.x<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID);

		thrust::device_vector<REAL> msd_sum(NTRACERS);
		REAL *msd_sum_ptr = thrust::raw_pointer_cast(msd_sum.data());

		Kernel_CalculateMeanSquareDisplacement<<<dimGrid,dimBlock>>>(d_tracers, d_tracerstw,msd_sum_ptr);

		double meansquaredisplacement = thrust::reduce(msd_sum.begin(),msd_sum.end())/(double)(NTRACERS);
		fstr << TIME_STEP*(t-tw) << " " << meansquaredisplacement << " " << '\n';
	}

	#ifdef SQ

	void CalculateAccumulateTrajectoriesStructureFactor(const unsigned int index_t, const unsigned int index_tw, \
						const unsigned int twtd){

		#ifdef PARALLEL_SF_REDUCTION
		dim3 dimBlock(TILEB, 1, 1);
		dim3 dimGrid(NTRACERS/TILEB, QNX, QNY);
		assert(dimBlock.x*dimBlock.y*dimBlock.z<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID & dimGrid.z<=BLOCKS_PER_GRID);
		#else
		dim3 dimBlock(QTILE_X, QTILE_Y);
		dim3 dimGrid(QNX/QTILE_X, QNY/QTILE_Y);
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);
		#endif
		thrust::device_ptr<unsigned int> qsquares_ptr(d_qsquares);
		thrust::device_vector<unsigned int> q2(QNN);
		thrust::device_ptr<REAL> sf_ptr(d_sf_aux);
		thrust::device_ptr<REAL> sf_out_ptr(d_sf_out);
		thrust::device_ptr<unsigned int> qq_ptr(d_qsquares_out);
		thrust::pair<thrust::device_ptr<unsigned int>, thrust::device_ptr<REAL> > new_end;
		thrust::device_ptr<unsigned int> occurence_ptr(d_qsquares_occurence);

		for (unsigned int i=0; i<index_tw+1; i++){

		#ifdef PARALLEL_SF_REDUCTION
		thrust::device_vector<REAL> d_sf_aux2(QNX*QNY*(NTRACERS/TILEB));
		REAL* raw_sf_aux2_ptr = thrust::raw_pointer_cast(d_sf_aux2.data());
		Kernel_CalculateTrajectoriesSFparallel<<<dimGrid, dimBlock>>>(d_tracers,&d_tracerstw[i*NTRACERS], \
								raw_sf_aux2_ptr, QNX, QNY);
		dim3 dimGrid2(1, QNX, QNY);
		dim3 dimBlock2(NTRACERS/TILEB, 1, 1);
		Kernel_SFparallelReduce<<<dimGrid2, dimBlock2>>>(d_sf_aux, raw_sf_aux2_ptr, QNX, QNY);
		#else
		Kernel_CalculateTrajectoriesSF<<<dimGrid, dimBlock>>>(d_tracers,&d_tracerstw[i*NTRACERS], d_sf_aux,\
										QNX, QNY);
		#endif
		//CUDA_SAFE_CALL(cudaThreadSynchronize()); //TODO: Check if needed

		//Do a copy of q^2 in the i*LY+j order
		thrust::copy(qsquares_ptr,qsquares_ptr+QNN,q2.begin());
		//CUDA_SAFE_CALL(cudaThreadSynchronize());
		//Sort the recentry calculated structure factor according to q2, this will sort the keys as well
		thrust::sort_by_key(q2.begin(),q2.end(),sf_ptr); //This is OK!!
		//reduce_by_key CANNOT support in_place keys nor values. Estimated time to realize this: 12hs
		new_end = thrust::reduce_by_key(q2.begin(),q2.end(),sf_ptr,qq_ptr,sf_out_ptr);
		assert(new_end.first-qq_ptr == qqssize);
		thrust::transform(sf_out_ptr,sf_out_ptr+qqssize, \
			occurence_ptr, sf_out_ptr, thrust::divides<REAL>()); 
		//we can also make this division at the very end

		thrust::host_vector<REAL> B(qqssize);
		thrust::copy(sf_out_ptr,sf_out_ptr+qqssize,B.begin());
		int sfdatasize = qqssize; // MIN(qqssize,NN/128);
			for(int k=0;k<sfdatasize;k++){
				h_sf[(index_t-i*twtd)*sfdatasize+k] += B[k]; 
			}
			h_sf_counter[index_t-i*twtd] += 1;
		}
	}

	void CalculatePrintTrajectoriesStructureFactor(ofstream &fstr, const unsigned long int t, const unsigned long int tw){
		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(LX/TILE_X, LY/TILE_Y);
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);

		Kernel_CalculateTrajectoriesSF<<<dimGrid, dimBlock>>>(d_tracers, d_tracerstw, d_sf_aux, LX, LY);
		//CUDA_SAFE_CALL(cudaThreadSynchronize()); //TODO: Check if needed

		//The clever way, sorting the array
		//Do a copy of q^2 in the i*LY+j order
		thrust::device_ptr<unsigned int> qsquares_ptr(d_qsquares);
		thrust::device_vector<unsigned int> q2(NN);
		thrust::copy(qsquares_ptr,qsquares_ptr+NN,q2.begin());

		//CUDA_SAFE_CALL(cudaThreadSynchronize());
		thrust::device_ptr<REAL> sf_ptr(d_sf_aux);

		//Sort the recentry calculated structure factor according to q2, this will sort the keys as well
		thrust::sort_by_key(q2.begin(),q2.end(),sf_ptr);

		//thrust::device_ptr<unsigned int> q2_ptr(&q2[0]);
		//thrust::device_ptr<unsigned int> qq_ptr = thrust::device_malloc<unsigned int>(NN);

		//thrust::device_vector<unsigned int> qq_out(NN);
		thrust::device_ptr<unsigned int> qq_ptr(d_qsquares_out);
		thrust::device_ptr<REAL> sf_out_ptr(d_sf_out);

		//reduce_by_key CANNOT support in_place keys nor values. Estimated time to realize this: 12hs
		//thrust::pair<thrust::device_ptr<unsigned int>, thrust::device_ptr<REAL> > new_end = 
		thrust::reduce_by_key(q2.begin(),q2.end(),sf_ptr,qq_ptr,sf_out_ptr);
		//later on we can neglect qq_ptr and use  thrust::make_discard_iterator()
		//assert(new_end.first-qq_ptr == qqssize);

		thrust::device_ptr<unsigned int> occurence_ptr(d_qsquares_occurence);
		thrust::transform(sf_out_ptr,sf_out_ptr+qqssize, \
				thrust::make_transform_iterator(occurence_ptr, _1*1.0), sf_out_ptr, \
				thrust::divides<REAL>()); 
		//we can also make this division at the very end

//Check
		thrust::host_vector<unsigned int> A(qqssize);
		thrust::host_vector<REAL> B(qqssize);
		thrust::copy(qq_ptr,qq_ptr+qqssize,A.begin());
		thrust::copy(sf_out_ptr,sf_out_ptr+qqssize,B.begin());
		fstr << "### Structure factor as a function of q^2 for time t" << std::setprecision(15) << t*TIME_STEP << '\n';
		for(int i=0; i<qqssize/50.; i++) fstr << (2*M_PI/(REAL)LX)*(2*M_PI/(REAL)LX)*A[i] << " " \
		<< B[i] << '\n';
		fstr << endl;
	}

	#ifdef VISUALIZATION
	void ComputeStructureFactor(){
		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(LX/TILE_X, LY/TILE_Y/2+1);
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);
		CUDAkernel_ComputeStructureFactor<<<dimGrid, dimBlock>>>(d_tracers, d_tracerstw, d_Sq2d, \
								LX, (LY/2+1));
	}

	void ComputeStructureFactorImage(){
		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(LX/TILE_X, LY/TILE_Y);
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);
		CUDAkernel_ComputeStructureFactorImage<<<dimGrid, dimBlock>>>(d_Sq2d, d_Sq2dimage, LX, LY);
		CUDA_SAFE_CALL(cudaMemcpy(h_Sq2dimage, d_Sq2dimage, sizeof(REAL)*LX*LY, \
				cudaMemcpyDeviceToHost));
	}
	#endif //VIS

	#endif //SQ

	#endif //TRA

	#ifdef WAITING_TIMES
	void ComputeWaitingTimeDistribution(const unsigned long int t){
		if(t==0){
		thrust::device_ptr<unsigned int> actual_tw_ptr(d_actual_tw);
		thrust::fill(actual_tw_ptr,actual_tw_ptr+NN,0);
		}

		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(LX/TILE_X, LY/TILE_Y);
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);
		Kernel_ComputeWaitingTimeDistribution<<<dimGrid, dimBlock>>>(d_arn, d_p_of_tw, d_actual_tw, t);
	}
	#endif

	#ifdef GLOBAL_WAITING_TIMES
	void ComputePrintGlobalWaitingTimeDistribution(const unsigned long int t, unsigned int &t_counter, unsigned int &t_aux){

		char filename[128];
		ofstream fstr, fstr2;
		sprintf(filename, "PofTau%s.dat",root);
		fstr.open(filename, ios::out | ios::app);
		//ofstream fstr(filename);
		#ifdef TRACERS
			sprintf(filename, "PofU%s.dat",root);
			fstr2.open(filename, ios::out | ios::app);
			//ofstream fstr2(filename);
		#endif

		thrust::device_ptr<bool> actual_state_ptr(d_state);
		thrust::device_ptr<bool> stored_state_ptr(d_state_aux);

		#ifdef TRACERS
		thrust::device_ptr<REAL2> actual_tracers_ptr(d_tracers);
		thrust::device_ptr<REAL2> stored_tracers_ptr(d_tracers_aux);
		thrust::device_vector<REAL> d_displacements(NTRACERS);
		thrust::host_vector<REAL> h_displacements(NTRACERS);
		#endif

		if(t==0){
			fstr << "### 0:Waiting times in units of" << TIME_STEP << " 1:actual real time" << '\n';
			thrust::copy(actual_state_ptr, actual_state_ptr + NN, stored_state_ptr);
			t_aux=t;
			#ifdef TRACERS
			thrust::copy(actual_tracers_ptr, actual_tracers_ptr + NTRACERS, stored_tracers_ptr);
			#endif
		}else{
			bool areequal = thrust::equal(actual_state_ptr, actual_state_ptr + NN, stored_state_ptr);
			if (!areequal){
				fstr << (t-t_aux) << "  " << std::setprecision(15) << t*TIME_STEP << '\n';
				thrust::copy(actual_state_ptr, actual_state_ptr + NN, stored_state_ptr);
				t_aux = t;
				t_counter+=1;
				#ifdef TRACERS
				thrust::transform(actual_tracers_ptr, actual_tracers_ptr+NTRACERS, stored_tracers_ptr,\
				d_displacements.begin(), euler_distance<REAL>());
				thrust::copy(actual_tracers_ptr, actual_tracers_ptr + NTRACERS, stored_tracers_ptr);
				if (t_counter*NTRACERS<DISPLFIELDMAXCOUNT){
				thrust::copy(d_displacements.begin(), d_displacements.end(), h_displacements.begin());
				for(unsigned int i=0; i<NTRACERS; i++) fstr2 << h_displacements[i] << '\n';
				}
				#endif
			}
		}
		fstr.close();
		#ifdef TRACERS
		fstr2.close();
		#endif
	}
	#endif


////////////////////////////////////////////////////////////////////////////////////////////////

//////////---------------------------Copy Functions------------------///////////////////////////

	/* transfer from GPU to CPU memory */
	void CpyDeviceToHostSigmaAndState(){
		size_t size_b = NN * sizeof(bool);
		size_t size_r = NN * sizeof(REAL);
		CUDA_SAFE_CALL(cudaMemcpy(h_state, d_state, size_b, cudaMemcpyDeviceToHost));
		CUDA_SAFE_CALL(cudaMemcpy(h_sigma, d_sigma, size_r, cudaMemcpyDeviceToHost));
	}

	void CpyDeviceToHostSigma(){
		size_t size_r = NN * sizeof(REAL);
		CUDA_SAFE_CALL(cudaMemcpy(h_sigma, d_sigma, size_r, cudaMemcpyDeviceToHost));
	}

	#ifdef TRACERS
	void CpyDeviceToHost1Step(){
		size_t size_r = NN * sizeof(REAL);
		CUDA_SAFE_CALL(cudaMemcpy(h_ux_r, d_ux_r, size_r, cudaMemcpyDeviceToHost));
		CUDA_SAFE_CALL(cudaMemcpy(h_uy_r, d_uy_r, size_r, cudaMemcpyDeviceToHost));
		CUDA_SAFE_CALL(cudaMemcpy(h_dsigma, d_dsigma_r, size_r, cudaMemcpyDeviceToHost));
	}
	#endif


////////////////////////////////////////////////////////////////////////////////////////////////

//////////---------------------------Print Functions-----------------///////////////////////////

	unsigned long int timeforprint(unsigned long int t){
		unsigned long int a = 10000000000+t;
		return a;
	}

	void DeclareFileNames(const unsigned long int t_transient, const unsigned long int t_transient2, const unsigned long int t_waiting, \
		const unsigned long int t_run, const unsigned long int t_data, const REAL temperature){

	#ifdef RANDOM_EVENTS
	sprintf(root, "_LX%iLY%itt%lutw%lutr%lutd%luA%.8fDt%.2f",LX,LY,t_transient2,t_waiting,t_run,t_data,temperature,TIME_STEP);
	#else
	sprintf(root, "_LX%iLY%itt%lutw%lutr%lutd%lutemp%.4fDt%.2f",LX,LY,t_transient2,t_waiting,t_run,t_data,temperature,TIME_STEP);
	#endif
	assert(strlen(root)<96);

	#ifdef TRACERS
		sprintf(filename_msd_tmp, "MSD%s-tmp.dat",root);
		sprintf(filename_msd, "MSD%s.dat",root);
		cout << "### " << filename_msd_tmp << '\n';
		#ifdef SQ
			sprintf(filename_sf_q_tmp, "Sk_vs_Q%s-tmp.dat", root);
			sprintf(filename_sf_q, "Sk_vs_Q%s.dat",root);
			cout << "### " << filename_sf_q << '\n';
			sprintf(filename_sf_t_tmp, "Sk_vs_t%s-tmp.dat",root);
			sprintf(filename_sf_t, "Sk_vs_t%s.dat",root);
			cout << "### " << filename_sf_t << '\n';
		#endif
		#ifdef PRINT_TRACERS
			sprintf(filename_tr, "tracers%s.dat",root);
			cout << "### " << filename_tr << '\n';
		#endif
	#endif
	cout.flush();
	}

	void PrintState(ofstream &fstr){
		for(int i=0;i<LX;i++){
			for(int j=0;j<LY;j++){
				int k=i*LY+j;
				//fstr << h_state[k] << " ";
				fstr << h_state[k] << '\n';
			}
			//fstr << '\n';
		}
		fstr << endl;
	}

	void PrintSigma(ofstream &fstr){
		for(int i=0;i<LX;i++){
			for(int j=0;j<LY;j++){
				int k=i*LY+j;
				//fstr << h_sigma[k] << " ";
				fstr << h_sigma[k] << '\n';
			}
			//fstr << '\n';
		}
		fstr << endl;
	}

	#ifdef WAITING_TIMES
	void PrintPofTW(const unsigned long int t){
		char filename[128];

		sprintf(filename, "PofTW%srounds%lu.dat",root,timeforprint(t));
		ofstream fstr(filename);

		CUDA_SAFE_CALL(cudaMemcpy(h_p_of_tw, d_p_of_tw, NN*sizeof(unsigned int), cudaMemcpyDeviceToHost));
		for(int i=0;i<LX;i++){
			for(int j=0;j<LY;j++){
				int k=i*LY+j;
				fstr << h_p_of_tw[k] << '\n';
			}
		}
		fstr << endl;
	}
	#endif

	#ifdef TRACERS
	void PrintTracers(unsigned long int t, int howmany){
		ofstream fstr;
		fstr.open(filename_tr, ios::out | ios::app);

		CUDA_SAFE_CALL(cudaMemcpy(h_tracers, d_tracers, howmany*sizeof(REAL2), cudaMemcpyDeviceToHost));
		fstr << "### tracers at time" << std::setprecision(15) << t*TIME_STEP << '\n';
		for(int i=0;i<howmany;i++){
				fstr << h_tracers[i].x << " " << h_tracers[i].y << " " ;
		}
		fstr << endl;
		fstr.close();
	}

	void PrintMeanSquareDisplacement(const bool av, const unsigned long int t_data, int printsize){

		ofstream fstr;
		if(av){fstr.open(filename_msd, ofstream::out);}else{fstr.open(filename_msd_tmp, ofstream::out);}

		fstr << "## 0:Time  1:Mean Square Displacement 2:Events counter" << '\n';
		for(int i=1; i<printsize; i++){ //Avoid point (0,0)
		REAL time = i*t_data*TIME_STEP;
		if (h_msd_counter[i]!=0) fstr << time << " " << h_msd[i]/(double)h_msd_counter[i] << " " \
		<< h_msd_counter[i] << " " << '\n';
		}
		fstr.flush();
		fstr.close();
	}

	#ifdef SQ
	void PrintTrajectoriesStructureFactor_vs_Q(const bool av, const unsigned long int t_data, int printsize){

		ofstream fstr;
		if(av){fstr.open(filename_sf_q, ofstream::out);}else{fstr.open(filename_sf_q_tmp, ofstream::out);}

		//times to be used for sizes
		//int datasize = int((RUN_TIME-WAITING_T)/WAITING_T)+1;
		int sfdatasize = qqssize; //MIN(qqssize,NN/128);

		int log_step = 1;
//		for(int i=0; i<printsize; i++){
		for(int i=0; i<printsize; i+=log_step){
			REAL time = i*t_data*TIME_STEP;
			fstr << "## SF for time window" << time << '\n';
			fstr << "@type xyz" << '\n';
			for(int k=0; k<sfdatasize; k++){
				if (h_sf_counter[i]!=0) fstr << (2*M_PI/(REAL)LX)*(2*M_PI/(REAL)LX)*h_qsquares_out[k] << " " \
				<< h_sf[i*sfdatasize+k]/(double)h_sf_counter[i] << " " << time << '\n';
				}
			fstr << '\n';
		if (i==log_step*100) log_step*=10;
		}
		fstr.flush();
		fstr.close();
	}

	void PrintTrajectoriesStructureFactor_vs_t(const bool av, const unsigned long int t_data, int printsize){

		ofstream fstr;
		if(av){fstr.open(filename_sf_t, ofstream::out);}else{fstr.open(filename_sf_t_tmp, ofstream::out);}

		//times to be used for sizes
		//int datasize = int((RUN_TIME-WAITING_T)/WAITING_T)+1;
		int sfdatasize = qqssize; //MIN(qqssize,NN/128);

		int log_step = 1;
//		for(int k=0; k<sfdatasize; k++){
		for(int k=0; k<sfdatasize; k+=log_step){
			//fstr << "## SF for time window" << i*tw*TIME_STEP //fixed 8/8/13
			REAL qsquare= (2*M_PI/(REAL)LX)*(2*M_PI/(REAL)LX)*h_qsquares_out[k];
			fstr << "## SF for q^2 = " << qsquare << '\n';
			fstr << "@type xyz" << '\n';
			for(int i=0; i<printsize; i++){
				if (h_sf_counter[i]!=0) fstr << i*t_data*TIME_STEP << " " \
				<< h_sf[i*sfdatasize+k]/(double)h_sf_counter[i] << " " << qsquare << '\n';
				}
			fstr << '\n';
		if (k==log_step*100) log_step*=10;
		}
		fstr.flush();
		fstr.close();
	}
	#endif

	void PrintDsigmaAndDu(unsigned long int t){

		char filename[128];
		sprintf(filename, "Dsigma%srounds%lu.dat", root, timeforprint(t));
		ofstream fstr(filename);
		sprintf(filename, "DU%srounds%lu.dat", root, timeforprint(t));
		ofstream fstr2(filename);

		// Retrieve result from device and store it in host array
		CpyDeviceToHost1Step();

		for(int i=0;i<LX;i++){
			for(int j=0;j<LY;j++){
				int k=i*LY+j;
				fstr << i << " " << j << " " << h_dsigma[k] << '\n';
			}
		}
		fstr << '\n';

		for(int i=0;i<LX;i++){
			for(int j=0;j<LY;j++){
				int k=i*LY+j;
				fstr2 << i << " " << j << " " << h_ux_r[k] << " " << h_uy_r[k] << " " << \
				sqrt(h_ux_r[k]*h_ux_r[k]+h_uy_r[k]*h_uy_r[k]) << '\n';
			}
			//fstr << '\n';
		}
		fstr << endl;
	}

	#endif

	#ifdef VISUALIZATION
	void Visualization(unsigned long int t){
		CpyDeviceToHostSigmaAndState();

		char filename[128];
		unsigned long int frameindex = timeforprint(t);
		sprintf(filename, "framesigma.ppm");
		PrintPicture(filename,0);
		sprintf(filename, "framestate.ppm");
		PrintPicture(filename,1);
		#ifdef JPEG
		char cmd[256];
		sprintf(cmd, "convert framesigma.ppm framestate.ppm +append frame_Sigma-State%lu.ppm", \
				frameindex);
		int i = system(cmd);
		//sprintf(cmd, "rm framesigma.ppm framestate.ppm");
		//system(cmd);
		#endif
	}

	void PrintPicture(char *picturename, int who){
		if (who==0) writePPMbinaryImage(picturename, h_sigma);
		if (who==1) writePPMbinaryImage_bool(picturename, h_state);
	/*	#ifdef JPEG
		char cmd[200];
		sprintf(cmd, "convert %s.ppm %s.jpg", picturename, picturename);
		system(cmd);
		sprintf(cmd, "rm %s.ppm", picturename);
		system(cmd);
		#endif
	*/
	};

	void PrintRawLocalStressAndState(unsigned long int t, REAL time){
		size_t size_r = 32*sizeof(REAL);
		size_t size_b = 32 * sizeof(bool);
		cudaMemcpy(h_sigma, d_sigma, size_r ,cudaMemcpyDeviceToHost);
		cudaMemcpy(h_state, d_state, size_b ,cudaMemcpyDeviceToHost);
		cout << (time*TIME_STEP) << " " << t*TIME_STEP << " " \
		<< h_sigma[0] << " "  << h_state[0] << " "  << h_sigma[1] << " "  << h_state[1] << " "  
		<< h_sigma[2] << " "  << h_state[2] << " "  << h_sigma[3] << " "  << h_state[3] << " "
		<< h_sigma[4] << " "  << h_state[4] << " "  << h_sigma[5] << " "  << h_state[5] << " "
		<< h_sigma[6] << " "  << h_state[6] << " "  << h_sigma[7] << " "  << h_state[7] << " "
		<< h_sigma[8] << " "  << h_state[8] << " "  << h_sigma[9] << " "  << h_state[9] << " "
		<< h_sigma[10] << " "  << h_state[10] << " "  << h_sigma[11] << " "  << h_state[11] << " "
		<< h_sigma[12] << " "  << h_state[12] << " "  << h_sigma[13] << " "  << h_state[13] << " "
		<< '\n';
	}

	void PrintImageStructureFactor(char *picturename, unsigned long int t){
		writePPMbinaryImage(picturename, h_Sq2dimage);
		#ifdef JPEG
		char cmd[256];
		sprintf(cmd, "convert Sq%lu.ppm -resize %dx%d\\! Sq%lu.ppm", timeforprint(t), LX, LX, timeforprint(t));
		int i = system(cmd);
		sprintf(cmd, "convert Sq%lu.ppm Sq%lu.jpg", timeforprint(t), timeforprint(t));
		i = system(cmd);
		sprintf(cmd, "rm Sq%lu.ppm", timeforprint(t));
		i = system(cmd);
		#endif
	}
	#endif

	#ifdef RAW_DATA
	void PrintSigmaAndState(const unsigned long int t){
		char filename[128];
		#ifndef VISUALIZATION
		CpyDeviceToHostSigmaAndState();
		#endif
		sprintf(filename, "Sigma%srounds%lu.dat",root, timeforprint(t));
		//cout << "### " << filename << endl;
		ofstream out(filename);
		PrintSigma(out);
		sprintf(filename, "State%srounds%lu.dat",root, timeforprint(t));
		//cout << "### " << filename << endl;
		ofstream out1(filename);
		PrintState(out1);
	}
	#endif

	void PrintLastStates(const unsigned long int t){
		// Retrieve result from device and store it in host array
		char filename[128];

		CpyDeviceToHostSigmaAndState(); //copy state and sigma

		sprintf(filename, "Sigma%srounds%lu.dat",root,timeforprint(t));
		cout << "### " << filename << '\n';
		ofstream out(filename);
		PrintSigma(out);

		sprintf(filename, "State%srounds%lu.dat",root,timeforprint(t));
		cout << "### " << filename << '\n';
		ofstream out2(filename);
		PrintState(out2);

		// Print frame for visualization
		#ifdef VISUALIZATION
		sprintf(filename, "lastframe.ppm");
		PrintPicture(filename,0);
		#endif

		#ifdef TRACERS
		PrintDsigmaAndDu(t); //Includes Copy
		#endif
	}

////////////////////////////////////////////////////////////////////////////////////////////////

//////////---------------------------Cleaning Issues-----------------///////////////////////////


	~mesoplastic_model(){

		/* frees CPU memory */
		cudaFree(h_state); cudaFree(h_sigma); cudaFree(h_propagator);
		#ifdef TRACERS
		free(h_msd); 
		free(h_msd_counter);
		#ifdef SQ
		free(h_sf); free(h_sf_counter);
		#endif
		cudaFree(h_tracers);
		cudaFree(h_dsigma);
		cudaFree(h_ux_r); cudaFree(h_uy_r);
		#endif
		#ifdef VISUALIZATION
		cudaFree(h_Sq2dimage);
		#endif
		#ifdef WAITING_TIMES
		cudaFree(h_p_of_tw);
		#endif

		/* frees GPU memory */
		cudaFree(d_state); cudaFree(d_sigma); cudaFree(d_strain_sign);
		cudaFree(d_dsigma); cudaFree(d_dsigma_r);
		cudaFree(d_epsilon_dot); cudaFree(d_epsilon_dot_r);
		cudaFree(d_propagator);
		#ifdef KMC
		cudaFree(d_tower);
		#endif
		#ifdef TRACERS
		cudaFree(d_tracers); cudaFree(d_tracerstw);
		cudaFree(d_ux); cudaFree(d_uy);
		cudaFree(d_ux_r); cudaFree(d_uy_r);
		cudaFree(d_oseen);
			#ifdef SQ
			cudaFree(d_sf_aux); cudaFree(d_sf_out);
			cudaFree(d_qsquares); cudaFree(d_qsquares_out);
			cudaFree(d_qsquares_sorted); cudaFree(d_qsquares_occurence);
			#ifdef VISUALIZATION
			cudaFree(d_Sq2d);
			cudaFree(d_Sq2dimage);
			#endif
			#endif
		#endif
		#ifdef WAITING_TIMES
		cudaFree(d_arn); cudaFree(d_actual_tw); cudaFree(d_p_of_tw);
		#endif
		#ifdef GLOBAL_WAITING_TIMES
		cudaFree(d_state_aux);
			#ifdef TRACERS
			cudaFree(d_tracers_aux);
			#endif
		#endif

		cufftDestroy(plan_r2c);
		cufftDestroy(plan_c2r);

	}
};


#endif /*  MESOPLASTIC_LIBRARY_CUH */
