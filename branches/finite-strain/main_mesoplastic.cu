/*
 * main_mesoplastic.cu
 *
 *  started on: May 10, 2013
 *      Author: eze
 * .....based on: main.cu by Kirsten
 */

//TODO: defines in Makefile
#ifndef LX
#define LX 256
#endif
#ifndef LY
#define LY LX 
#endif
//TODO: check for rectangular geometries //DONE, seems that it works //check again

#define NN (LX*LY)
#define SCALE (1.0/NN)

#ifndef TIME_STEP
#define TIME_STEP 0.01
#endif

//#define MAX_STEPS (int) (RUN_TIME/TIME_STEP + 0.1)
//#define TRAN_STEPS (int) (TIME_TO_REACH_NESS/TIME_STEP + 0.1)

#ifndef STRESS_THRESHOLD
#define STRESS_THRESHOLD 1.0
#endif

#ifndef TAU_ELAST
#define TAU_ELAST 1.0
#endif

#ifndef TAU_PLAST
#define TAU_PLAST 1.0
#endif

#ifndef SHEAR_RATE
#define SHEAR_RATE 1.0
#endif
// NESS: Non-Equilibrium Steady State
#define TIME_TO_REACH_NESS (1/SHEAR_RATE)

#ifndef TRANSIENT_TIME
//#define TRANSIENT_TIME (50/SHEAR_RATE)
//#define TRANSIENT_TIME 100/SHEAR_RATE
//#define TRANSIENT_TIME 1000
#define TRANSIENT_TIME 0
#endif

#ifndef RUN_TIME
#define RUN_TIME (600/SHEAR_RATE)
//#define RUN_TIME 1000/SHEAR_RATE
//#define RUN_TIME 10000
#endif

#ifndef DATA_DTIME
#define DATA_DTIME (1/SHEAR_RATE)
//#define DATA_DTIME 100/SHEAR_RATE
//#define DATA_DTIME 100
#endif

#ifdef ACTIVATED_DYNAMICS
	#ifndef CRITICAL_STRESS
	#define CRITICAL_STRESS 0.01
	#endif
	#ifndef TEMP_EFF
	#define TEMP_EFF 0.01
	#endif
#endif

#ifdef CORRELATION
	# define WAITING_T (2/SHEAR_RATE)
//	# define WAITING_T 100/SHEAR_RATE
//	# define WAITING_T 100
	# define NTW 300
#endif

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

#ifdef TRACERS
	#define NTRACERS MIN(LX*LY,65536)
	#define QMAX (8*M_PI*M_PI)
	#define QSMAX 2*(LX-1)*(LY-1)
#endif

//TODO: Tunne this to optimize
#define TILE_X 4
#define TILE_Y 64
#define TILE 128

#ifndef CUDA_DEVICE
#define CUDA_DEVICE 0
#endif

// Functions
#define MAX(a,b) (((a)<(b))?(b):(a))	// maximum
#define MIN(a,b) (((a)<(b))?(a):(b))	// minimum
#define ABS(a) (((a)< 0)?(-a):(a))	// absolute value

// Hardware parameters for Tesla c2075 (GF100) cc 2.0
#define SHARED_PER_BLOCK 49152
#define WARP_SIZE 32
#define THREADS_PER_BLOCK 1024
#define BLOCKS_PER_GRID 65535

#include "mesoplastic_library.cuh"
#include "../common/timers.hpp"

REAL TimeLoop(mesoplastic_model &T, const REAL gamma_p, const unsigned int t_transient, const unsigned int t_waiting,\
						const unsigned int t_run, const unsigned int t_data){

	unsigned int count=0;
	double ssstress=0;
	#ifdef CORRELATION
	unsigned int tw=0;
	REAL ro_zero2=0;
	#endif
	#ifdef TRACERS
	unsigned int tw_counter=0;
	#ifdef SQ
	char filename1[100];char filename1b[100];
	#endif
	char filename2[100];char filename3[100];
	#ifdef DOUBLE_PRECISION
		#ifdef SQ
//		sprintf(filename1, "Sk_LX%iLY%igammadot%.5fDt%.2f-DP.dat",LX,LY,gamma_p,TIME_STEP);
		sprintf(filename1b, "Sk_LX%iLY%igammadot%.5fDt%.2f-av-DP.dat",LX,LY,gamma_p,TIME_STEP);
		#endif
		sprintf(filename2, "MSD_LX%iLY%igammadot%.5fDt%.2f-DP.dat",LX,LY,gamma_p,TIME_STEP);
		sprintf(filename3, "tracers_LX%iLY%igammadot%.5fDt%.2f-DP.dat",LX,LY,gamma_p,TIME_STEP);
		#else
		#ifdef SQ
		sprintf(filename1, "Sk_LX%iLY%igammadot%.5fDt%.2f-g.dat",LX,LY,gamma_p,TIME_STEP);
		sprintf(filename1b, "Sk_LX%iLY%igammadot%.5fDt%.2f-av-g.dat",LX,LY,gamma_p,TIME_STEP);
		#endif
		sprintf(filename2, "MSD_LX%iLY%igammadot%.5fDt%.2f-g.dat",LX,LY,gamma_p,TIME_STEP);
		sprintf(filename3, "tracers_LX%iLY%igammadot%.5fDt%.2f-g.dat",LX,LY,gamma_p,TIME_STEP);
	#endif
	#ifdef SQ
//	ofstream file1(filename1);
	ofstream file1b(filename1b);
	#endif
	ofstream file2(filename2);
	ofstream file3(filename3);
	#endif

	for(unsigned int i=0; i<t_transient; i++){
		T.UpdateStateVariables(i);
		T.ComputePlasticStrain();
		T.TransformToFourierSpace();
		T.Convolution();
		T.AntitransformFromFourierSpace();
		T.EulerIntegrationStep(gamma_p);
	}

	//#ifdef CORRELATION
	//assert(t_waiting>=t_transient);
	//#endif

	unsigned int t=0;
	while(t<t_run+1){
	//cout << t << " ";
	//Measurements
		#ifdef CORRELATION
		if ((t<t_waiting) && (t%t_data==0)){
			ssstress += T.ComputeSsstress();
			count++;
		}

		if(t==t_waiting){
			ssstress = ssstress/(double)count;
			tw=t;
			cout << "## Waiting time for gamma_dot " << gamma_p << " tw= " << tw << " WAITING_T= " << WAITING_T <<endl;
			cout << "## 1:gamma(time*gamma_p) 2:correlation 3:time" << endl;
			ro_zero2 = T.ComputeFluctuationsAtTw(gamma_p, ssstress); //gamma_p not used
		}

		if((t>=t_waiting) && ((t-tw)%t_data==0)){
			REAL corr = T.ComputeAutoCorrelationFunction(gamma_p, ssstress, ro_zero2); //gamma_p not used
			REAL gamma = (t-tw)*TIME_STEP*gamma_p;
			cout << gamma << " " << corr << " " << (t-tw)*TIME_STEP << endl;
		}
		#else
		if(t%t_data==0){
			ssstress+= T.CalculateSigmaAv()/(REAL) NN;
			count+=1;
		}
		#endif


		#ifdef TRACERS
		if((t>0) && (t%t_waiting==0) && (tw_counter<NTW)){
			T.SetTracersAtTW(tw_counter);
			tw_counter++;
		}

		if((t>0) && (t%t_waiting==0)){
			T.CalculateAccumulateMeanSquareDisplacement(t/t_waiting,tw_counter);
			#ifdef SQ
			T.CalculateAccumulateTrajectoriesStructureFactor(t/t_waiting,tw_counter);
			#endif
			T.PrintTracers(file3,t,64);
		/*	if (t%(4*t_waiting)==0 && t<40*t_waiting){
				T.CalculatePrintTrajectoriesStructureFactor(file1, t, t_waiting);
			}
		*/
		}

/*		if((t>2*t_waiting) && ((t-t_waiting)%t_data==0)){
		//if((t>=t_waiting) && (t%t_waiting==0)){

				//T.CalculateTrajectoriesStructureFactor(t, t_waiting);
				//if ((t-t_waiting)%(2*t_data)==0) T.PrintStructureFactor(file1, t, t_waiting);

				//T.PrintSmallQStructureFactor(file, (t-tw)*TIME_STEP);
				//T.ComputeStructureFactor();
				//T.ComputeStructureFactorImage();
				//sprintf(filename, "Sq%.2f.ppm", 100000000+(t-tw)*TIME_STEP);
				//T.PrintImageStructureFactor(filename,(t-tw)*TIME_STEP);
		}
*/
		#endif



	//Dynamics
		#ifdef ACTIVATED_DYNAMICS
		int firstplasticactive = T.CheckForPlasticActivity(); 
		// It will return the position of the first 1 or the size of the array (NN) if there are none
		if(firstplasticactive==NN){
			//cout << "### KMC move, at time" << t*TIME_STEP << endl;
			t+=(int)(T.ConstructActivatedProbabilityTowerChooseOneEventAndUpdate(t)/TIME_STEP +0.5);
		}else{
			//cout << "### Normal setp, at time" << t << endl;
			T.UpdateStateVariablesActivated(t);
			t++;
		}
		#else
		// Update n(x,z;t) state variables: 0/1 corresponds to elastic/plastic
		T.UpdateStateVariables(t);
		t++;
		#endif

		// Calculation of the plastic strain "epsilon_dot" ~ n*sigma
		T.ComputePlasticStrain();
		// Calculation of the convolution with the propagator and the plastic strain
		// we transform the plastic strain epsilon_dot
		T.TransformToFourierSpace();
		//convolution
		T.Convolution();
		#ifdef TRACERS
		//displacement field u
		T.CalculateDisplacementField();
		#endif
		// we antitransform the result of the convolution "delta_sigma" (and eventually delta_u_x/y)
		T.AntitransformFromFourierSpace();
		// Update of the shear stress "sigma" with a simple Euler integration using delta_sigma and a \
		global elastic loading, the shear rate "gamma_dot" 
		T.EulerIntegrationStep(gamma_p);
		#ifdef TRACERS
		//Update tracers positions preserving PBC
		T.UpdateTracers();
		#endif
	}
	cout << " " << endl;

	#ifdef TRACERS
	T.PrintMeanSquareDisplacement(file2, t_waiting);
	#ifdef SQ
	T.PrintTrajectoriesStructureFactor(file1b, t_waiting);
	#endif
	#endif

	#ifdef CORRELATION
	return ssstress;
	#else
	return ssstress/(REAL) count;
	#endif
}

// main routine
int main(){

	//TODO: complete asserts
	assert(TILE_X%2==0);
	assert(TILE_Y%2==0);
	assert(LX%TILE_X==0);
	assert(LY%TILE_Y==0);
	//assert(MAX_STEPS%DATA==0);
	//We've assumed a small temporal step in the integration scheme
	assert(TIME_STEP<=0.1);
	#ifdef TRACERS
	assert(NN%NTRACERS==0);
	assert(NTRACERS%TILE==0);
	assert(LX==LY); //TODO: Solve this. The thing is the definiton of the q-squares and their ordering.
	#endif

	// Set the GPGPU computing device
	#ifdef CUDA_DEVICE
	CUDA_SAFE_CALL(cudaSetDevice(CUDA_DEVICE));
	#endif

	// Choosing less "shared memory", more cache.
	//CUDA_SAFE_CALL(cudaThreadSetCacheConfig(cudaFuncCachePreferL1)); 

	#ifdef ACTIVATED_DYNAMICS
	// Initialize random(), it will be used for the KMC Poissonian time
	srandom(RANDOM_SEED);
	#endif

	std::setprecision (15);

	//Print simulation parameters
	cout << "### LX ................................: " << LX                << endl;
	cout << "### LY ................................: " << LY                << endl;
	cout << "### Time Step..........................: " << TIME_STEP         << endl;
	cout << "### Stress Thershold...................: " << STRESS_THRESHOLD  << endl;
	cout << "### Recovery rate unit (tau_elast).....: " << TAU_ELAST         << endl;
	cout << "### Activation rate unit (tau_plast)...: " << TAU_PLAST         << endl;
	cout << "### Shear rate ........................: " << SHEAR_RATE        << endl;
	cout << "### Transient time ....................: " << TRANSIENT_TIME    << endl;
	cout << "### Running time ......................: " << RUN_TIME          << endl;
	cout << "### Data aquiring time.................: " << DATA_DTIME        << endl;
	cout << "### SEED Philox .......................: " << PHILOX_SEED       << endl;
	#ifdef ACTIVATED_DYNAMICS
	cout << "### SEED Random()......................: " << RANDOM_SEED       << endl;
	cout << "### Critical stress cuttof.............: " << CRITICAL_STRESS   << endl;
	cout << "### Effective temperature .............: " << TEMP_EFF          << endl;
	#endif

	// Our class
	mesoplastic_model T;

	// Turn on cronometer
	cpu_timer clock;
	clock.tic();

	// Initialization
	//T.Initialize();
	T.InitializeOnDevice();
	#ifdef TRACERS
	T.InitializeTracersAndAll();
	#endif

	//Set integer times //HARDCODED, if chaged take care of h_msd...
	unsigned int t_transient = (int)(TRANSIENT_TIME/TIME_STEP+0.5);
	unsigned int t_waiting = (int)(WAITING_T/TIME_STEP+0.5);
	unsigned int t_run = (int)(RUN_TIME/TIME_STEP+0.5);
	unsigned int t_data = (int)(DATA_DTIME/TIME_STEP+0.5);

	//Time loop call, returns averaged stress
	REAL sigma_av = TimeLoop(T, SHEAR_RATE, t_transient, t_waiting, t_run, t_data);

	//TODO:Visualization in time!!!!
	/* Print frames for visualization (includes cudaMemCpyDtoH */
	#ifdef VISUALIZATION
		#warning VISUALIZATION ON HERE
		T.Visualization(SHEAR_RATE);
	#endif

	#ifdef RAW_DATA
/*		// Retrieve result from device and store it in host array
		T.CpyDeviceToHost();
		// Print results
		//T.PrintResults();
		string output_file = "sigma.dat";
		ofstream out(output_file.c_str());
		T.PrintSigma(out);
		output_file = "state.dat";
		ofstream out2(output_file.c_str());
		T.PrintState(out2);
		// Print frame for visualization
		char filename [200];
		sprintf(filename, "lastframe.ppm");
		T.PrintPicture(filename,0);
*/
		// Retrieve result from device and store it in host array
		REAL *h_dsigma; REAL *h_deltau_x_r; REAL *h_deltau_y_r;
		h_dsigma = (REAL *)malloc(sizeof(REAL)*LX*LY);
		h_deltau_x_r = (REAL *)malloc(sizeof(REAL)*LX*LY);
		h_deltau_y_r = (REAL *)malloc(sizeof(REAL)*LX*LY);

		T.CpyDeviceToHost1Step(h_dsigma,h_deltau_x_r,h_deltau_y_r);

		// Print results
		//T.PrintResults();
		string output_file = "dsigma.dat";
		ofstream fstr(output_file.c_str());

		for(int i=0;i<LX;i++){
			for(int j=0;j<LY;j++){
				int k=i*LY+j;
				fstr << h_dsigma[k] << " ";
			}
			fstr << endl;
		}
		fstr << endl;

		output_file = "du.dat";
		ofstream fstr2(output_file.c_str());

		for(int i=0;i<LX;i++){
			for(int j=0;j<LY;j++){
				int k=i*LY+j;
				fstr2 << i << " " << j << " " << h_deltau_x_r[k] << " " << h_deltau_y_r[k] << endl;
			}
			//fstr << endl;
		}
		fstr << endl;

		free(h_dsigma); free(h_deltau_x_r);free(h_deltau_y_r);

	#endif

	cout << "### final global stress " << sigma_av << endl;
	cout << "### ------ CUDA MESO_PLASTIC MODEL------ " << endl;
	// Turn off cronometer
	clock.tac();
	//clock.print();
	REAL total_time = clock.cpu_elapsed();
	cout << "### Total time    : " << total_time/1000.  << " seg" << endl;

	return 0;
}

