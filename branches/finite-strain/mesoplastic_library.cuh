/*
 * mesoplastic_library.cuh
 *
 *  started on: May 10, 2013
 *      Author: eze
 * .....based on: main.cu by Kirsten
 */

#ifndef MESOPLASTIC_LIBRARY_CUH
#define MESOPLASTIC_LIBRARY_CUH

// RNG: PHILOX
#include "../common/RNGcommon/Random123/philox.h"
#include "../common/RNGcommon/Random123/u01.h"

#include <iostream> 	/* std::cout, std::fixed */
#include <iomanip>		/* std::setprecision */
#include <cmath>
#include <fstream>
#include <string>

#include <assert.h>
#include <stdint.h> 	/* uint8_t */
#include <cufft.h>
#include <cstdlib>

#include <thrust/device_vector.h>
#include <thrust/reduce.h>
#include <thrust/transform.h>
#include <thrust/transform_reduce.h>
#include <thrust/find.h>
#include <thrust/scan.h>
#include <thrust/sort.h>
#include <thrust/binary_search.h>
#include <thrust/iterator/constant_iterator.h>
#include <thrust/functional.h>
#include <thrust/unique.h>


//Define random() Seed
#define RANDOM_SEED 7536951

//Define Philox Seed
#ifdef DOUBLE_PRECISION
	typedef r123::Philox2x64 RNG2;
	typedef r123::Philox4x64 RNG4;
	#define PHILOX_SEED 1332277027LLU
#else
	typedef r123::Philox2x32 RNG2;
	typedef r123::Philox4x32 RNG4;
	#define PHILOX_SEED 125522117 //67321461
#endif

#include "../common/cuda_util.h"
#include "mesoplastic_kernels.cuh"
#include "../common/writeppm.h"
#include "../common/histo.h"

using namespace std;
using namespace thrust::placeholders;

struct is_non_zero
{
	__host__ __device__
	bool operator()(unsigned int x)
	{
	return (x!=0);
	}
};

struct cast_int_real
{
	__host__ __device__
	REAL operator()(unsigned int x) const
	{
	return 1.0*x;
	}
};

//typedef thrust::tuple<COMPLEX,COMPLEX,REAL> Tuple;
struct distance
{
	template <typename Tuple>
	__host__ __device__
	void operator()(Tuple t)
	{
	// C[i] = (A[i].x-B[i].x)*(A[i].x-B[i].x)+(A[i].y-B[i].y)*(A[i].y-B[i].y);
	thrust::get<3>(t) = (thrust::get<0>(t).x - thrust::get<1>(t).x)*(thrust::get<0>(t).x - thrust::get<1>(t).x) +\
			(thrust::get<0>(t).y - thrust::get<1>(t).y)*(thrust::get<0>(t).y - thrust::get<1>(t).y);
	}
};

//////////////////////////////////////////////////////////////////////
class mesoplastic_model
{
	private:
	cufftHandle plan_c2r;
	cufftHandle plan_r2c;

	public:
	// Pointer to host arrays (realspace)
	bool *h_state;
	REAL *h_sigma;
	REAL *h_propagator;
	#ifdef TRACERS
	COMPLEX *h_tracers; /* Can be float2, we only want to have a 2 coordinate variable */ //TODO: Move this to a SOA
	double *h_sf;
	double *h_msd;
	unsigned int *h_sf_counter;
	unsigned int *h_msd_counter;
	int qqssize;
	#endif
	#ifdef VISUALIZATION
	REAL *h_structure_factor;
	REAL *h_Sq2dimage;
	#endif

	// Pointer to device arrays (realspace and fourierspace)
	bool *d_state;
	REAL *d_sigma;
	COMPLEX *d_dsigma;
	REAL *d_dsigma_r;
	COMPLEX *d_epsilon_dot;
	REAL *d_epsilon_dot_r;
	REAL *d_propagator;
	#ifdef ACTIVATED_DYNAMICS
	REAL *d_tower;
	#endif
	#ifdef CORRELATION
	REAL *d_fluctuations_tw;
	#endif
	#ifdef TRACERS
	COMPLEX *d_tracers;  //TODO: Move this to a SOA
	COMPLEX *d_tracerstw; //TODO: Move this to a SOA
	COMPLEX *d_deltau_x; //TODO: Move this to a SOA
	COMPLEX *d_deltau_y; //TODO: Move this to a SOA
	REAL *d_deltau_x_r;
	REAL *d_deltau_y_r;
	REAL *d_oseen_x;
	REAL *d_oseen_y;
	REAL *d_sf_aux;
	REAL *d_sf_out;
	unsigned int *d_qsquares;
	unsigned int *d_qsquares_out;
	unsigned int *d_qsquares_sorted;
	unsigned int *d_qsquares_occurence;
	#endif
	#ifdef VISUALIZATION
	REAL *d_structure_factor;
	REAL *d_Sq2d, *d_Sq2dimage;
	#endif

	// intializing the class
	mesoplastic_model(){

		//Array sizes
		size_t size_b  = NN * sizeof(bool);
		size_t size_ui = NN * sizeof(unsigned int);
		size_t size_r  = NN * sizeof(REAL);
		size_t size_c  = LX*(LY/2+1) * sizeof(COMPLEX);
		size_t size_rc = LX*(LY/2+1) * sizeof(REAL);

		//times to be used for sizes
		int datasize = int(RUN_TIME/(float)WAITING_T)+1;

		// Allocate arrays on host
		h_state = (bool *)malloc(size_b);
		h_sigma = (REAL *)malloc(size_r);
		h_propagator = (REAL *)malloc(size_r);
		#ifdef TRACERS
		h_tracers = (COMPLEX *)malloc(NTRACERS*sizeof(COMPLEX));
		h_msd = (double *)malloc(datasize*sizeof(double));
		h_msd_counter = (unsigned int *)malloc(datasize*sizeof(unsigned int));
		assert(NN%128==0);
		h_sf = (double *)malloc((datasize+1)*NN/128*sizeof(double));
		h_sf_counter = (unsigned int *)malloc(datasize*sizeof(unsigned int));
		#endif
		#ifdef VISUALIZATION
		h_structure_factor = (REAL *)malloc(size_r);
		h_Sq2dimage = (REAL *)malloc(size_r);
		#endif

		// Allocate arrays on device
		CUDA_SAFE_CALL(cudaMalloc((void **) &d_state, size_b));
		CUDA_SAFE_CALL(cudaMalloc((void **) &d_sigma, size_r));
		CUDA_SAFE_CALL(cudaMalloc((void **) &d_dsigma, size_c));
		CUDA_SAFE_CALL(cudaMalloc((void **) &d_dsigma_r, size_r));
		CUDA_SAFE_CALL(cudaMalloc((void **) &d_epsilon_dot, size_c));
		CUDA_SAFE_CALL(cudaMalloc((void **) &d_epsilon_dot_r, size_r));
		CUDA_SAFE_CALL(cudaMalloc((void **) &d_propagator, size_rc));
		#ifdef ACTIVATED_DYNAMICS
		CUDA_SAFE_CALL(cudaMalloc((void **) &d_tower, size_r));
		#endif
		#ifdef CORRELATION
		CUDA_SAFE_CALL(cudaMalloc((void **) &d_fluctuations_tw, size_r));
		#endif
		#ifdef TRACERS
		CUDA_SAFE_CALL(cudaMalloc((void **) &d_tracers, NTRACERS*sizeof(COMPLEX)));
		CUDA_SAFE_CALL(cudaMalloc((void **) &d_tracerstw, NTW*NTRACERS*sizeof(COMPLEX)));
		CUDA_SAFE_CALL(cudaMalloc((void **) &d_deltau_x, size_c));
		CUDA_SAFE_CALL(cudaMalloc((void **) &d_deltau_y, size_c));
		CUDA_SAFE_CALL(cudaMalloc((void **) &d_deltau_x_r, size_r));
		CUDA_SAFE_CALL(cudaMalloc((void **) &d_deltau_y_r, size_r));
		CUDA_SAFE_CALL(cudaMalloc((void **) &d_oseen_x, size_rc));
		CUDA_SAFE_CALL(cudaMalloc((void **) &d_oseen_y, size_rc));

		CUDA_SAFE_CALL(cudaMalloc((void **) &d_sf_aux, size_r));
		CUDA_SAFE_CALL(cudaMalloc((void **) &d_qsquares, size_ui));
		CUDA_SAFE_CALL(cudaMalloc((void **) &d_qsquares_out, size_ui));
		CUDA_SAFE_CALL(cudaMalloc((void **) &d_qsquares_sorted, size_ui));
		#endif
		#ifdef VISUALIZATION
		CUDA_SAFE_CALL(cudaMalloc((void **) &d_structure_factor, size_r));
		CUDA_SAFE_CALL(cudaMalloc((void **) &d_Sq2d, size_r));
		CUDA_SAFE_CALL(cudaMalloc((void **) &d_Sq2dimage, size_r));
		#endif

		//Preventive fill everything with 0s
		for (unsigned int k=0; k<NN; k++){h_state[k]=0; h_sigma[k]=0.0; h_propagator[k]=0.0;}
		CUDA_SAFE_CALL(cudaMemset(d_state, 0, size_b));
		CUDA_SAFE_CALL(cudaMemset(d_sigma, 0, size_r));
		CUDA_SAFE_CALL(cudaMemset(d_dsigma, 0, size_c));
		CUDA_SAFE_CALL(cudaMemset(d_dsigma_r, 0, size_r));
		CUDA_SAFE_CALL(cudaMemset(d_epsilon_dot, 0, size_c));
		CUDA_SAFE_CALL(cudaMemset(d_epsilon_dot_r, 0, size_r));
		CUDA_SAFE_CALL(cudaMemset(d_propagator, 0, size_rc));
		#ifdef ACTIVATED_DYNAMICS
		CUDA_SAFE_CALL(cudaMemset(d_tower, 0, size_r));
		#endif
		#ifdef CORRELATION
		CUDA_SAFE_CALL(cudaMemset(d_fluctuations_tw, 0, size_r));
		#endif
		#ifdef TRACERS
		for (unsigned int k=0; k<NTRACERS; k++){h_tracers[k].x=0.0;h_tracers[k].y=0.0;}
		//for (unsigned int k=0; k<NN; k++){h_Sq2dimage[k] = 0.0;}
		for (int k=0; k<datasize; k++) {h_msd[k] = 0.0; h_msd_counter[k] = 0; h_sf_counter[k] = 0;}
		for (int k=0; k<(datasize+1)*NN/128; k++) {h_sf[k] = 0.0;}
		CUDA_SAFE_CALL(cudaMemset(d_tracers, 0, NTRACERS*sizeof(COMPLEX)));
		CUDA_SAFE_CALL(cudaMemset(d_tracerstw, 0, NTW*NTRACERS*sizeof(COMPLEX)));
		CUDA_SAFE_CALL(cudaMemset(d_deltau_x, 0, size_c));
		CUDA_SAFE_CALL(cudaMemset(d_deltau_y, 0, size_c));
		CUDA_SAFE_CALL(cudaMemset(d_deltau_x_r, 0, size_r));
		CUDA_SAFE_CALL(cudaMemset(d_deltau_y_r, 0, size_r));
		CUDA_SAFE_CALL(cudaMemset(d_oseen_x, 0, size_rc));
		CUDA_SAFE_CALL(cudaMemset(d_oseen_y, 0, size_rc));

		CUDA_SAFE_CALL(cudaMemset(d_sf_aux, 0, size_r));
		CUDA_SAFE_CALL(cudaMemset(d_qsquares, 0, size_ui));
		CUDA_SAFE_CALL(cudaMemset(d_qsquares_out, 0, size_ui));
		CUDA_SAFE_CALL(cudaMemset(d_qsquares_sorted, 0, size_ui));
		#endif
		#ifdef VISUALIZATION
		CUDA_SAFE_CALL(cudaMemset(d_structure_factor, 0, size_r)); 
		CUDA_SAFE_CALL(cudaMemset(d_Sq2d, 0, size_r));
		CUDA_SAFE_CALL(cudaMemset(d_Sq2dimage, 0, size_r));
		#endif

		// cuFFT plan
		#ifdef DOUBLE_PRECISION
		cufftPlan2d(&plan_c2r, LX, LY, CUFFT_Z2D);
		cufftPlan2d(&plan_r2c, LX, LY, CUFFT_D2Z);
		#else
		cufftPlan2d(&plan_c2r, LX, LY, CUFFT_C2R);
		cufftPlan2d(&plan_r2c, LX, LY, CUFFT_R2C);
		#endif
		//cufftSetCompatibilityMode(plan_c2r, CUFFT_COMPATIBILITY_NATIVE);
		//cufftSetCompatibilityMode(plan_r2c, CUFFT_COMPATIBILITY_NATIVE);
	}

////////////////////////////////////////////////////////////////////////////////////////////////

//////////-----------------------Initialize Functions----------------///////////////////////////

	//Initializes on Host and CpyToDevice
	void Initialize(){

		// Initialize trivial host arrays
		for(int i = 0; i<LX; i++)
			for(int j = 0; j<LY; j++)
			{
				int ij = i*LY+j;
				h_sigma[ij] = STRESS_THRESHOLD;
				h_state[ij] = 0;
			}
		h_state[0]=1;
		h_state[LX/2*LY+LY/2]=1;

		// Initialize host array for propagator
		for(int i = 0; i<LX/2+1; i++)
			for(int j = 0; j<(LY/2+1); j++)
			{

			REAL qx=2.0*M_PI*double(i)/LX;
			REAL qy=2.0*M_PI*double(j)/LY;
			REAL q2=qx*qx+qy*qy;

			if(i!=0 || j!=0) h_propagator[i*(LY/2+1)+j] = -4*qx*qx*qy*qy/(q2*q2);
			if(i!=0) h_propagator[(LX-i)*(LY/2+1)+j] = h_propagator[i*(LY/2+1)+j];
			}
		h_propagator[0] =-1.0; // amplitude of the plastic event

		size_t size_b = NN * sizeof(bool);
		size_t size_r = NN * sizeof(REAL);
		size_t size_rc = LX*(LY/2+1) * sizeof(REAL);

		cudaMemcpy(d_sigma, h_sigma, size_r ,cudaMemcpyHostToDevice);
		cudaMemcpy(d_state, h_state, size_b ,cudaMemcpyHostToDevice);
		cudaMemcpy(d_propagator, h_propagator, size_rc, cudaMemcpyHostToDevice);
	}

	void InitializeOnDevice(){

		// Initialize State
		thrust::device_ptr<bool> state_ptr (d_state);
		thrust::fill(state_ptr, state_ptr+NN, 0);
		//Or construct a particular initial state:
		/*thrust::device_vector<bool> tmp(NN, 0);
		tmp[0]=1;
		tmp[LX/2*LY+LY/2]=1; //(random() & 0x1u);
		thrust::copy(tmp.begin(),tmp.end(),state_ptr);
		*/

		// Initialize Stress
		thrust::device_ptr<REAL> sigma_ptr (d_sigma);
		thrust::fill(sigma_ptr, sigma_ptr+NN, STRESS_THRESHOLD);

		// Initialize Propagator
		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(LX/TILE_X, LY/TILE_Y/2+1);
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);
		Kernel_SetPropagator<<<dimGrid, dimBlock>>>(d_propagator,LX,LY/2+1);
		//TODO: Try to fit propagator in constant or texture memory
		//CUDA_SAFE_CALL(cudaBindTexture(NULL, propagatorTex, d_propagator, LX*(LY/2+1)*sizeof(REAL)));
	}

	#ifdef TRACERS
	void InitializeTracersAndAll(){

		// Initialize Oseen Tensor
		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(LX/TILE_X, LY/TILE_Y/2+1);
		Kernel_SetOseenTensor<<<dimGrid, dimBlock>>>(d_oseen_x,d_oseen_y,LX,LY/2+1);
		//TODO: Bind Oseen tensor to texture

		// Initialize Tracers
		assert(TILE<=THREADS_PER_BLOCK);
		assert(NTRACERS/TILE<=BLOCKS_PER_GRID);
		Kernel_InitTracers<<<NTRACERS/TILE, TILE>>>(d_tracers, NTRACERS);

		//Compute qsquares
		dim3 dimBlock2(TILE_X, TILE_Y);
		dim3 dimGrid2(LX/TILE_X, LY/TILE_Y);
		assert(dimBlock2.x*dimBlock2.y<=THREADS_PER_BLOCK);
		assert(dimGrid2.x<=BLOCKS_PER_GRID && dimGrid2.y<=BLOCKS_PER_GRID);
		Kernel_FillQSquares<<<dimGrid2, dimBlock2>>>(d_qsquares);
		//in d_squares we have the function q2(i*LY+j)= i^2+j^2
		//CUDA_SAFE_CALL(cudaThreadSynchronize()); //TODO: Check if needed
		thrust::device_ptr<unsigned int> qsquares_ptr(d_qsquares);

		//Sort qsquares
		thrust::device_ptr<unsigned int> qsquares_sorted_ptr(d_qsquares_sorted);
		thrust::copy(qsquares_ptr, qsquares_ptr+NN, qsquares_sorted_ptr);
		thrust::sort(qsquares_sorted_ptr,qsquares_sorted_ptr+NN);
		//in d_squares_sorted we have the LX*LY values of q2 sorted (with redundance)

		//Compute qsquares occurence
		thrust::device_vector<unsigned int> d_occurence(NN,1);
		thrust::device_vector<unsigned int> d_occ_out(NN);
		thrust::device_ptr<unsigned int> qq_ptr(d_qsquares_out);
		thrust::fill(qq_ptr, qq_ptr+NN, 0); //TODO: Check if needed
		//reduce_by_key CANNOT support in_place keys nor values. Estimated time to realize this: 12hs
		thrust::pair<thrust::device_ptr<unsigned int>, thrust::device_vector<unsigned int>::iterator> new_end = \
		thrust::reduce_by_key(qsquares_sorted_ptr,
				qsquares_sorted_ptr+NN,
				d_occurence.begin(),
				qq_ptr,
				d_occ_out.begin());
		//in qq_ptr we have the values of q2 sorted (each one appearing only once)
		//in d_occurence we have the corresponding occurence of each value of qq_ptr

		//Let's find the lenght of qq_ptr, we search for the biggest possible q^2
		thrust::device_ptr<unsigned int> iter;
		iter = thrust::find(qq_ptr,qq_ptr+NN, QSMAX);
		qqssize = (iter - qq_ptr)+1; //This is a public variable of the class, so we can use it later
		//qqssize = (new_end.first-qq_ptr); //TODO: Understand why this was not working

		//Copy d_occurence into the global array d_qsquares_occurence
		CUDA_SAFE_CALL(cudaMalloc((void**) &d_qsquares_occurence, sizeof(unsigned int)*qqssize));
		CUDA_SAFE_CALL(cudaMemset(d_qsquares_occurence, 0, sizeof(unsigned int)*qqssize));
		thrust::device_ptr<unsigned int> occ_qsqr_ptr(d_qsquares_occurence);
		thrust::copy(d_occ_out.begin(),d_occ_out.begin()+qqssize, occ_qsqr_ptr);
		d_occ_out.erase(new_end.second, d_occ_out.end());

		CUDA_SAFE_CALL(cudaMalloc((void**) &d_sf_out, sizeof(REAL)*qqssize));
		thrust::device_ptr<REAL> sf_out_ptr (d_sf_out);
		thrust::fill(sf_out_ptr,sf_out_ptr+qqssize, 0.0);

// For check purpouses
/*		thrust::host_vector<unsigned int> A(qqssize);
		thrust::host_vector<unsigned int> B(qqssize);
		thrust::copy(qq_ptr,qq_ptr+qqssize,A.begin());
		thrust::copy(d_occurence.begin(),d_occurence.begin()+qqssize,B.begin());
		for(int i=0; i<qqssize; i++) cout << A[i] << " " << B[i] << endl;
		cout << "## Occ has size " << qqssize << endl;
		system("pause");
*/
	}
	#endif

////////////////////////////////////////////////////////////////////////////////////////////////

//////////-------------------------Update Functions------------------///////////////////////////


	void UpdateStateVariables(int time){
		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(LX/2/TILE_X, LY/TILE_Y); //using PhiloxPair
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);
		Kernel_UpdateStateVariables<<<dimGrid, dimBlock>>>(d_sigma, d_state, time);
	}

	void ComputePlasticStrain(){
		thrust::device_ptr<REAL> sigma_ptr (d_sigma);
		thrust::device_ptr<bool> state_ptr (d_state);
		thrust::device_ptr<REAL> epsilon_ptr (d_epsilon_dot_r);
		//TODO: This can be a transform by key or something takig advantage of the bool
		// epsilon_dot <- sigma * state
		thrust::transform(sigma_ptr, sigma_ptr+NN, state_ptr, epsilon_ptr, thrust::multiplies<REAL>());
		//NOTICE:There is a factor of 1/2 missing in the calculation of epsilon_dot, that would cancell with the factor \
		of 2 that is missing in the calculus of the d_dsigma later on. Everyone happy.
	}

	void Convolution(){
		//TODO: Do this with Thrust.
	/*	thrust::device_ptr<COMPLEX> epsilon_ptr (d_epsilon_dot);
		thrust::device_ptr<REAL> propagator_ptr (d_propagator);
		thrust::device_ptr<COMPLEX> state_ptr (d_dsigma);
		//AND NOW? SAXPY with complex type???
		thrust::transform(epsilon_ptr, epsilon_ptr+NN, propagator_ptr, state_ptr, 
				SCALE * _1 * _2); // placeholder expression
	*/
		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(LX/TILE_X, LY/TILE_Y/2+1);
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);
		Kernel_ComplexPointwiseMulAndScale<<<dimGrid, dimBlock>>>(d_epsilon_dot, d_propagator, d_dsigma, SCALE);
	}

	void EulerIntegrationStep(float shear_rate){
		//TODO: If the single other term  in the equation of motion besides the convolution is the shear rate, we \
		can perform Euler directly in fourier, applying gamma to the 0 mode, and then antitransform the new sigma.
		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(LX/TILE_X, LY/TILE_Y);
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);
		Kernel_EulerIntegrationStep<<<dimGrid, dimBlock>>>(d_sigma, d_dsigma_r, shear_rate, TIME_STEP);
	}

	#ifdef ACTIVATED_DYNAMICS
	void UpdateStateVariablesActivated(int time){
		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(LX/2/TILE_X, LY/TILE_Y); //using PhiloxPair
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);
		Kernel_UpdateStateVariablesActivated<<<dimGrid, dimBlock>>>(d_sigma, d_state, time);
	}

	int CheckForPlasticActivity(){
		thrust::device_ptr<bool> state_ptr (d_state);
		thrust::device_ptr<bool> iter;
		iter = thrust::find(state_ptr, state_ptr+NN,1);
		int position = iter - state_ptr;
		return (position);
	}

	REAL ConstructActivatedProbabilityTowerChooseOneEventAndUpdate(int time){
		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(LX/TILE_X, LY/TILE_Y);
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);
		Kernel_ConstructActivatedPorbabilityTower<<<dimGrid, dimBlock>>>(d_tower, d_sigma);

		thrust::device_ptr<REAL> tower_ptr (d_tower);

		//REAL totalprobability = thrust::reduce(tower_ptr,tower_ptr+NN);
		thrust::inclusive_scan(tower_ptr,tower_ptr+NN,tower_ptr); //in-place inclusive scan

		//CPY to recover the total sum
		REAL totalprobability;
		CUDA_SAFE_CALL(cudaMemcpy(&totalprobability, &d_tower[NN-1], sizeof(REAL), cudaMemcpyDeviceToHost));

		//thrust::transform(d_tower.begin(),d_tower.end(), \
				thrust::make_constant_iterator(totalprobability), \
				d_tower.begin(),thrust::divides<REAL>());
		//Istead doing the later we will multiply by totalsum the random number

		thrust::device_ptr<REAL> chosen = thrust::lower_bound(tower_ptr,tower_ptr+NN,(random()/RAND_MAX)*totalprobability);
		Kernel_UpdateChosenSite<<<1,1>>>(d_state, chosen-tower_ptr);
		REAL timeincrement = totalprobability/((-1.)*log((REAL)random()/RAND_MAX));

		return timeincrement;
	}
	#endif

	#ifdef TRACERS
	void CalculateDisplacementField(){
		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(LX/TILE_X, LY/TILE_Y/2+1);
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);
		Kernel_CalculateDisplacementField<<<dimGrid, dimBlock>>>(d_deltau_x, d_deltau_y, d_oseen_x, d_oseen_y, d_epsilon_dot, SCALE);
	}

	void UpdateTracers(){
		dim3 dimBlock(TILE);
		dim3 dimGrid(NTRACERS/TILE);
		assert(dimBlock.x<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID);
		Kernel_UpdateTracersPositions<<<dimGrid, dimBlock>>>(d_deltau_x_r, d_deltau_y_r, d_tracers, TIME_STEP);
	}
	#endif

	void TransformToFourierSpace(){
		#ifdef DOUBLE_PRECISION
		CUFFT_SAFE_CALL(cufftExecD2Z(plan_r2c, d_epsilon_dot_r, d_epsilon_dot));
		#else
		CUFFT_SAFE_CALL(cufftExecR2C(plan_r2c, d_epsilon_dot_r, d_epsilon_dot));
		#endif
	}

	void AntitransformFromFourierSpace(){
		#ifdef DOUBLE_PRECISION
		CUFFT_SAFE_CALL(cufftExecZ2D(plan_c2r, d_dsigma, d_dsigma_r));
			#ifdef TRACERS
			CUFFT_SAFE_CALL(cufftExecZ2D(plan_c2r, d_deltau_x, d_deltau_x_r));
			CUFFT_SAFE_CALL(cufftExecZ2D(plan_c2r, d_deltau_y, d_deltau_y_r));
			#endif
		#else
		CUFFT_SAFE_CALL(cufftExecC2R(plan_c2r, d_dsigma, d_dsigma_r));
			#ifdef TRACERS
			CUFFT_SAFE_CALL(cufftExecC2R(plan_c2r, d_deltau_x, d_deltau_x_r));
			CUFFT_SAFE_CALL(cufftExecC2R(plan_c2r, d_deltau_y, d_deltau_y_r));
			#endif
		#endif
	}

////////////////////////////////////////////////////////////////////////////////////////////////

//////////-------------------------Calculate Functions---------------///////////////////////////

	REAL CalculateSigmaAv(){
		thrust::device_ptr<REAL> sigma_ptr (d_sigma);
		return thrust::reduce(sigma_ptr, sigma_ptr+NN);
	}

	#ifdef CORRELATION
	REAL ComputeSsstress(){
		thrust::device_ptr<REAL> sigma_ptr (d_sigma);
		return thrust::reduce(sigma_ptr,sigma_ptr+NN)/(REAL)NN;
	}

	REAL ComputeFluctuationsAtTw(const REAL gamma_p, const REAL ssstress){

		thrust::device_ptr<REAL> sigma_ptr (d_sigma);
		thrust::device_ptr<REAL> fluct_tw_ptr (d_fluctuations_tw);

		//TODO: Fusion the two transforms with a transform_iterator
		thrust::transform(sigma_ptr, sigma_ptr+NN, thrust::make_constant_iterator(ssstress), \
				fluct_tw_ptr, thrust::minus<REAL>());

		return thrust::transform_reduce(fluct_tw_ptr,fluct_tw_ptr+NN, _1*_1, 0.0f, thrust::plus<REAL>());
	}

	REAL ComputeAutoCorrelationFunction(const REAL gamma_p, const REAL ssstress, const REAL ro_zero2){

		thrust::device_vector<REAL> partial_corr(NN);
		thrust::device_vector<double> partial_corr_double(NN); //TODO:FIX this, workaround to avoid presicion problems.

		thrust::device_ptr<REAL> sigma_ptr (d_sigma);
		thrust::device_ptr<REAL> fluct_tw_ptr (d_fluctuations_tw);

		//TODO: Fusion transforms with transform_iterators
		thrust::transform(sigma_ptr, sigma_ptr+NN, thrust::make_constant_iterator(ssstress), \
				partial_corr.begin(), thrust::minus<REAL>());

		thrust::transform(partial_corr.begin(), partial_corr.end(), fluct_tw_ptr, \
				partial_corr.begin(), thrust::multiplies<REAL>());

		thrust::transform(partial_corr.begin(), partial_corr.end(), \
				thrust::make_constant_iterator(1.0/(ro_zero2)), \
				partial_corr_double.begin(), thrust::multiplies<REAL>());

		return thrust::reduce(partial_corr_double.begin(),partial_corr_double.end());
	}
	#endif


	#ifdef TRACERS
	void SetTracersAtTW(unsigned int i){
		//CUDA_SAFE_CALL(cudaMemcpy(d_tracerstw+(i*NTRACERS), d_tracers, sizeof(COMPLEX)*NTRACERS, cudaMemcpyDeviceToDevice));
		CUDA_SAFE_CALL(cudaMemcpy(&d_tracerstw[i*NTRACERS], d_tracers, sizeof(COMPLEX)*NTRACERS, cudaMemcpyDeviceToDevice));
	}

	void CalculateAccumulateMeanSquareDisplacement(const unsigned int index, const unsigned int tw_counter){

		dim3 dimBlock(TILE);
		dim3 dimGrid(NTRACERS/TILE);
		assert(dimBlock.x<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID);

		thrust::device_vector<REAL> msd_sum(NTRACERS);
		REAL *msd_sum_ptr = thrust::raw_pointer_cast(msd_sum.data());

		for (unsigned int i=0; i<tw_counter; i++){
		Kernel_CalculateMeanSquareDisplacement<<<dimGrid,dimBlock>>>(d_tracers,&d_tracerstw[i*NTRACERS],msd_sum_ptr);
		//CUDA_SAFE_CALL(cudaThreadSynchronize());
		double meansquaredisplacement = thrust::reduce(msd_sum.begin(),msd_sum.end())/(double)(NTRACERS);
		h_msd[index-i] += meansquaredisplacement;
		h_msd_counter[index-i] += 1;
		}

	}

	void CalculatePrintMeanSquareDisplacement(ofstream &fstr, const unsigned int t, const unsigned int tw, const unsigned int tw_counter){

		dim3 dimBlock(TILE);
		dim3 dimGrid(NTRACERS/TILE);
		assert(dimBlock.x<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID);

		thrust::device_vector<REAL> msd_sum(NTRACERS);
		REAL *msd_sum_ptr = thrust::raw_pointer_cast(msd_sum.data());

		Kernel_CalculateMeanSquareDisplacement<<<dimGrid,dimBlock>>>(d_tracers, d_tracerstw,msd_sum_ptr);

		double meansquaredisplacement = thrust::reduce(msd_sum.begin(),msd_sum.end())/(double)(NTRACERS);
		fstr << TIME_STEP*(t-tw) << " " << meansquaredisplacement << " " << endl;

	}


	void CalculateAccumulateTrajectoriesStructureFactor(const unsigned int index, const unsigned int tw_counter){

		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(LX/TILE_X, LY/TILE_Y);
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);

		thrust::device_ptr<unsigned int> qsquares_ptr(d_qsquares);
		thrust::device_vector<unsigned int> q2(NN);
		thrust::device_ptr<REAL> sf_ptr(d_sf_aux);
		thrust::device_ptr<REAL> sf_out_ptr(d_sf_out);
		thrust::device_ptr<unsigned int> qq_ptr(d_qsquares_out);
		thrust::pair<thrust::device_ptr<unsigned int>, thrust::device_ptr<REAL> > new_end;
		thrust::device_ptr<unsigned int> occurence_ptr(d_qsquares_occurence);

		for (unsigned int i=0; i<tw_counter; i++){
		Kernel_CalculateTrajectoriesSF<<<dimGrid, dimBlock>>>(d_tracers,&d_tracerstw[i*NTRACERS], \
					d_sf_aux, LX, LY);

		//Do a copy of q^2 in the i*LY+j order
		thrust::copy(qsquares_ptr,qsquares_ptr+NN,q2.begin());
		//CUDA_SAFE_CALL(cudaThreadSynchronize());
		//Sort the recentry calculated structure factor according to q2, this will sort the keys as well
		thrust::sort_by_key(q2.begin(),q2.end(),sf_ptr); //This is OK!!
		//reduce_by_key CANNOT support in_place keys nor values. Estimated time to realize this: 12hs
		new_end = thrust::reduce_by_key(q2.begin(),q2.end(),sf_ptr,qq_ptr,sf_out_ptr);
		assert(new_end.first-qq_ptr == qqssize);
		thrust::transform(sf_out_ptr,sf_out_ptr+qqssize, \
			thrust::make_transform_iterator(occurence_ptr, _1*1.0), sf_out_ptr, \
			thrust::divides<REAL>()); 
		//we can also make this division at the very end

		thrust::host_vector<REAL> B(qqssize);
		thrust::copy(sf_out_ptr,sf_out_ptr+qqssize,B.begin());
		int sfdatasize = MIN(qqssize,NN/128);
			for(int k=0;k<sfdatasize;k++){
				h_sf[(index-i)*sfdatasize+k] += B[k]; 
			}
			h_sf_counter[index-i] += 1;
		}
		//thrust::device_ptr<REAL> sf_ptr (d_structure_factor);
		//TODO: Use this array to accumulate and average over t0.
	}

	void CalculatePrintTrajectoriesStructureFactor(ofstream &fstr, const unsigned int t, const unsigned int tw){

		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(LX/TILE_X, LY/TILE_Y);
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);

		Kernel_CalculateTrajectoriesSF<<<dimGrid, dimBlock>>>(d_tracers, d_tracerstw, \
					d_sf_aux, LX, LY);

		//The clever way, sorting the array
		//Do a copy of q^2 in the i*LY+j order
		thrust::device_ptr<unsigned int> qsquares_ptr(d_qsquares);
		thrust::device_vector<unsigned int> q2(NN);
		thrust::copy(qsquares_ptr,qsquares_ptr+NN,q2.begin());

		//CUDA_SAFE_CALL(cudaThreadSynchronize());
		thrust::device_ptr<REAL> sf_ptr(d_sf_aux);

		//Sort the recentry calculated structure factor according to q2, this will sort the keys as well
		thrust::sort_by_key(q2.begin(),q2.end(),sf_ptr); //This is OK!!

		//thrust::device_ptr<unsigned int> q2_ptr(&q2[0]);
		//thrust::device_ptr<unsigned int> qq_ptr = thrust::device_malloc<unsigned int>(NN);

		//thrust::device_vector<unsigned int> qq_out(NN);
		thrust::device_ptr<unsigned int> qq_ptr(d_qsquares_out);
		thrust::device_ptr<REAL> sf_out_ptr(d_sf_out);

		//reduce_by_key CANNOT support in_place keys nor values. Estimated time to realize this: 12hs
		//thrust::pair<thrust::device_ptr<unsigned int>, thrust::device_ptr<REAL> > new_end = 
		thrust::reduce_by_key(q2.begin(),q2.end(),sf_ptr,qq_ptr,sf_out_ptr);
		//later on we can neglect qq_ptr and use  thrust::make_discard_iterator()
		//assert(new_end.first-qq_ptr == qqssize);

		thrust::device_ptr<unsigned int> occurence_ptr(d_qsquares_occurence);
		thrust::transform(sf_out_ptr,sf_out_ptr+qqssize, \
				thrust::make_transform_iterator(occurence_ptr, _1*1.0), sf_out_ptr, \
				thrust::divides<REAL>()); 
		//we can also make this division at the very end

		//thrust::device_ptr<REAL> sf_ptr (d_structure_factor);
		//TODO: Use this array to accumulate and average over t0.
//Check
		thrust::host_vector<unsigned int> A(qqssize);
		thrust::host_vector<REAL> B(qqssize);
		thrust::copy(qq_ptr,qq_ptr+qqssize,A.begin());
		thrust::copy(sf_out_ptr,sf_out_ptr+qqssize,B.begin());
		fstr << "### Structure factor as a function of q^2 for time t" << t*TIME_STEP << endl;
		for(int i=0; i<qqssize/50.; i++) fstr << (2*M_PI/(REAL)LX)*(2*M_PI/(REAL)LX)*A[i] << " " \
		<< B[i] << endl;
		fstr << endl;
		fstr.flush();
	}
	#endif

	#ifdef VISUALIZATION
	void ComputeStructureFactor(){
		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(LX/TILE_X, LY/TILE_Y/2+1);
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);
		CUDAkernel_ComputeStructureFactor<<<dimGrid, dimBlock>>>(d_tracers, d_tracerstw, d_Sq2d, LX, (LY/2+1));
	}

	void ComputeStructureFactorImage(){
		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(LX/TILE_X, LY/TILE_Y);
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);
		CUDAkernel_ComputeStructureFactorImage<<<dimGrid, dimBlock>>>(d_Sq2d, d_Sq2dimage, LX, LY);
		CUDA_SAFE_CALL(cudaMemcpy(h_Sq2dimage, d_Sq2dimage, sizeof(cufftReal)*LX*LY, cudaMemcpyDeviceToHost));
	}
	#endif


////////////////////////////////////////////////////////////////////////////////////////////////

//////////---------------------------Copy Functions------------------///////////////////////////

	/* transfer from GPU to CPU memory */
	void CpyDeviceToHost(){
		size_t size_b = NN * sizeof(bool);
		size_t size_r = NN * sizeof(REAL);
		CUDA_SAFE_CALL(cudaMemcpy(h_state, d_state, size_b, cudaMemcpyDeviceToHost));
		CUDA_SAFE_CALL(cudaMemcpy(h_sigma, d_sigma, size_r, cudaMemcpyDeviceToHost));
	}

	#ifdef TRACERS
	void CpyDeviceToHost1Step(REAL *h_dsigma, REAL *h_deltau_x_r, REAL *h_deltau_y_r){
		size_t size_r = NN * sizeof(REAL);
		CUDA_SAFE_CALL(cudaMemcpy(h_deltau_x_r, d_deltau_x_r, size_r, cudaMemcpyDeviceToHost));
		CUDA_SAFE_CALL(cudaMemcpy(h_deltau_y_r, d_deltau_y_r, size_r, cudaMemcpyDeviceToHost));
		CUDA_SAFE_CALL(cudaMemcpy(h_dsigma, d_dsigma_r, size_r, cudaMemcpyDeviceToHost));
	}
	#endif


////////////////////////////////////////////////////////////////////////////////////////////////

//////////---------------------------Print Functions-----------------///////////////////////////

	void PrintState(ofstream &fstr){
		for(int i=0;i<LX;i++){
			for(int j=0;j<LY;j++){
				int k=i*LY+j;
				fstr << h_state[k] << " ";
			}
			fstr << endl;
		}
		fstr << endl;
	};

	void PrintSigma(ofstream &fstr){
		for(int i=0;i<LX;i++){
			for(int j=0;j<LY;j++){
				int k=i*LY+j;
				fstr << h_sigma[k] << " ";
			}
			fstr << endl;
		}
		fstr << endl;
	};

	void PrintResults(){
		string outputFile = "sigma.dat";
		ofstream out(outputFile.c_str());
		if(out)
		{
			for (int j=0; j<LX; j++) 
			{
			  for (int i=0; i<LX; i++) 
			    out << h_sigma[j*LX+i] << "\t";
			  out << endl;
			}
		}
		else
		cerr<<"ERROR!"<<endl;

		string outputFile2 = "state.dat";
		ofstream out2(outputFile2.c_str());
		if(out2)
		{
			for (int j=0; j<LX; j++) 
			{
			  for (int i=0; i<LX; i++) 
			    out2 << h_state[j*LX+i] << "\t";
			  out2 << endl;
			}
		}
		else
		cerr<<"ERROR!"<<endl;
	}

	#ifdef TRACERS
	void PrintTracers(ofstream &fstr, int t, int howmany){
		//CUDA_SAFE_CALL(cudaMemcpy(h_tracers, d_tracers, NTRACERS*sizeof(COMPLEX), cudaMemcpyDeviceToHost));
		CUDA_SAFE_CALL(cudaMemcpy(h_tracers, d_tracers, howmany*sizeof(COMPLEX), cudaMemcpyDeviceToHost));
		//cout << h_tracers[0].x << " " << h_tracers[0].y << " " << 0.0 << " " << 0.0;
		fstr << "### tracers at time" << t*TIME_STEP << endl;
		for(int i=0;i<howmany;i++){
				fstr << h_tracers[i].x << " " << h_tracers[i].y << " " ;
		}
		fstr.flush();
		fstr << endl;
	}

	void PrintMeanSquareDisplacement(ofstream &fstr, const unsigned int tw){
		//times to be used for sizes
		int datasize = int((RUN_TIME-WAITING_T)/WAITING_T)+1;
		for(int i=1; i<datasize; i++){
		//if (h_msd_counter[i]!=0) fstr << i*tw*TIME_STEP //fixed 8/8/13
		if (h_msd_counter[i]!=0) fstr << (i-1)*tw*TIME_STEP << " " << h_msd[i]/(double)h_msd_counter[i] << " " \
		<< h_msd_counter[i] << " " << endl;
		}
		fstr.flush();
	}

	void PrintTrajectoriesStructureFactor(ofstream &fstr, const unsigned int tw){
		//times to be used for sizes
		int datasize = int((RUN_TIME-WAITING_T)/WAITING_T)+1;
		int sfdatasize = MIN(qqssize,NN/128);

		thrust::device_ptr<unsigned int> qsquares_out_ptr(d_qsquares_out);
		thrust::host_vector<REAL> A(qqssize);
		thrust::copy(qsquares_out_ptr,qsquares_out_ptr+qqssize,A.begin());

		for(int i=1; i<datasize; i++){
			//fstr << "## SF for time window" << i*tw*TIME_STEP //fixed 8/8/13
			fstr << "## SF for time window" << (i-1)*tw*TIME_STEP << endl;
			for(int k=0; k<sfdatasize; k++){
				if (h_sf_counter[i]!=0) fstr << (2*M_PI/(REAL)LX)*(2*M_PI/(REAL)LX)*A[k] << " " \
				<< h_sf[i*sfdatasize+k]/(double)h_sf_counter[i] << endl;
				}
			fstr << endl;
		}
		fstr.flush();
	}
	#endif

	#ifdef VISUALIZATION
	void Visualization(float gamma_p){
		CpyDeviceToHost();

		char filename[200];
		sprintf(filename, "framesigma%5f.ppm", gamma_p);
		PrintPicture(filename,0);
		sprintf(filename, "framestate%5f.ppm", gamma_p);
		PrintPicture(filename,1);
		#ifdef JPEG
		char cmd[200];
		sprintf(cmd, "convert framesigma%5f.ppm framestate%5f.ppm +append frame_Sigma-State%5f.ppm", \
				gamma_p, gamma_p, gamma_p);
		system(cmd);
		sprintf(cmd, "rm framesigma%5f.ppm framestate%5f.ppm", gamma_p, gamma_p);
		system(cmd);
		#endif
	}

	void PrintPicture(char *picturename, int who){
		if (who==0) writePPMbinaryImage(picturename, h_sigma);
		if (who==1) writePPMbinaryImage_bool(picturename, h_state);
		#ifdef JPEG
		char cmd[200];
		sprintf(cmd, "convert frame%d.ppm frame%d.jpg", 100000000+x, 100000000+x);
		system(cmd);
		sprintf(cmd, "rm frame%d.ppm", 100000000+x);
		system(cmd);
		#endif
	};

	void PrintImageStructureFactor(char *picturename, REAL x){
		writePPMbinaryImage(picturename, h_Sq2dimage);
		#ifdef JPEG
		char cmd[200];
		sprintf(cmd, "convert Sq%.2f.ppm -resize %dx%d\\! Sq%.2f.ppm", 100000000+x, LX, LX, 100000000+x);
		system(cmd);
		sprintf(cmd, "convert Sq%.2f.ppm Sq%.2f.jpg", 100000000+x, 100000000+x);
		system(cmd);
		sprintf(cmd, "rm Sq%.2f.ppm", 100000000+x);
		system(cmd);
		#endif
	}

	#endif

////////////////////////////////////////////////////////////////////////////////////////////////

//////////---------------------------Cleaning Issues-----------------///////////////////////////


	~mesoplastic_model(){

		/* frees CPU memory */
		free(h_state);
		free(h_sigma);
		free(h_propagator);
		#ifdef TRACERS
		free(h_tracers);
		free(h_msd);
		free(h_msd_counter);
		free(h_sf);
		free(h_sf_counter);
		#endif

		/* frees GPU memory */
		cudaFree(d_state);
		cudaFree(d_sigma);
		cudaFree(d_dsigma);
		cudaFree(d_dsigma_r);
		cudaFree(d_epsilon_dot);
		cudaFree(d_epsilon_dot_r);
		cudaFree(d_propagator);
		#ifdef ACTIVATED_DYNAMICS
		cudaFree(d_tower);
		#endif
		#ifdef CORRELATION
		cudaFree(d_fluctuations_tw);
		#endif
		#ifdef TRACERS
		cudaFree(d_tracers);
		cudaFree(d_tracerstw);
		cudaFree(d_deltau_x);
		cudaFree(d_deltau_y);
		cudaFree(d_deltau_x_r);
		cudaFree(d_deltau_y_r);
		cudaFree(d_oseen_x);
		cudaFree(d_oseen_y);
		cudaFree(d_sf_aux);
		cudaFree(d_sf_out);
		cudaFree(d_qsquares);
		cudaFree(d_qsquares_out);
		cudaFree(d_qsquares_sorted);
		cudaFree(d_qsquares_occurence);
		#endif
		#ifdef VISUALIZATION
		cudaFree(d_structure_factor);
		cudaFree(d_Sq2d);
		cudaFree(d_Sq2dimage);
		#endif

		cufftDestroy(plan_r2c);
		cufftDestroy(plan_c2r);

	};
};


#endif /*  MESOPLASTIC_LIBRARY_CUH */
