
#ifdef DOUBLE_PRECISION
typedef double REAL;
typedef cufftDoubleComplex COMPLEX;
#else
typedef float REAL;
typedef cufftComplex COMPLEX;
#endif

/*--------RNG ROUTINES--------*/

__device__
REAL DeviceBoxMuller(const REAL u1, const REAL u2)
{
#ifdef DOUBLE_PRECISION
	REAL r = sqrt( -2.0*log(u1) );
	REAL theta = 2.0*M_PI*u2;
	return r*sin(theta);
#else
	REAL r = sqrtf( -2.0*logf(u1) );
	REAL theta = 2.0*M_PI*u2;
	return r*sinf(theta);
#endif
}


__device__
void PhiloxRandomQuartet(const unsigned int index, const unsigned int time, REAL *r1, REAL *r2, REAL *r3, REAL *r4)
{
	RNG4 rng;
	RNG4::ctr_type c_pair={{}};
	RNG4::key_type k_pair={{}};
	RNG4::ctr_type r_quartet;

		// keys = threadid
		k_pair[0]= index;
		// time counter
		c_pair[0]= time;
		c_pair[1]= PHILOX_SEED; // eventually do: PHILOX_SEED + sample;
		// random number generation
		r_quartet = rng(c_pair, k_pair);

	#ifdef DOUBLE_PRECISION
		*r1= u01_closed_closed_64_53(r_quartet[0]);
		*r2= u01_closed_closed_64_53(r_quartet[1]);
		*r3= u01_closed_closed_64_53(r_quartet[2]);
		*r4= u01_closed_closed_64_53(r_quartet[3]);
	#else
		*r1= u01_closed_closed_32_53(r_quartet[0]);
		*r2= u01_closed_closed_32_53(r_quartet[1]);
		*r3= u01_closed_closed_32_53(r_quartet[2]);
		*r4= u01_closed_closed_32_53(r_quartet[3]);
	#endif
}

__device__
void PhiloxRandomPair(const unsigned int index, const unsigned int time, REAL *r1, REAL *r2)
{
	RNG2 rng;
	RNG2::ctr_type c_pair={{}};
	RNG2::key_type k_pair={{}};
	RNG2::ctr_type r_pair;

		// keys = threadid
		k_pair[0]= index;
		// time counter
		c_pair[0]= time;
		c_pair[1]= PHILOX_SEED; // eventually do: PHILOX_SEED + sample;
		// random number generation
		r_pair = rng(c_pair, k_pair);

	#ifdef DOUBLE_PRECISION
		*r1= u01_closed_closed_64_53(r_pair[0]);
		*r2= u01_closed_closed_64_53(r_pair[1]);
	#else
		*r1= u01_closed_closed_32_53(r_pair[0]);
		*r2= u01_closed_closed_32_53(r_pair[1]);
	#endif
}


//---------- DEVICE FUNCTIONS -----------//

//////////-----------------------Initialize Kernels----------------///////////////////////////

__global__ void Kernel_SetPropagator(REAL *g, int nx, int ny){
	unsigned int i = blockIdx.x*blockDim.x+threadIdx.x;
	unsigned int j = blockIdx.y*blockDim.y+threadIdx.y;
	if ( i < nx && j < ny){

			int iprime = (i <= LX/2) ? i : i - LX;

			REAL qx=2.0*M_PI*iprime/(REAL)LX;
			REAL qy=2.0*M_PI*j/(REAL)LY;
			REAL q2=qx*qx+qy*qy;

			if(i!=0 || j!=0) g[i*ny+j] = -4*qx*qx*qy*qy/(q2*q2);
			//if(i!=0) g[(LX-i)*ny+j] = g[i*ny+j];
			if (i==0 && j==0) g[i*ny+j] = -1.0;

	}
}

#ifdef TRACERS
__global__ void Kernel_InitTracers(COMPLEX *tracers, int ntracers){
	const unsigned int tid = blockIdx.x*blockDim.x+threadIdx.x;
	if (tid<ntracers){
		REAL r1, r2;
		PhiloxRandomPair(tid, 123, &r1, &r2);
		tracers[tid].x = r1*LX;
		tracers[tid].y = r2*LY;

		//int increment = NN/NTRACERS;
		//tracers[tid].x = 1.*((tid*increment)%LX);
		//tracers[tid].y = 1.*((tid*increment)/LX);

		//tracers[tid].x = (REAL)(tid%LX);
		//tracers[tid].y = (REAL)(tid/LX);

		//TODO:Fix this to have tracers homogeneously distributed in space
		//tracers[tid].x = (tid*NN/ntracers)/LX;
		//tracers[tid].y = (tid*NN/ntracers) - tracers[tid].x * LX;
	}
}

__global__ void Kernel_SetOseenTensor(REAL *ox, REAL *oy, int nx, int ny){
	unsigned int i = blockIdx.x*blockDim.x+threadIdx.x;
	unsigned int j = blockIdx.y*blockDim.y+threadIdx.y;
	if ( i < nx && j < ny){
		int iprime = (i <= LX/2) ? i : i - LX;

		REAL qx=2.0*M_PI*iprime/(REAL)LX;
		REAL qy=2.0*M_PI*j/(REAL)LY;
		REAL q2=qx*qx+qy*qy;

		if(i!=0 || j!=0){
		ox[i*ny+j] = 2*qy*(1-2*qx*qx/q2)/q2;
		oy[i*ny+j] = 2*qx*(1-2*qy*qy/q2)/q2;
		}

/*		if(i!=0){
		ox[(LX-i)*ny+j] = -ox[i*ny+j];
		oy[(LX-i)*ny+j] = oy[i*ny+j];
		}
*/
		if (i==0 && j==0){
		ox[i*ny+j] = 0.0;
		oy[i*ny+j] = 0.0;
		}
	}
}

__global__ void Kernel_FillQSquares(unsigned int* map)
{
	unsigned int i = blockIdx.x*blockDim.x+threadIdx.x;
	unsigned int j = blockIdx.y*blockDim.y+threadIdx.y;
	if ( i < LX && j < LY){
		unsigned int tid = i*LY+j;
		map[tid]=i*i+j*j;
	}
}
#endif

//////////-------------------------Update Kernels------------------///////////////////////////

__global__ void Kernel_UpdateStateVariables(const REAL* d_sigma, bool* d_state, int time)
{
	// compute idx and idy, the location of the element in the original LX*LY array 
	int idx = blockIdx.x*blockDim.x+threadIdx.x;
	int idy = blockIdx.y*blockDim.y+threadIdx.y;
	if ( idx < LX/2 && idy < LY){
		int index = idx*LY + idy;
		int index1 = 2*index;
		int index2 = 2*index+1;
		const bool actualstate1 = d_state[index1];
		const bool actualstate2 = d_state[index2];
		REAL r1, r2;
		PhiloxRandomPair(index, time, &r1, &r2);
		d_state[index1]= ((actualstate1==0 && d_sigma[index1]>=STRESS_THRESHOLD && r1<TIME_STEP/TAU_PLAST)|| \
		(actualstate1==1 && r1>TIME_STEP/TAU_ELAST));
		d_state[index2]= ((actualstate2==0 && d_sigma[index2]>=STRESS_THRESHOLD && r2<TIME_STEP/TAU_PLAST)|| \
		(actualstate2==1 && r2>TIME_STEP/TAU_ELAST));
	//if (index==64) printf("%d %f %f \n", time, d_sigma[index1], d_sigma[index2]);
	}
}

//PhiloxQuadVersion: TODO: bad performance, why?
__global__ void Kernel_UpdateStateVariablesQuad(const REAL* d_sigma, bool* d_state, int time)
{
	// compute idx and idy, the location of the element in the original LX*LY array 
	int idx = blockIdx.x*blockDim.x+threadIdx.x;
	int idy = blockIdx.y*blockDim.y+threadIdx.y;
	if ( idx < LX/4 && idy < LY){
		int index = idx*LY + idy;
		bool actualstate[4]={0,0,0,0};
		for (int k=0;k<4;k++) actualstate[k]=d_state[4*index+k];
		REAL r[4]={0,0,0,0};
		PhiloxRandomQuartet(index, time, &r[0], &r[1],&r[2], &r[3]);
		for (int k=0;k<4;k++) d_state[4*index+k]= ((actualstate[k]==0 && d_sigma[4*index+k]>=STRESS_THRESHOLD && r[k]<TIME_STEP)|| \
		(actualstate[k]==1 && r[k]>TIME_STEP));
	}
}


// Pointwise multiplication
static __global__ void Kernel_PointwiseMul(const REAL *a, const bool *b, REAL* c)
{
	// compute idx and idy, the location of the element in the original LX*LY array 
	int idx = blockIdx.x*blockDim.x+threadIdx.x;
	int idy = blockIdx.y*blockDim.y+threadIdx.y;
	if ( idx < LX && idy < LY){
		int index = idx*LY + idy;
		c[index] = a[index] * b[index];
	}
}

// COMPLEX pointwise multiplication
static __global__ void Kernel_ComplexPointwiseMulAndScale(const COMPLEX* a, const REAL* b, COMPLEX* c, REAL scale)
{
	// compute idx and idy, the location of the element in the original LX*LY array 
	int idx = blockIdx.x*blockDim.x+threadIdx.x;
	int idy = blockIdx.y*blockDim.y+threadIdx.y;
	if ( idx < LX && idy < (LY/2+1)){
		int index = idx*(LY/2+1) + idy;
		c[index].x = scale*(a[index].x * b[index]);
		c[index].y = scale*(a[index].y * b[index]);
	}
} 

static __global__ void Kernel_EulerIntegrationStep(REAL *a, const REAL *b, REAL gamma, REAL dt)
{
	// compute idx and idy, the location of the element in the original LX*LY array 
	int idx = blockIdx.x*blockDim.x+threadIdx.x;
	int idy = blockIdx.y*blockDim.y+threadIdx.y;
	if ( idx < LX && idy < LY){
		int index = idx*LY + idy;
		//There is a factor of 2 missing here (multipling b), that would cancell with the factor of 1/2 \
		that is missing in the calculus of b as well. Everyone happy.
		a[index]+=(gamma+b[index])*dt;
	}
}


#ifdef ACTIVATED_DYNAMICS
__global__ void Kernel_UpdateStateVariablesActivated(const REAL* d_sigma, bool* d_state, int time)
{
	// compute idx and idy, the location of the element in the original LX*LY array 
	int idx = blockIdx.x*blockDim.x+threadIdx.x;
	int idy = blockIdx.y*blockDim.y+threadIdx.y;
	if ( idx < LX/2 && idy < LY){
		int index = idx*LY + idy;
		bool actualstate;
		REAL actualsigma, lsigma;
		REAL r1, r2;
		PhiloxRandomPair(index, time, &r1, &r2);
		for (int j=0;j<2;j++)
		{
			actualstate = d_state[2*index+j];
			actualsigma = d_sigma[2*index+j];
			//lsigma = (actualsigma>CRITICAL_STRESS)*exp((actualsigma-STRESS_THRESHOLD)/TEMP_EFF)/TAU_PLAST;
			lsigma = exp((actualsigma-STRESS_THRESHOLD)/TEMP_EFF)/TAU_PLAST;
			if(actualstate==0 && r1*(j==0)+r2*(j==1)<lsigma) d_state[2*index+j]=1;
			if(actualstate==1 && r1*(j==0)+r2*(j==1)<TIME_STEP/TAU_ELAST) d_state[2*index+j]=0;
		}
	}
}

__global__ void Kernel_ConstructActivatedPorbabilityTower(REAL *d_tower, const REAL *d_sigma){
	// compute idx and idy, the location of the element in the original LX*LY array 
	int idx = blockIdx.x*blockDim.x+threadIdx.x;
	int idy = blockIdx.y*blockDim.y+threadIdx.y;
	if ( idx < LX && idy < LY){
		int index = idx*LY + idy;
		const REAL actualsigma = d_sigma[index];
		d_tower[index] = (actualsigma>CRITICAL_STRESS)*exp((actualsigma-STRESS_THRESHOLD)/TEMP_EFF);
		//d_tower[index] = exp((d_sigma[index] - STRESS_THRESHOLD)/TEMP_EFF)/TAU_PLAST;
		//if (idx==0&idy==3) printf("e %f %f \n",d_sigma[index],d_tower[index]);
	}
}

__global__ void Kernel_UpdateChosenSite(bool *d_state, unsigned int chosen){
	int tid = blockIdx.x*blockDim.x+threadIdx.x;
	if(tid==0) d_state[chosen]=1;
}
#endif


#ifdef TRACERS
__global__ void Kernel_CalculateDisplacementField(COMPLEX *dux, COMPLEX *duy, const REAL *ox, const REAL *oy, const COMPLEX *epsdot, const REAL scale)
{
	// compute idx and idy, the location of the element in the original LX*LY array 
	int idx = blockIdx.x*blockDim.x+threadIdx.x;
	int idy = blockIdx.y*blockDim.y+threadIdx.y;
	if ( idx < LX && idy < (LY/2+1)){
		int index = idx*(LY/2+1) + idy;

		dux[index].y = ox[index]*epsdot[index].x*scale;
		duy[index].y = oy[index]*epsdot[index].x*scale;

		dux[index].x = -ox[index]*epsdot[index].y*scale;
		duy[index].x = -oy[index]*epsdot[index].y*scale;

	}
}

__global__ void Kernel_UpdateTracersPositions(const REAL *dux, const REAL *duy, COMPLEX *tracer, const REAL dt)
{
	int tid = blockIdx.x*blockDim.x+threadIdx.x;
	if ( tid < NTRACERS){
		//int tracerlocation = int(tracer[tid].x+0.5)*(int(tracer[tid].x+0.5)!=LX)*LY + \
					int(tracer[tid].y+0.5)*(int(tracer[tid].y+0.5)!=LY);
		//Put the thing into the box
		//TODO: Improove this workaround
		int actualx = (tracer[tid].x < 0)?int(tracer[tid].x+0.5)+(int(abs(tracer[tid].x)/LX)+2)*LX:int(tracer[tid].x+0.5);
		int actualy = (tracer[tid].y < 0)?int(tracer[tid].y+0.5)+(int(abs(tracer[tid].y)/LY)+2)*LY:int(tracer[tid].y+0.5);
		int tracerlocation = ((actualx)%LX)*LY + (actualy)%LY;
		//int tracerlocation = ((int(tracer[tid].x+0.5)+1000*LX)%LX)*LY + (int((tracer[tid].y+0.5)+1000*LY)%LY);

		tracer[tid].x+=dux[tracerlocation]*dt;
		tracer[tid].y+=duy[tracerlocation]*dt;

		//assert(tracer[tid].x<1e20);
		//assert(tracer[tid].y<1e20);
		//assure PBC
		//tracer[tid].x=tracer[tid].x-LX*(tracer[tid].x>=LX)+LX*(tracer[tid].x<0);
		//tracer[tid].y=tracer[tid].y-LY*(tracer[tid].y>=LY)+LY*(tracer[tid].y<0);
	}
}
#endif

//////////-------------------------Calculate Functions---------------///////////////////////////

#ifdef TRACERS
__global__ void Kernel_CalculateMeanSquareDisplacement(const COMPLEX *a, const COMPLEX *b, REAL *r){
	const unsigned int tid = blockIdx.x*blockDim.x+threadIdx.x;
	if (tid < NTRACERS){
		r[tid] = (a[tid].x-b[tid].x)*(a[tid].x-b[tid].x) + (a[tid].y-b[tid].y)*(a[tid].y-b[tid].y);
		//if (tid==0) printf("%f \n",r[tid]);
	}
}

__global__ void Kernel_CalculateTrajectoriesSF(const COMPLEX *tracer, const COMPLEX *tracertw, \
			REAL *structure_factor, int nx, int ny){

	unsigned int i = blockIdx.x*blockDim.x+threadIdx.x;
	unsigned int j = blockIdx.y*blockDim.y+threadIdx.y;
	unsigned int index = i*LY+j;
	if ( i < LX && j < LY){
		REAL qx=2.0*M_PI*i/(REAL)LX;
		REAL qy=2.0*M_PI*j/(REAL)LY;

		REAL sf=0.0;
		//Still can paralelize this loop with  thrird dimension in the kernel
		for(unsigned int k=0; k<NTRACERS; k++){
			REAL dotproduct = qx*(tracer[k].x - tracertw[k].x)+qy*(tracer[k].y - tracertw[k].y);
			sf += cos(dotproduct);
		}
		structure_factor[index]=sf/(REAL)NTRACERS;
	}
}


/*__global__ void Kernel_CalculateTrajectoriesSF3D(const COMPLEX *tracer, const COMPLEX *tracertw, \
			REAL *structure_factor, int nx, int ny){

	unsigned int i = blockIdx.x*blockDim.x+threadIdx.x;
	unsigned int j = blockIdx.y*blockDim.y+threadIdx.y;
	unsigned int k = blockIdx.z*blockDim.z+threadIdx.z;
	unsigned int index = i*LY+j;
	if ( i < LX && j < LY && k < NTRACERS/TILE){

		REAL qx=2.0*M_PI*i/(REAL)LX;
		REAL qy=2.0*M_PI*j/(REAL)LY;

		REAL s_sf=0.0;
		//Still can paralelize this loop with  thrird dimension in the kernel
		for(unsigned int kk=0; kk<TILE; kk++){
			REAL dotproduct = qx*(tracer[k*TILE+kk].x - tracertw[k*TILE+kk].x)+qy*(tracer[k*TILE+kk].y - tracertw[k*TILE+kk].y);
			s_sf += cos(dotproduct);
		}
	}

	__shared__ s_sfforreduction[];

	structure_factor[index]=s_sf/(REAL)NTRACERS;
}
*/
#endif

#ifdef VISUALIZATION
__global__ void CUDAkernel_ComputeStructureFactor(const COMPLEX *tracer, const COMPLEX *tracertw, \
						REAL *d_Sq2d, int nx, int ny){
	int i = blockIdx.x*blockDim.x+threadIdx.x;
	int j = blockIdx.y*blockDim.y+threadIdx.y;
	if ( i < nx  && j < ny){
		unsigned int k=i*(LY/2+1)+j;
		unsigned int kk=i*LY+j;
		unsigned int kkk=(i+1)*LY-j;

		REAL qx=2.0*M_PI*double(i)/LX;
		REAL qy=2.0*M_PI*double(j)/LY;
		//REAL q2=qx*qx+qy*qy;
		d_Sq2d[kk] = 0;
		for(unsigned int i=0; i<NTRACERS; i++){
			REAL dotproduct = qx*(tracer[k].x - tracertw[k].x)+qy*(tracer[k].y - tracertw[k].y);
			d_Sq2d[kk] += cos(dotproduct);
		}
		d_Sq2d[kk] /=NTRACERS;
		d_Sq2d[kkk] = d_Sq2d[kk];
	}
}

__global__ void CUDAkernel_ComputeStructureFactorImage(const REAL *d_Sq2d, REAL *d_Sq2dimage, int nx, int ny){
	int idx = blockIdx.x*blockDim.x+threadIdx.x;
	int idy = blockIdx.y*blockDim.y+threadIdx.y;
	if ( idx < nx  && idy < ny){
		unsigned int k=idx*LY+idy;
		int ii=(idx>LX/2)?(idx-LX/2):(idx+LX/2);
		int jj=(idy>LY/2)?(idy-LY/2):(idy+LY/2);
		unsigned int kk=ii*LY+jj;
		d_Sq2dimage[kk]=d_Sq2d[k];
	}
}
#endif

