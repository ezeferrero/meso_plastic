/*
 * main_mesoplastic.cu
 *
 *  started on: May 10, 2013
 *      Author: eze
 * .....based on: main.cu by Kirsten
 */

//TODO: defines in Makefile
#ifndef LX
#define LX 256
#endif
#ifndef LY
#define LY LX 
#endif
//TODO: check for rectangular geometries //DONE, seems that it works //check again

#define NN (LX*LY)
#define SCALE (1.0/NN)

#ifndef TIME_STEP
#define TIME_STEP 0.01
#endif


//#define MAX_STEPS (int) (RUN_TIME/TIME_STEP + 0.1)
//#define TRAN_STEPS (int) (TIME_TO_REACH_NESS/TIME_STEP + 0.1)

#ifndef STRESS_THRESHOLD
#define STRESS_THRESHOLD 1.0
#endif

#ifndef TAU_ELAST
#define TAU_ELAST 1.0
#endif

#ifndef TAU_PLAST
#define TAU_PLAST 1.0
#endif

#ifndef CRITICAL_STRESS
#define CRITICAL_STRESS 0.01
#endif

#ifndef TEMP_EFF
#define TEMP_EFF 0.007
#endif

#ifndef GAMMADOT_MIN
#define GAMMADOT_MIN 0.001
#endif

#ifndef GAMMADOT_MAX
#define GAMMADOT_MAX 10.00
#endif

#ifndef GAMMADOT_POINTS
#define GAMMADOT_POINTS 80
#endif

#define SMALL_TRANSIENT_T 4 //10
#define BIG_TRANSIENT_T 100 //100
#define SMALL_RUNNING_T 8 //20
#define BIG_RUNNING_T 200 //100
#define SMALL_DATA_T 0.2 //0.05
#define BIG_DATA_T 1.0

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

//TODO: Tunne this to optimize
#define TILE_X 2
#define TILE_Y 128
#define TILE 128

#define CUDA_DEVICE 0

// Functions
#define MAX(a,b) (((a)<(b))?(b):(a))	// maximum
#define MIN(a,b) (((a)<(b))?(a):(b))	// minimum
#define ABS(a) (((a)< 0)?(-a):(a))	// absolute value

// Hardware parameters for Tesla c2075 (GF100) cc 2.0
#define SHARED_PER_BLOCK 49152
#define WARP_SIZE 32
#define THREADS_PER_BLOCK 1024
#define BLOCKS_PER_GRID 65535

#include "mesoplastic_library.cuh"
#include "../common/timers.hpp"


REAL TimeLoop(mesoplastic_model &T, const REAL gamma_p, const unsigned int t_transient, const unsigned int t_run, \
									const unsigned int t_data){

	unsigned int count=0;
	double ssstress=0;

	for(unsigned int i=0; i<t_transient; i++){
		T.UpdateStateVariables(i);
		T.ComputePlasticStrain();
		T.TransformToFourierSpace();
		T.Convolution();
		T.AntitransformFromFourierSpace();
		T.EulerIntegrationStep(gamma_p);
	}

	unsigned int t=0;
	while(t<t_run){

	//Measurements
		if(t%t_data==0){
			ssstress+= T.CalculateSigmaAv()/(REAL) NN;
			count+=1;
		}

	//Dynamics
		#ifdef ACTIVATED_DYNAMICS
		int firstplasticactive = T.CheckForPlasticActivity(); 
		// It will return the position of the first 1 or the size of the array (NN) if there are none
		if(firstplasticactive==NN){
			//cout << "### KMC move, at time" << t*TIME_STEP << endl;
			t+=(int)(T.ConstructActivatedPorbabilityTowerChooseOneEventAndUpdate(t)/TIME_STEP +0.5);
		}else{
			//cout << "### Normal setp, at time" << t << endl;
			T.UpdateStateVariablesActivated(t);
			t++;
		}
		#else
		// Update n(x,z;t) state variables: 0/1 corresponds to elastic/plastic
		T.UpdateStateVariables(t);
		t++;
		#endif

		// Calculation of the plastic strain "epsilon_dot" ~ n*sigma
		T.ComputePlasticStrain();
		// Calculation of the convolution with the propagator and the plastic strain
		// we transform the plastic strain epsilon_dot
		T.TransformToFourierSpace();
		//convolution
		T.Convolution();
	}
	cout << " " << endl;

	return ssstress/(REAL) count;
}


// main routine
int main(){

	//TODO: complete asserts
	assert(TILE_X%2==0);
	assert(TILE_Y%2==0);
	assert(LX%TILE_X==0);
	assert(LY%TILE_Y==0);
	//assert(MAX_STEPS%DATA==0);
	//We've assumed a small temporal step in the integration scheme
	assert(TIME_STEP<=0.1);

	// Set the GPGPU computing device
	#ifdef CUDA_DEVICE
	CUDA_SAFE_CALL(cudaSetDevice(CUDA_DEVICE));
	#endif

	// Choosing less "shared memory", more cache.
	CUDA_SAFE_CALL(cudaThreadSetCacheConfig(cudaFuncCachePreferL1)); 

	#ifdef ACTIVATED_DYNAMICS
	// Initialize random(), it will be used for the KMK Poissonian time
	srandom(RANDOM_SEED);
	#endif

	std::setprecision (15);

	//print simulation parameters
	cout << "### LX ................................: " << LX                << endl;
	cout << "### LY ................................: " << LY                << endl;
	cout << "### Time Step..........................: " << TIME_STEP         << endl;
	cout << "### Stress Thershold...................: " << STRESS_THRESHOLD  << endl;
	cout << "### Recovery rate unit (tau_elast).....: " << TAU_ELAST         << endl;
	cout << "### Activation rate unit (tau_plast)...: " << TAU_PLAST         << endl;
	cout << "### Shear rate min ....................: " << GAMMADOT_MIN      << endl;
	cout << "### Shear rate max ....................: " << GAMMADOT_MAX      << endl;
	cout << "### Shear rate points .................: " << GAMMADOT_POINTS   << endl;
	cout << "### Transient time for gamma_dot <= 0.1: " << SMALL_TRANSIENT_T << endl;
	cout << "### Running time for gamma_dot <= 0.1..: " << SMALL_RUNNING_T   << endl;
	cout << "### Data aquiring for gamma_dot <= 0.1.: " << SMALL_DATA_T      << endl;
	cout << "### Transient time for gamma_dot > 0.1.: " << BIG_TRANSIENT_T   << endl;
	cout << "### Running time for gamma_dot > 0.1...: " << BIG_RUNNING_T     << endl;
	cout << "### Data aquiring for gamma_dot > 0.1..: " << BIG_DATA_T        << endl;
	cout << "### SEED Philox .......................: " << PHILOX_SEED       << endl;
	#ifdef ACTIVATED_DYNAMICS
	cout << "### SEED Random .......................: " << RANDOM_SEED       << endl;
	cout << "### Critical stress cuttof.............: " << CRITICAL_STRESS   << endl;
	cout << "### Effective temperature .............: " << TEMP_EFF          << endl;
	#endif

	// Our class
	mesoplastic_model T;

	// Turn on cronometer
	cpu_timer clock;
	clock.tic();

	char flowcurvefilename [200];
	sprintf(flowcurvefilename, "flowcurve_LX%dLY%d_dt%2.2f.dat",LX , LY, TIME_STEP);
	ofstream outflow(flowcurvefilename);

	REAL exponent_min= log10(GAMMADOT_MIN);
	REAL exponent_step= (log10(GAMMADOT_MAX)-log10(GAMMADOT_MIN))/(REAL)GAMMADOT_POINTS;

	for(int k=0; k<GAMMADOT_POINTS+1; k++)
	{
		REAL gamma_p=pow(10.0,exponent_min+k*exponent_step);  // shear rate 

		unsigned int t_transient=(int)(SMALL_TRANSIENT_T/gamma_p/TIME_STEP+0.5);
		unsigned int t_run=(int)(SMALL_RUNNING_T/gamma_p/TIME_STEP+0.5);
		unsigned int t_data=(int)(SMALL_DATA_T/gamma_p/TIME_STEP+0.5);
		if(gamma_p>0.1){
			t_transient=(int)(BIG_TRANSIENT_T/gamma_p/TIME_STEP+0.5);
			t_run=(int)(BIG_RUNNING_T/gamma_p/TIME_STEP+0.5);
			t_data=(int)(BIG_DATA_T/gamma_p/TIME_STEP+0.5);
		}

		// Initialization
		//T.Initialize();
		T.InitializeOnDevice();

		//Time loop call, returns averaged stress
		REAL sigma_av = TimeLoop(T, gamma_p, t_transient, t_run,t_data);

		outflow << gamma_p << " "<< sigma_av << endl;

		// Print frames for visualization (includes cudaMemCpyDtoH)
		#ifdef VISUALIZATION
			#warning VISUALIZATION ON HERE
			T.Visualization(gamma_p);
		#endif
	}

	cout << "### ------ CUDA MESO_PLASTIC MODEL------ " << endl;
	// Turn off cronometer
	clock.tac();
	//clock.print();
	REAL total_time = clock.cpu_elapsed();
	cout << "### Total time    : " << total_time  << " ms" << endl;

	return 0;
}

