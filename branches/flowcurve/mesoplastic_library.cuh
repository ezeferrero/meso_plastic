/*
 * mesoplastic_library.cuh
 *
 *  started on: May 10, 2013
 *      Author: eze
 * .....based on: main.cu by Kirsten
 */

#ifndef MESOPLASTIC_LIBRARY_CUH
#define MESOPLASTIC_LIBRARY_CUH

// RNG: PHILOX
#include "../common/RNGcommon/Random123/philox.h"
#include "../common/RNGcommon/Random123/u01.h"

#include <iostream> 	/* std::cout, std::fixed */
#include <iomanip>		/* std::setprecision */
#include <cmath>
#include <fstream>
#include <string>

#include <assert.h>
#include <stdint.h> 	/* uint8_t */
#include <cufft.h>
#include <cstdlib>

#include <thrust/device_vector.h>
#include <thrust/reduce.h>
#include <thrust/transform.h>
#include <thrust/transform_reduce.h>
#include <thrust/find.h>
#include <thrust/scan.h>
#include <thrust/binary_search.h>
#include <thrust/iterator/constant_iterator.h>
#include <thrust/functional.h>

//Define random() Seed
#define RANDOM_SEED 7536951

//Define Philox Seed
#ifdef DOUBLE_PRECISION
	typedef r123::Philox2x64 RNG2;
	typedef r123::Philox4x64 RNG4;
	#define PHILOX_SEED 1332277027LLU
#else
	typedef r123::Philox2x32 RNG2;
	typedef r123::Philox4x32 RNG4;
	#define PHILOX_SEED 67321463
#endif

#include "../common/cuda_util.h"
#include "mesoplastic_kernels.cuh"
#include "../common/writeppm.h"
#include "../common/histo.h"

using namespace std;
using namespace thrust::placeholders;

//////////////////////////////////////////////////////////////////////
class mesoplastic_model
{
	private:
	cufftHandle plan_c2r;
	cufftHandle plan_r2c;

	public:
	// Pointer to host arrays (realspace)
	bool *h_state;
	REAL *h_sigma;
	REAL *h_propagator;

	// Pointer to device arrays (realspace and fourierspace)
	bool *d_state;
	REAL *d_sigma;
	COMPLEX *d_dsigma;
	REAL *d_dsigma_r;
	COMPLEX *d_epsilon_dot;
	REAL *d_epsilon_dot_r;
	REAL *d_propagator;
	#ifdef ACTIVATED_DYNAMICS
	REAL *d_tower;
	#endif

	// intializing the class
	mesoplastic_model(){

		//Array sizes
		size_t size_b = NN * sizeof(bool);
		size_t size_r = NN * sizeof(REAL);
		size_t size_c = LX*(LY/2+1) * sizeof(COMPLEX);
		size_t size_rc = LX*(LY/2+1) * sizeof(REAL);

		// Allocate array on host
		h_state = (bool *)malloc(sizeof(bool)*LX*LY);
		h_sigma = (REAL *)malloc(sizeof(COMPLEX)*LX*LY); //TODO:CHECK SIZE
		h_propagator = (REAL *)malloc(sizeof(REAL)*LX*LY);

		// Allocate array on device
		CUDA_SAFE_CALL(cudaMalloc((void **) &d_state, size_b));
		CUDA_SAFE_CALL(cudaMalloc((void **) &d_sigma, size_r));
		CUDA_SAFE_CALL(cudaMalloc((void **) &d_dsigma, size_c));
		CUDA_SAFE_CALL(cudaMalloc((void **) &d_dsigma_r, size_r));
		CUDA_SAFE_CALL(cudaMalloc((void **) &d_epsilon_dot, size_c));
		CUDA_SAFE_CALL(cudaMalloc((void **) &d_epsilon_dot_r, size_r));
		CUDA_SAFE_CALL(cudaMalloc((void **) &d_propagator, size_rc));
		#ifdef ACTIVATED_DYNAMICS
		CUDA_SAFE_CALL(cudaMalloc((void **) &d_tower, size_r));
		#endif

		//TODO: Preventive fill everything with 0s
		//for (unsigned int k=0; k<LX*LY; k++) h_sigma[k] = 0.0;
		//CUDA_SAFE_CALL(cudaMemset(d_dsigma, 0, LX*LY*sizeof(COMPLEX))); 

		// cuFFT plan
		#ifdef DOUBLE_PRECISION
		cufftPlan2d(&plan_c2r, LX, LY, CUFFT_Z2D);
		cufftPlan2d(&plan_r2c, LX, LY, CUFFT_D2Z);
		#else
		cufftPlan2d(&plan_c2r, LX, LY, CUFFT_C2R);
		cufftPlan2d(&plan_r2c, LX, LY, CUFFT_R2C);
		#endif
		//cufftSetCompatibilityMode(plan_c2r, CUFFT_COMPATIBILITY_NATIVE);
		//cufftSetCompatibilityMode(plan_r2c, CUFFT_COMPATIBILITY_NATIVE);
	}

	//////------- Class' Functions ------//////

	//--------Initialization Functions------//

	//Initializes on Host and CpyToDevice
	void Initialize(){

		// Initialize trivial host arrays
		for(int i = 0; i<LX; i++)
			for(int j = 0; j<LY; j++)
			{
				int ij = i*LY+j;
				h_sigma[ij] = STRESS_THRESHOLD;
				h_state[ij] = 0;
			}
		h_state[0]=1;
		h_state[LX/2*LY+LY/2]=1;

		// Initialize host array for propagator
		//TODO: Check this problematic array
		for(int i = 0; i<LX/2+1; i++)
			for(int j = 0; j<(LY/2+1); j++)
			{

			REAL qx=2.0*M_PI*double(i)/LX;
			REAL qy=2.0*M_PI*double(j)/LY;
			REAL q2=qx*qx+qy*qy;

			if(i!=0 || j!=0) h_propagator[i*(LY/2+1)+j] = -4*qx*qx*qy*qy/(q2*q2);
			if(i!=0) h_propagator[(LX-i)*(LY/2+1)+j] = h_propagator[i*(LY/2+1)+j];
			}
		h_propagator[0] =-1.0; // amplitude of the plastic event

		size_t size_b = NN * sizeof(bool);
		size_t size_r = NN * sizeof(REAL);
		size_t size_rc = LX*(LY/2+1) * sizeof(REAL);

		cudaMemcpy(d_sigma, h_sigma, size_r ,cudaMemcpyHostToDevice);
		cudaMemcpy(d_state, h_state, size_b ,cudaMemcpyHostToDevice);
		cudaMemcpy(d_propagator, h_propagator, size_rc, cudaMemcpyHostToDevice);
	}

	void InitializeOnDevice(){

		// Initialize State
		thrust::device_ptr<bool> state_ptr (d_state);
		thrust::device_vector<bool> tmp(NN, 0);
		tmp[0]=1;
		tmp[LX/2*LY+LY/2]=1; //(random() & 0x1u);
		thrust::copy(tmp.begin(),tmp.end(),state_ptr);

		// Initialize Stress
		thrust::device_ptr<REAL> sigma_ptr (d_sigma);
		thrust::fill(sigma_ptr, sigma_ptr+NN, STRESS_THRESHOLD);

		// Initialize Propagator
		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(LX/TILE_X/2+1, LY/TILE_Y/2+1);
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);
		Kernel_SetPropagator<<<dimGrid, dimBlock>>>(d_propagator,LX/2+1,LY/2+1);
		//TODO: Try to fit propagator in constant or texture memory
		//CUDA_SAFE_CALL(cudaBindTexture(NULL, propagatorTex, d_propagator, LX*(LY/2+1)*sizeof(REAL)));
	}


	//-----------Update Functions---------//

	void UpdateStateVariables(int time){
		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(LX/2/TILE_X, LY/TILE_Y); //using PhiloxPair
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);
		Kernel_UpdateStateVariables<<<dimGrid, dimBlock>>>(d_sigma, d_state, time);
	}

	#ifdef ACTIVATED_DYNAMICS
	void UpdateStateVariablesActivated(int time){
		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(LX/2/TILE_X, LY/TILE_Y); //using PhiloxPair
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);
		Kernel_UpdateStateVariablesActivated<<<dimGrid, dimBlock>>>(d_sigma, d_state, time);
	}
	#endif

	void ComputePlasticStrain(){
		thrust::device_ptr<REAL> sigma_ptr (d_sigma);
		thrust::device_ptr<bool> state_ptr (d_state);
		thrust::device_ptr<REAL> epsilon_ptr (d_epsilon_dot_r);
		//TODO: This can be a transform by key or something takig advantage of the bool
		// epsilon_dot <- sigma * state
		thrust::transform(sigma_ptr, sigma_ptr+NN, state_ptr, epsilon_ptr, thrust::multiplies<REAL>());
		//NOTICE:There is a factor of 1/2 missing in the calculation of epsilon_dot, that would cancell with the factor \
		of 2 that is missing in the calculus of the d_dsigma later on. Everyone happy.
	}

	void TransformToFourierSpace(){
		CUFFT_SAFE_CALL(cufftExecR2C(plan_r2c, d_epsilon_dot_r, d_epsilon_dot));
	}

	void Convolution(){
		//TODO: Do this with Thrust.
	/*	thrust::device_ptr<COMPLEX> epsilon_ptr (d_epsilon_dot);
		thrust::device_ptr<REAL> propagator_ptr (d_propagator);
		thrust::device_ptr<COMPLEX> state_ptr (d_dsigma);
		//AND NOW? SAXPY with complex type???
		thrust::transform(epsilon_ptr, epsilon_ptr+NN, propagator_ptr, state_ptr, 
				SCALE * _1 * _2); // placeholder expression
	*/
		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(LX/TILE_X, LY/TILE_Y/2+1);
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);
		Kernel_ComplexPointwiseMulAndScale<<<dimGrid, dimBlock>>>(d_epsilon_dot, d_propagator, d_dsigma, SCALE);
	}

	void AntitransformFromFourierSpace(){
		CUFFT_SAFE_CALL(cufftExecC2R(plan_c2r, d_dsigma, d_dsigma_r));
	}

	void EulerIntegrationStep(float shear_rate){
		//TODO: If the single other term  in the equation of motion besides the convolution is the shear rate, we \
		can perform Euler directly in fourier, applying gamma to the 0 mode, and then antitransform the new sigma.
		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(LX/TILE_X, LY/TILE_Y);
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);
		Kernel_EulerIntegrationStep<<<dimGrid, dimBlock>>>(d_sigma, d_dsigma_r, shear_rate, TIME_STEP);
	}

	#ifdef ACTIVATED_DYNAMICS
	int CheckForPlasticActivity(){
		thrust::device_ptr<bool> state_ptr (d_state);
		thrust::device_ptr<bool> iter;
		iter = thrust::find(state_ptr, state_ptr+NN,1);
		int position = iter - state_ptr;
		return (position);
	}

	REAL ConstructActivatedPorbabilityTowerChooseOneEventAndUpdate(int time){
		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(LX/TILE_X, LY/TILE_Y);
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);
		Kernel_ConstructActivatedPorbabilityTower<<<dimGrid, dimBlock>>>(d_tower, d_sigma);

		thrust::device_ptr<REAL> tower_ptr (d_tower);

		//REAL totalprobability = thrust::reduce(tower_ptr,tower_ptr+NN);
		thrust::inclusive_scan(tower_ptr,tower_ptr+NN,tower_ptr); //in-place inclusive scan

		//CPY to recover the total sum
		REAL totalprobability;
		CUDA_SAFE_CALL(cudaMemcpy(&totalprobability, &d_tower[NN-1], sizeof(REAL), cudaMemcpyDeviceToHost));

		//thrust::transform(d_tower.begin(),d_tower.end(), \
				thrust::make_constant_iterator(totalprobability), \
				d_tower.begin(),thrust::divides<REAL>());
		//Istead doing the later we will multiply by totalsum the random number

		thrust::device_ptr<REAL> chosen = thrust::lower_bound(tower_ptr,tower_ptr+NN,(random()/RAND_MAX)*totalprobability);
		Kernel_UpdateChosenSite<<<1,1>>>(d_state, chosen-tower_ptr);
		REAL timeincrement = totalprobability/((-1.)*log((REAL)random()/RAND_MAX));

		return timeincrement;
	}
	#endif

	//-----------Calculate Functions---------//

	REAL CalculateSigmaAv(){
		thrust::device_ptr<REAL> sigma_ptr (d_sigma);
		return thrust::reduce(sigma_ptr, sigma_ptr+NN);
	}


	#ifdef VISUALIZATION
	void Visualization(float gamma_p){
		CpyDeviceToHost();

		char filename[200];
		sprintf(filename, "framesigma%5f.ppm", gamma_p);
		PrintPicture(filename,0);
		sprintf(filename, "framestate%5f.ppm", gamma_p);
		PrintPicture(filename,1);

		char cmd[200];
		sprintf(cmd, "convert framesigma%5f.ppm framestate%5f.ppm +append frame_Sigma-State%5f.ppm", gamma_p, gamma_p, gamma_p);
		system(cmd);
		sprintf(cmd, "rm framesigma%5f.ppm framestate%5f.ppm", gamma_p, gamma_p);
		system(cmd);
	}
	#endif

	//-----------Copy Functions---------//

	/* transfer from GPU to CPU memory */
	void CpyDeviceToHost(){
		size_t size_b = NN * sizeof(bool);
		size_t size_r = NN * sizeof(REAL);
		CUDA_SAFE_CALL(cudaMemcpy(h_state, d_state, size_b, cudaMemcpyDeviceToHost));
		CUDA_SAFE_CALL(cudaMemcpy(h_sigma, d_sigma, size_r, cudaMemcpyDeviceToHost));
	}

	//-----------Print Functions---------//

	void PrintState(ofstream &fstr){
		for(int i=0;i<LX;i++){
			for(int j=0;j<LY;j++){
				int k=i*LY+j;
				fstr << h_state[k] << " ";
			}
			fstr << endl;
		}
		fstr << endl;
	};

	void PrintSigma(ofstream &fstr){
		for(int i=0;i<LX;i++){
			for(int j=0;j<LY;j++){
				int k=i*LY+j;
				fstr << h_sigma[k] << " ";
			}
			fstr << endl;
		}
		fstr << endl;
	};

	void PrintPicture(char *picturename, int who){
		if (who==0) writePPMbinaryImage(picturename, h_sigma);
		if (who==1) writePPMbinaryImage_bool(picturename, h_state);
		//char cmd[200];
		//sprintf(cmd, "convert frame%d.ppm frame%d.jpg", 100000000+x, 100000000+x);
		//system(cmd);
		//sprintf(cmd, "rm frame%d.ppm", 100000000+x);
		//system(cmd);
	};

	void PrintResults(){
		string outputFile = "sigma.dat";
		ofstream out(outputFile.c_str());
		if(out)
		{
			for (int j=0; j<LX; j++) 
			{
			  for (int i=0; i<LX; i++) 
			    out << h_sigma[j*LX+i] << "\t";
			  out << endl;
			}
		}
		else
		cerr<<"ERROR!"<<endl;

		string outputFile2 = "state.dat";
		ofstream out2(outputFile2.c_str());
		if(out2)
		{
			for (int j=0; j<LX; j++) 
			{
			  for (int i=0; i<LX; i++) 
			    out2 << h_state[j*LX+i] << "\t";
			  out2 << endl;
			}
		}
		else
		cerr<<"ERROR!"<<endl;
	}

	~mesoplastic_model(){

		//TODO: Complete frees
		/* frees CPU memory */
		free(h_state);
		free(h_sigma);
		free(h_propagator);

		/* frees GPU memory */
		cudaFree(d_state);
		cudaFree(d_sigma);
		cudaFree(d_epsilon_dot); cudaFree(d_epsilon_dot_r);
		cudaFree(d_dsigma); cudaFree(d_dsigma_r);
		cudaFree(d_propagator);

		cufftDestroy(plan_r2c);
		cufftDestroy(plan_c2r);
	};
};


#endif /*  MESOPLASTIC_LIBRARY_CUH */
