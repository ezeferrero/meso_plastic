
#ifdef DOUBLE_PRECISION
typedef double REAL;
typedef cufftDoubleComplex COMPLEX;
#else
typedef float REAL;
typedef cufftComplex COMPLEX;
#endif

/*--------RNG ROUTINES--------*/

__device__
REAL DeviceBoxMuller(const REAL u1, const REAL u2)
{
#ifdef DOUBLE_PRECISION
	REAL r = sqrt( -2.0*log(u1) );
	REAL theta = 2.0*M_PI*u2;
	return r*sin(theta);
#else
	REAL r = sqrtf( -2.0*logf(u1) );
	REAL theta = 2.0*M_PI*u2;
	return r*sinf(theta);
#endif
}


__device__
void PhiloxRandomQuartet(const unsigned int index, const unsigned int time, REAL *r1, REAL *r2, REAL *r3, REAL *r4)
{
	RNG4 rng;
	RNG4::ctr_type c_pair={{}};
	RNG4::key_type k_pair={{}};
	RNG4::ctr_type r_quartet;

		// keys = threadid
		k_pair[0]= index;
		// time counter
		c_pair[0]= time;
		c_pair[1]= PHILOX_SEED; // eventually do: PHILOX_SEED + sample;
		// random number generation
		r_quartet = rng(c_pair, k_pair);

	#ifdef DOUBLE_PRECISION
		*r1= u01_closed_closed_64_53(r_quartet[0]);
		*r2= u01_closed_closed_64_53(r_quartet[1]);
		*r3= u01_closed_closed_64_53(r_quartet[2]);
		*r4= u01_closed_closed_64_53(r_quartet[3]);
	#else
		*r1= u01_closed_closed_32_53(r_quartet[0]);
		*r2= u01_closed_closed_32_53(r_quartet[1]);
		*r3= u01_closed_closed_32_53(r_quartet[2]);
		*r4= u01_closed_closed_32_53(r_quartet[3]);
	#endif
}

__device__
void PhiloxRandomPair(const unsigned int index, const unsigned int time, REAL *r1, REAL *r2)
{
	RNG2 rng;
	RNG2::ctr_type c_pair={{}};
	RNG2::key_type k_pair={{}};
	RNG2::ctr_type r_pair;

		// keys = threadid
		k_pair[0]= index;
		// time counter
		c_pair[0]= time;
		c_pair[1]= PHILOX_SEED; // eventually do: PHILOX_SEED + sample;
		// random number generation
		r_pair = rng(c_pair, k_pair);

	#ifdef DOUBLE_PRECISION
		*r1= u01_closed_closed_64_53(r_pair[0]);
		*r2= u01_closed_closed_64_53(r_pair[1]);
	#else
		*r1= u01_closed_closed_32_53(r_pair[0]);
		*r2= u01_closed_closed_32_53(r_pair[1]);
	#endif
}


//---------- DEVICE FUNCTIONS -----------//

//--------Initialization Functions------//

__global__ void Kernel_SetPropagator(REAL *g, int nx, int ny){
	unsigned int i = blockIdx.x*blockDim.x+threadIdx.x;
	unsigned int j = blockIdx.y*blockDim.y+threadIdx.y;
	if ( i < nx && j < ny){
			REAL qx=2.0*M_PI*double(i)/LX;
			REAL qy=2.0*M_PI*double(j)/LY;
			REAL q2=qx*qx+qy*qy;

			if(i!=0 || j!=0) g[i*ny+j] = -4*qx*qx*qy*qy/(q2*q2);
			if(i!=0) g[(LX-i)*ny+j] = g[i*ny+j];
			if (i==0 && j==0) g[i*ny+j] = -1.0;

	}
}

	//-----------Update Functions---------//

/* //PhiloxQuadVersion: TODO: bad performance, why?
__global__ void Kernel_UpdateStateVariables(const REAL* d_sigma, bool* d_state, int time)
{
	// compute idx and idy, the location of the element in the original LX*LY array 
	int idx = blockIdx.x*blockDim.x+threadIdx.x;
	int idy = blockIdx.y*blockDim.y+threadIdx.y;
	if ( idx < LX/4 && idy < LY){
		int index = idx*LY + idy;
		bool actualstate[4]={0,0,0,0};
		for (int k=0;k<4;k++) actualstate[k]=d_state[4*index+k];
		REAL r[4]={0,0,0,0};
		PhiloxRandomQuartet(index, time, &r[0], &r[1],&r[2], &r[3]);
		for (int k=0;k<4;k++) d_state[4*index+k]= ((actualstate[k]==0 && d_sigma[4*index+k]>=STRESS_THRESHOLD && r[k]<TIME_STEP)|| \
		(actualstate[k]==1 && r[k]>TIME_STEP));
	}
}
*/

__global__ void Kernel_UpdateStateVariables(const REAL* d_sigma, bool* d_state, int time)
{
	// compute idx and idy, the location of the element in the original LX*LY array 
	int idx = blockIdx.x*blockDim.x+threadIdx.x;
	int idy = blockIdx.y*blockDim.y+threadIdx.y;
	if ( idx < LX/2 && idy < LY){
		int index = idx*LY + idy;
		int index1 = 2*index;
		int index2 = 2*index+1;
		const bool actualstate1 = d_state[index1];
		const bool actualstate2 = d_state[index2];
		REAL r1, r2;
		PhiloxRandomPair(index, time, &r1, &r2);
		d_state[index1]= ((actualstate1==0 && d_sigma[index1]>=STRESS_THRESHOLD && r1<TIME_STEP/TAU_PLAST)|| \
		(actualstate1==1 && r1>TIME_STEP/TAU_ELAST));
		d_state[index2]= ((actualstate2==0 && d_sigma[index2]>=STRESS_THRESHOLD && r2<TIME_STEP/TAU_PLAST)|| \
		(actualstate2==1 && r2>TIME_STEP/TAU_ELAST));
	//if (index==64) printf("%d %f %f \n", time, d_sigma[index1], d_sigma[index2]);
	}
}

#ifdef ACTIVATED_DYNAMICS
__global__ void Kernel_UpdateStateVariablesActivated(const REAL* d_sigma, bool* d_state, int time)
{
	// compute idx and idy, the location of the element in the original LX*LY array 
	int idx = blockIdx.x*blockDim.x+threadIdx.x;
	int idy = blockIdx.y*blockDim.y+threadIdx.y;
	if ( idx < LX/2 && idy < LY){
		int index = idx*LY + idy;
		bool actualstate;
		REAL actualsigma, lsigma;
		REAL r1, r2;
		PhiloxRandomPair(index, time, &r1, &r2);
		for (int j=0;j<2;j++)
		{
			actualstate = d_state[2*index+j];
			actualsigma = d_sigma[2*index+j];
			//lsigma = (actualsigma>CRITICAL_STRESS)*exp((actualsigma-STRESS_THRESHOLD)/TEMP_EFF)/TAU_PLAST;
			lsigma = exp((actualsigma-STRESS_THRESHOLD)/TEMP_EFF)/TAU_PLAST;
			if(actualstate==0 && r1*(j==0)+r2*(j==1)<lsigma) d_state[2*index+j]=1;
			if(actualstate==1 && r1*(j==0)+r2*(j==1)<TIME_STEP/TAU_ELAST) d_state[2*index+j]=0;
		}
	}
}
#endif

// Pointwise multiplication
static __global__ void Kernel_PointwiseMul(const REAL *a, const bool *b, REAL* c)
{
	// compute idx and idy, the location of the element in the original LX*LY array 
	int idx = blockIdx.x*blockDim.x+threadIdx.x;
	int idy = blockIdx.y*blockDim.y+threadIdx.y;
	if ( idx < LX && idy < LY){
		int index = idx*LY + idy;
		c[index] = a[index] * b[index];
	}
}

// COMPLEX pointwise multiplication
static __global__ void Kernel_ComplexPointwiseMulAndScale(const COMPLEX* a, const REAL* b, COMPLEX* c, REAL scale)
{
	// compute idx and idy, the location of the element in the original LX*LY array 
	int idx = blockIdx.x*blockDim.x+threadIdx.x;
	int idy = blockIdx.y*blockDim.y+threadIdx.y;
	if ( idx < LX && idy < (LY/2+1)){
		int index = idx*(LY/2+1) + idy;
		c[index].x = scale*(a[index].x * b[index]);
		c[index].y = scale*(a[index].y * b[index]);
	}
} 

static __global__ void Kernel_EulerIntegrationStep(REAL *a, const REAL *b, REAL gamma, REAL dt)
{
	// compute idx and idy, the location of the element in the original LX*LY array 
	int idx = blockIdx.x*blockDim.x+threadIdx.x;
	int idy = blockIdx.y*blockDim.y+threadIdx.y;
	if ( idx < LX && idy < LY){
		int index = idx*LY + idy;
		//There is a factor of 2 missing here (multipling b), that would cancell with the factor of 1/2 \
		that is missing in the calculus of b as well. Everyone happy.
		a[index]+=(gamma+b[index])*dt;
	}
}

#ifdef ACTIVATED_DYNAMICS
__global__ void Kernel_ConstructActivatedPorbabilityTower(REAL *d_tower, const REAL *d_sigma){
	// compute idx and idy, the location of the element in the original LX*LY array 
	int idx = blockIdx.x*blockDim.x+threadIdx.x;
	int idy = blockIdx.y*blockDim.y+threadIdx.y;
	if ( idx < LX && idy < LY){
		int index = idx*LY + idy;
		const REAL actualsigma = d_sigma[index];
		d_tower[index] = (actualsigma>CRITICAL_STRESS)*exp((actualsigma-STRESS_THRESHOLD)/TEMP_EFF);
		//d_tower[index] = exp((d_sigma[index] - STRESS_THRESHOLD)/TEMP_EFF)/TAU_PLAST;
		//if (idx==0&idy==3) printf("e %f %f \n",d_sigma[index],d_tower[index]);
	}
}

__global__ void Kernel_UpdateChosenSite(bool *d_state, unsigned int chosen){
	int tid = blockIdx.x*blockDim.x+threadIdx.x;
	if(tid==0) d_state[chosen]=1;
}
#endif

