/*
 * main_mesoplastic.cu
 *
 *  started on: May 10, 2013
 *      Author: eze
 * .....based on: main.cu by Kirsten
 */

//TODO: defines in Makefile
#ifndef LX
#define LX 256
#endif
#ifndef LY
#define LY LX 
#endif
//TODO: check for rectangular geometries //DONE, seems that it works //check again

#define NN (LX*LY)
#define SCALE (1.0/NN)

#ifndef TIME_STEP
#define TIME_STEP 0.01
#endif


//#define MAX_STEPS (int) (RUN_TIME/TIME_STEP + 0.1)
//#define TRAN_STEPS (int) (TIME_TO_REACH_NESS/TIME_STEP + 0.1)

#ifndef STRESS_THRESHOLD
#define STRESS_THRESHOLD 1.0
#endif

#ifndef TAU_ELAST
#define TAU_ELAST 1.0
#endif

#ifndef TAU_PLAST
#define TAU_PLAST 1.0
#endif

#ifndef CRITICAL_STRESS
#define CRITICAL_STRESS 0.01
#endif

#ifndef TEMP_EFF
#define TEMP_EFF 0.007
#endif

#ifdef SHEAR_RATE_LOOP
	#define GAMMADOT_MIN 0.0001
	#define GAMMADOT_MAX 0.0100
	#define GAMMADOT_POINTS 5
	#define SMALL_TRANSIENT_T 4 //10
	#define BIG_TRANSIENT_T 50 //100
	#define SMALL_RUNNING_T 8 //20
	#define BIG_RUNNING_T 50 //100
	#define SMALL_DATA_T 0.1 //0.05
	#define BIG_DATA_T 1.0
#else
	#define SHEAR_RATE 1.0
	#define TIME_TO_REACH_NESS (1/SHEAR_RATE)  // NESS: Non-Equilibrium Steady State 
	#define TRANSIENT_TIME (100.0/SHEAR_RATE)
	#define RUN_TIME 2*TRANSIENT_TIME
	#define DATA_AMOUNT 100
#endif

#ifdef CORRELATION
# define WAITING_T 10
//#define SSSTRESS(a) 0.76282 + 0.25612*a + 1.9662*a*a - 0.94216*a*a*a + 0.24374*a*a*a*a - 0.034583*a*a*a*a*a + 0.0025222*a*a*a*a*a*a - 7.3638e-05*a*a*a*a*a*a*a
//#define SSSTRESS(a) 0.75424 + 2.0694*a - 198.54*a*a + 11356*a*a*a
#endif

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

#ifdef TRACERS
#define NTRACERS 4096
#define NOFBINS 256
#define QMAX (8*M_PI*M_PI)
#endif


//TODO: Tunne this to optimize
#define TILE_X 2
#define TILE_Y 128
#define TILE 128

#define CUDA_DEVICE 0

// Functions
#define MAX(a,b) (((a)<(b))?(b):(a))	// maximum
#define MIN(a,b) (((a)<(b))?(a):(b))	// minimum
#define ABS(a) (((a)< 0)?(-a):(a))	// absolute value

// Hardware parameters for Tesla c2075 (GF100) cc 2.0
#define SHARED_PER_BLOCK 49152
#define WARP_SIZE 32
#define THREADS_PER_BLOCK 1024
#define BLOCKS_PER_GRID 65535

#include "mesoplastic_library.cuh"
#include "../common/timers.hpp"


REAL TimeLoop(mesoplastic_model &T, REAL gamma_p, REAL t_transient, REAL t_run, REAL t_data){

	REAL sigma_av=0.0;
	unsigned int count=0;
	unsigned int count1=0;
	REAL t = 0; //STRESS_THRESHOLD/SHEAR_RATE ;
	unsigned int i=0;
	bool set_tw=1;
	REAL ro_zero2=0;
	double ssstress=0;
	char filename[100];
	sprintf(filename, "Sk_LX%iLY%igammadot%.5fDt%.2f.dat",LX,LY,gamma_p,TIME_STEP);
	ofstream file(filename);

	while(t < t_run){
	//TODO: Probably is a better idea to loop time with an integer counter
	//while(t < RUN_TIME){
	//for(unsigned int i=0; i<int(t_run/TIME_STEP+0.5)+1; i++){

		/* if(t >= STRESS_THRESHOLD/SHEAR_RATE+count*(RUN_TIME-STRESS_THRESHOLD/SHEAR_RATE)/100.0){
			cout<<"flame-gpu:  "<<count<<" % "<<endl;
			count+=1;
		}
		*/

		//TODO: Use Thrust to replace some kernels
		#ifdef CORRELATION
		assert(WAITING_T>2);
		//if (i==int(2/(TIME_STEP*gamma_p)))
		if ((t>=2/gamma_p+count1*t_data) && (set_tw)){
			ssstress += T.ComputeSsstress();
			count1++;
		}
		if((t>=WAITING_T/gamma_p) && (set_tw)){
			ssstress = ssstress/(double)count1;
			set_tw = 0;
			cout << "## Waiting time for gamma_dot " << gamma_p << " tw= " << t << " WAITING_T= " << WAITING_T <<endl;
			cout << "## 1:gamma(t*gamma_p) 2:correlation 3:time" << endl;
			count1=0;
			ro_zero2 = T.ComputeFluctuationsAtTw(gamma_p, ssstress);
			#ifdef TRACERS
			T.SetTracersAtTW();
			#endif
		}
		if(t>=WAITING_T/gamma_p+count1*t_data*10){
			REAL corr = T.ComputeAutoCorrelationFunction(gamma_p, ssstress, ro_zero2);
			cout << t*gamma_p-WAITING_T << " " << corr << " " << t << endl;
			#ifdef TRACERS
			T.CalculateTrajectoriesStructureFactor();
			T.PrintStructureFactor(file, t, WAITING_T/gamma_p);
			//T.PrintSmallQStructureFactor(file, t);

			//T.ComputeStructureFactor();
			//T.ComputeStructureFactorImage();
			//sprintf(filename, "Sq%.2f.ppm", 100000000+t);
			//T.PrintImageStructureFactor(filename,t);

			#endif
			count1++;
		}
		#endif

		#ifdef ACTIVATED_DYNAMICS
		int firstplasticactive = T.CheckForPlasticActivity(); 
		// It will return the position of the first 1 or the size of the array if there are none.
		if(firstplasticactive==NN){
			//KMC move
			//cout << "### KMC jump, at time" << t << endl;
			t+=T.ConstructActivatedPorbabilityTowerChooseOneEventAndUpdate(i);
		}else{
			//cout << "### Normal setp, at time" << t << endl;
			T.UpdateStateVariablesActivated(i);
			t+=TIME_STEP;
		}
		#else
		// Update n(x,z;t) state variables: 0/1 corresponds to elastic/plastic
		T.UpdateStateVariables(i);
		t+=TIME_STEP;
		#endif

		// Calculation of the plastic strain "epsilon_dot" ~ n*sigma
		T.ComputePlasticStrain();

		// Calculation of the convolution with the propagator and the plastic strain
		// we transform the plastic strain epsilon_dot
		T.TransformToFourierSpace();
		//convolution
		T.Convolution();
		#ifdef TRACERS
		//displacement field u
		T.CalculateDisplacementField();
		#endif
		// we antitransform the result of the convolution "delta_sigma" (and eventually delta_u_x/y)
		T.AntitransformFromFourierSpace();

		// Update of the shear stress "sigma" with a simple Euler integration using delta_sigma and a \
		global elastic loading, the shear rate "gamma_dot" 
		T.EulerIntegrationStep(gamma_p);

		#ifdef TRACERS
		//Update tracers positions preserving PBC
		T.UpdateTracers();
		#endif

		//if (i%DATA==0 && i>TRAN_STEPS) sigma_av+= T.CalculateSigmaAv()/(double) NN;
		if(t>=t_transient+count*t_data){
			sigma_av+= T.CalculateSigmaAv()/(REAL) NN;
			//#ifdef TRACERS
			//T.PrintTracers();
			//#endif
			count+=1;
		}

		i++;
	}
	cout << " " << endl;
	return sigma_av/(REAL) count;
}


// main routine
int main(){

	//TODO: cpmplete asserts
	assert(TILE_X%2==0);
	assert(TILE_Y%2==0);
	assert(LX%TILE_X==0);
	assert(LY%TILE_Y==0);
	//assert(MAX_STEPS%DATA==0);
	//We've assumed a small temporal step in the integration scheme
	assert(TIME_STEP<=0.1);
	#ifdef TRACERS
	assert(NN%NTRACERS==0);
	assert(NTRACERS%TILE==0);
	#endif

	// Set the GPGPU computing device
	#ifdef CUDA_DEVICE
	CUDA_SAFE_CALL(cudaSetDevice(CUDA_DEVICE));
	#endif

	// Choosing less "shared memory", more cache.
	CUDA_SAFE_CALL(cudaThreadSetCacheConfig(cudaFuncCachePreferL1)); 

	#ifdef ACTIVATED_DYNAMICS
	// Initialize random(), it will be used for the KMK Poissonian time
	srandom(RANDOM_SEED);
	#endif

	std::setprecision (15);

	//print simulation parameters
	cout << "### LX ................................: " << LX                << endl;
	cout << "### LY ................................: " << LY                << endl;
	cout << "### Time Step..........................: " << TIME_STEP         << endl;
	cout << "### Stress Thershold...................: " << STRESS_THRESHOLD  << endl;
	cout << "### Recovery rate unit (tau_elast).....: " << TAU_ELAST         << endl;
	cout << "### Activation rate unit (tau_plast)...: " << TAU_PLAST         << endl;
	#ifdef SHEAR_RATE_LOOP
	cout << "### Shear rate min ....................: " << GAMMADOT_MIN      << endl;
	cout << "### Shear rate max ....................: " << GAMMADOT_MAX      << endl;
	cout << "### Shear rate points .................: " << GAMMADOT_POINTS   << endl;
	cout << "### Transient time for gamma_dot <= 0.1: " << SMALL_TRANSIENT_T << endl;
	cout << "### Running time for gamma_dot <= 0.1..: " << SMALL_RUNNING_T   << endl;
	cout << "### Data aquiring for gamma_dot <= 0.1.: " << SMALL_DATA_T      << endl;
	cout << "### Transient time for gamma_dot > 0.1.: " << BIG_TRANSIENT_T   << endl;
	cout << "### Running time for gamma_dot > 0.1...: " << BIG_RUNNING_T     << endl;
	cout << "### Data aquiring for gamma_dot > 0.1..: " << BIG_DATA_T        << endl;
	#else
	cout << "### Shear rate ........................: " << SHEAR_RATE        << endl;
	cout << "### Transient time ....................: " << TRANSIENT_TIME    << endl;
	cout << "### Running time ......................: " << RUN_TIME          << endl;
	cout << "### Data Amount .......................: " << DATA_AMOUNT       << endl;
	#endif
	#ifdef PHILOX
	cout << "### SEED Philox .......................: " << PHILOX_SEED       << endl;
	#else
	cout << "### SEED Rand48 .......................: " << RAND48_SEED       << endl;
	#endif
	#ifdef ACTIVATED_DYNAMICS
	cout << "### SEED Random .......................: " << RANDOM_SEED       << endl;
	cout << "### Critical stress cuttof.............: " << CRITICAL_STRESS   << endl;
	cout << "### Effective temperature .............: " << TEMP_EFF          << endl;
	#endif

	// Our class
	mesoplastic_model T;

	// Turn on cronometer
	cpu_timer clock;
	clock.tic();

	#ifdef SHEAR_RATE_LOOP
		char flowcurvefilename [200];
		sprintf(flowcurvefilename, "flowcurve_LX%dLY%d_dt%2.2f.dat",LX , LY, TIME_STEP);
		ofstream outflow(flowcurvefilename);

		REAL exponent_min= log10(GAMMADOT_MIN);
		REAL exponent_step= (log10(GAMMADOT_MAX)-log10(GAMMADOT_MIN))/GAMMADOT_POINTS;

		for(int k=0; k<GAMMADOT_POINTS+1; k++)
		{
			REAL gamma_p=pow(10.0,exponent_min+k*exponent_step);  // shear rate 

			REAL t_transient=SMALL_TRANSIENT_T/gamma_p;
			REAL t_run=t_transient+SMALL_RUNNING_T/gamma_p;
			REAL t_data=SMALL_DATA_T/gamma_p;
			if(gamma_p>0.1){
				t_transient=BIG_TRANSIENT_T/gamma_p;
				t_run=t_transient+BIG_RUNNING_T/gamma_p;
				t_data=BIG_DATA_T/gamma_p;
			}

			// Initialization
			//T.Initialize();
			T.InitializeOnDevice();

			//Time loop call, returns averaged stress
			REAL sigma_av = TimeLoop(T, gamma_p, t_transient, t_run,t_data);

			outflow << gamma_p << " "<< sigma_av << endl;

			// Print frames for visualization (includes cudaMemCpyDtoH)
			#ifdef VISUALIZATION
				#warning VISUALIZATION ON HERE
				T.Visualization(gamma_p);
			#endif
		}
	#else

		// Initialization
		//T.Initialize();
		T.InitializeOnDevice();

		// Adquiring time step
		REAL t_data=(RUN_TIME-TRANSIENT_TIME)/DATA_AMOUNT;
		//Time loop call, returns averaged stress
		REAL sigma_av = TimeLoop(T, SHEAR_RATE, TRANSIENT_TIME, RUN_TIME,t_data);

		//TODO:Visualization in time!!!!

		/* Print frames for visualization (includes cudaMemCpyDtoH */
		#ifdef VISUALIZATION
			#warning VISUALIZATION ON HERE
			T.Visualization(SHEAR_RATE);
		#endif

		#ifdef RAW_DATA
		// Retrieve result from device and store it in host array
		T.CpyDeviceToHost();

		// Print results
		//T.PrintResults();
		string output_file = "sigma.dat";
		ofstream out(output_file.c_str());
		T.PrintSigma(out);
		output_file = "state.dat";
		ofstream out2(output_file.c_str());
		T.PrintState(out2);

		/* Print frame for visualization */
		char filename [200];
		sprintf(filename, "lastframe.ppm");
		T.PrintPicture(filename,0);
		#endif

	#endif

	cout << "### ------ CUDA MESO_PLASTIC MODEL------ " << endl;
	// Turn off cronometer
	clock.tac();
	//clock.print();
	REAL total_time = clock.cpu_elapsed();
	cout << "### Total time    : " << total_time  << " ms" << endl;

	return 0;
}

