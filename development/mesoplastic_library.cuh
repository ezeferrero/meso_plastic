/*
 * mesoplastic_library.cuh
 *
 *  started on: May 10, 2013
 *      Author: eze
 * .....based on: main.cu by Kirsten
 */

#ifndef MESOPLASTIC_LIBRARY_CUH
#define MESOPLASTIC_LIBRARY_CUH

// RNG: PHILOX
#include "../common/RNGcommon/Random123/philox.h"
#include "../common/RNGcommon/Random123/u01.h"

// RNG RAND48
#include "../common/random.hpp"

#include <iostream> 	/* std::cout, std::fixed */
#include <iomanip>		/* std::setprecision */
#include <cmath>
#include <fstream>
#include <string>

#include <assert.h>
#include <stdint.h> 	/* uint8_t */
#include <cufft.h>
#include <cstdlib>

#include <thrust/device_vector.h>
#include <thrust/reduce.h>
#include <thrust/transform.h>
#include <thrust/transform_reduce.h>
#include <thrust/find.h>
#include <thrust/scan.h>
#include <thrust/binary_search.h>
#include <thrust/iterator/constant_iterator.h>
#include <thrust/functional.h>

//Define random() Seed
#define RANDOM_SEED 7536951

//Define Rand48 Seed
#define RAND48_SEED 123
#define FLOATSCALE (1.0/2147483647)
RNG_rand48 rand48(RAND48_SEED);

//Define Philox Seed
#ifdef DOUBLE_PRECISION
	typedef r123::Philox2x64 RNG2;
	typedef r123::Philox4x64 RNG4;
	#define PHILOX_SEED 1332277027LLU
#else
	typedef r123::Philox2x32 RNG2;
	typedef r123::Philox4x32 RNG4;
	#define PHILOX_SEED 67321463
#endif

#include "../common/cuda_util.h"
#include "mesoplastic_kernels.cuh"
#include "../common/writeppm.h"
#include "../common/histo.h"

using namespace std;
using namespace thrust::placeholders;

//////////////////////////////////////////////////////////////////////
class mesoplastic_model
{
	private:
	cufftHandle plan_c2r;
	cufftHandle plan_r2c;

	public:
	// Pointer to host arrays (realspace)
	bool *h_state;
	REAL *h_sigma;
	REAL *h_propagator;
	#ifdef TRACERS
	COMPLEX *h_tracers; /* Can be float2, we only want to have a 2 coordinate variable */ //TODO: Move this to a SOA
	#endif

	// Pointer to device arrays (realspace and fourierspace)
	bool *d_state;
	REAL *d_sigma;
	COMPLEX *d_dsigma;
	REAL *d_dsigma_r;
	COMPLEX *d_epsilon_dot;
	REAL *d_epsilon_dot_r;
	REAL *d_propagator;
	#ifdef TRACERS
	COMPLEX *d_tracers;  //TODO: Move this to a SOA
	COMPLEX *d_deltau_x; //TODO: Move this to a SOA
	COMPLEX *d_deltau_y;
	REAL *d_deltau_x_r;
	REAL *d_deltau_y_r;
	REAL *d_oseen_x;
	REAL *d_oseen_y;
	COMPLEX *d_tracerstw; //TODO: Move this to a SOA
	COMPLEX *d_structure_factor;
	COMPLEX *h_structure_factor;
	REAL *d_sf_distr; REAL *h_sf_distr;
	REAL *d_Sq2d, *d_Sq2dimage;
	REAL *h_Sq2dimage;
	#endif
	#ifdef ACTIVATED_DYNAMICS
	REAL *d_tower;
	#endif
	#ifdef CORRELATION
	REAL *d_fluctuations_tw;
	#endif


	int *random_d;

	// intializing the class
	mesoplastic_model(){

		// Allocate array on host
		h_state = (bool *)malloc(sizeof(bool)*LX*LY);
		h_sigma = (REAL *)malloc(sizeof(COMPLEX)*LX*LY);
		h_propagator = (REAL *)malloc(sizeof(REAL)*LX*LY);
		#ifdef TRACERS
		h_tracers = (COMPLEX *)malloc(NTRACERS*sizeof(COMPLEX));
		h_structure_factor = (COMPLEX *)malloc(LX*(LY/2+1)*sizeof(COMPLEX));

		CUDA_SAFE_CALL(cudaMalloc((void**)&d_Sq2d, sizeof(REAL)*LX*LY));
		CUDA_SAFE_CALL(cudaMalloc((void**)&d_Sq2dimage, sizeof(REAL)*LX*LY));
		h_Sq2dimage = (REAL *)malloc(sizeof(REAL)*LX*LY);

		#endif

		// Allocate array on device
		size_t size_b = NN * sizeof(bool);
		size_t size_r = NN * sizeof(REAL);
		size_t size_rc = LX*(LY/2+1) * sizeof(REAL);
		size_t size_c = LX*(LY/2+1) * sizeof(COMPLEX);
		size_t size_ui = NN * sizeof(int);

		cudaMalloc((void **) &d_state, size_b);
		cudaMalloc((void **) &d_sigma, size_r);
		cudaMalloc((void **) &d_dsigma, size_c);
		cudaMalloc((void **) &d_dsigma_r, size_r);
		cudaMalloc((void **) &d_epsilon_dot, size_c);
		cudaMalloc((void **) &d_epsilon_dot_r, size_r);
		cudaMalloc((void **) &d_propagator, size_rc);
		#ifdef TRACERS
		cudaMalloc((void **) &d_tracers, NTRACERS*sizeof(COMPLEX));
		cudaMalloc((void **) &d_deltau_x, size_c);
		cudaMalloc((void **) &d_deltau_y, size_c);
		cudaMalloc((void **) &d_deltau_x_r, size_r);
		cudaMalloc((void **) &d_deltau_y_r, size_r);
		cudaMalloc((void **) &d_oseen_x, size_rc);
		cudaMalloc((void **) &d_oseen_y, size_rc);
		cudaMalloc((void **) &d_tracerstw, NTRACERS*sizeof(COMPLEX));
		cudaMalloc((void **) &d_structure_factor, size_c);
		cudaMalloc((void **) &d_sf_distr, NOFBINS*sizeof(REAL));
		h_sf_distr = (REAL *)malloc(sizeof(REAL)*NOFBINS);
		#endif
		#ifdef ACTIVATED_DYNAMICS
		cudaMalloc((void **) &d_tower, size_r);
		#endif
		cudaMalloc((void **) &random_d, size_ui);
		#ifdef CORRELATION
		cudaMalloc((void **) &d_fluctuations_tw, size_r);
		#endif

		//TODO: Preventive fill with 0s
		// Fill everything with 0s 
		//for (unsigned int k=0; k<LX*LY; k++) h_sigma[k] = 0.0;
		//CUDA_SAFE_CALL(cudaMemset(d_sigma, 0, LX*LY*sizeof(REAL)));
		//CUDA_SAFE_CALL(cudaMemset(d_dsigma, 0, LX*LY*sizeof(COMPLEX))); 

		// cuFFT plan
		#ifdef DOUBLE_PRECISION
		cufftPlan2d(&plan_c2r, LX, LY, CUFFT_Z2D);
		cufftPlan2d(&plan_r2c, LX, LY, CUFFT_D2Z);
		#else
		cufftPlan2d(&plan_c2r, LX, LY, CUFFT_C2R);
		cufftPlan2d(&plan_r2c, LX, LY, CUFFT_R2C);
		#endif
		//cufftSetCompatibilityMode(plan_c2r, CUFFT_COMPATIBILITY_NATIVE);
		//cufftSetCompatibilityMode(plan_r2c, CUFFT_COMPATIBILITY_NATIVE);
	}

	//////------- Class' Functions ------//////

	//--------Initialization Functions------//

	//Initializes on Host and CpyToDevice
	void Initialize(){

		// Initialize trivial host arrays
		for(int i = 0; i<LX; i++)
			for(int j = 0; j<LY; j++)
			{
				int ij = i*LY+j;
				h_sigma[ij] = STRESS_THRESHOLD;
				h_state[ij] = 0;
			}
		h_state[0]=1;
		h_state[LX/2*LY+LY/2]=1;

		// Initialize host array for propagator
		//TODO: Check this problematic array
		for(int i = 0; i<LX/2+1; i++)
			for(int j = 0; j<(LY/2+1); j++)
			{

			REAL qx=2.0*M_PI*double(i)/LX;
			REAL qy=2.0*M_PI*double(j)/LY;
			REAL q2=qx*qx+qy*qy;

			if(i!=0 || j!=0) h_propagator[i*(LY/2+1)+j] = -4*qx*qx*qy*qy/(q2*q2);
			if(i!=0) h_propagator[(LX-i)*(LY/2+1)+j] = h_propagator[i*(LY/2+1)+j];
			}
		h_propagator[0] =-1.0; // amplitude of the plastic event

/*		for (int i=0; i<LX/2+1; i++) 
			for (int j=0; j<LY/2+1; j++) 
			{
				REAL q_i=2.0*M_PI*REAL(i)/LX;
				REAL q_j=2.0*M_PI*REAL(j)/LY;
				//REAL q_squared=pow(q_i,2)+pow(q_j,2);
				REAL q_squared = q_i*q_i + q_j*q_j;
				//REAL q_squared = -2.0*(cosf(q_i)+cosf(q_j)-2.0);
				//if(q_squared != 0) h_propagator[j*LX+i] = -4*pow(q_i,2)*pow(q_j,2)/pow(q_squared,2);
				if(i!=0 && j!=0) h_propagator[j*LX+i] = -4*q_i*q_i*q_j*q_j/(q_squared*q_squared);
				if(i>0) h_propagator[j*LX+(LX-i)] = h_propagator[j*LX+i];
				if(j>0) h_propagator[(LY-j)*LX+i] = h_propagator[j*LX+i];
				if(i>0 && j>0) h_propagator[(LY-j)*LX+(LX-i)] = h_propagator[j*LX+i];
			}
		h_propagator[0] = -1.0;
*/
/*
		// My guess
		int nx=LX; int ny=(LY/2+1);
		int nxs2=(nx/2); int nys2=(ny/2);
		for (int i=0; i<nx; i++) 
			for (int j=0; j<ny; j++) 
			{
			float qx=2*M_PI/(float)LX*((i+nxs2)-nx);
			float qy=2*M_PI/(float)LY*((j+nys2)-ny);
			int index = ((i+nxs2)%nx)*ny + ((j+nys2)%ny); 
			REAL qsquared = qx*qx+qy*qy;
			if(i!=0 && j!=0) h_propagator[index] = -4*qx*qx*qy*qy/(qsquared*qsquared);
			}
		h_propagator[0] = -1.0;
*/

		size_t size_b = NN * sizeof(bool);
		size_t size_r = NN * sizeof(REAL);
		size_t size_rc = LX*(LY/2+1) * sizeof(REAL);

		cudaMemcpy(d_sigma, h_sigma, size_r ,cudaMemcpyHostToDevice);
		cudaMemcpy(d_state, h_state, size_b ,cudaMemcpyHostToDevice);
		cudaMemcpy(d_propagator, h_propagator, size_rc, cudaMemcpyHostToDevice);
	}

	void InitializeOnDevice(){

		// Initialize State
		thrust::device_ptr<bool> state_ptr (d_state);
		thrust::device_vector<bool> tmp(NN, 0);
		tmp[0]=1;
		tmp[LX/2*LY+LY/2]=1; //(random() & 0x1u);
		thrust::copy(tmp.begin(),tmp.end(),state_ptr);

		// Initialize Stress
		thrust::device_ptr<REAL> sigma_ptr (d_sigma);
		thrust::fill(sigma_ptr, sigma_ptr+NN, STRESS_THRESHOLD);

		// Initialize Propagator
		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(LX/TILE_X/2+1, LY/TILE_Y/2+1);
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);
		Kernel_SetPropagator<<<dimGrid, dimBlock>>>(d_propagator,LX/2+1,LY/2+1);
		//TODO: Try to fit propagator in constant or texture memory
		//CUDA_SAFE_CALL(cudaBindTexture(NULL, propagatorTex, d_propagator, LX*(LY/2+1)*sizeof(REAL)));

		// Initialize Oseen Tensor and Tracers
		#ifdef TRACERS
		Kernel_SetOseenTensor<<<dimGrid, dimBlock>>>(d_oseen_x,d_oseen_y,LX/2+1,LY/2+1);
		//TODO: Bind Oseen tensor to texture
		Kernel_InitTracers<<<NTRACERS/TILE, TILE>>>(d_tracers, NTRACERS);
		#endif
	}


	//-----------Update Functions---------//

	void UpdateStateVariables(int time){
		#ifdef PHILOX
		//PhiloxPair
		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(LX/2/TILE_X, LY/TILE_Y);
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);
		Kernel_UpdateStateVariables<<<dimGrid, dimBlock>>>(d_sigma, d_state, time);
		#else
		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(LX/TILE_X, LY/TILE_Y);
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);
		rand48.generate(NN);
		random_d=(int*)rand48.get_random_numbers();
		Kernel_UpdateStateVariables<<<dimGrid, dimBlock>>>(d_sigma, d_state, random_d);
		#endif

		//thrust::device_ptr<REAL> sigma_ptr (d_sigma);
		//REAL sy = thrust::reduce(sigma_ptr, sigma_ptr + LY) / (REAL) LY;
		//cout << time << " " << sy << endl;
	}

	void UpdateStateVariablesActivated(int time){
		#ifdef PHILOX
		//PhiloxPair
		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(LX/2/TILE_X, LY/TILE_Y);
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);
		Kernel_UpdateStateVariablesActivated<<<dimGrid, dimBlock>>>(d_sigma, d_state, time);
		#else
		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(LX/TILE_X, LY/TILE_Y);
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);
		rand48.generate(NN);
		random_d=(int*)rand48.get_random_numbers();
		Kernel_UpdateStateVariablesActivated<<<dimGrid, dimBlock>>>(d_sigma, d_state, random_d);
		#endif
	}

	#ifdef TRACERS
	void CalculateDisplacementField(){
		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(LX/TILE_X, LY/TILE_Y/2+1);
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);
		
		Kernel_CalculateDisplacementField<<<dimGrid, dimBlock>>>(d_deltau_x, d_deltau_y, d_oseen_x, d_oseen_y, d_epsilon_dot, SCALE);
	}

	void UpdateTracers(){
		dim3 dimBlock(TILE);
		dim3 dimGrid(NTRACERS/TILE);
		assert(dimBlock.x<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID);
		Kernel_UpdateTracersPositions<<<dimGrid, dimBlock>>>(d_deltau_x_r, d_deltau_y_r, d_tracers, TIME_STEP);
	}
	#endif

	void ComputePlasticStrain(){

		thrust::device_ptr<REAL> sigma_ptr (d_sigma);
		thrust::device_ptr<bool> state_ptr (d_state);
		thrust::device_ptr<REAL> epsilon_ptr (d_epsilon_dot_r);

		//TODO: This can be a transform by key or something takig advantage of the bool
		// epsilon_dot <- sigma * state
		thrust::transform(sigma_ptr, sigma_ptr+NN, state_ptr, epsilon_ptr, thrust::multiplies<REAL>());

		//There is a factor of 1/2 missing in the calculation of epsilon_dot, that would cancell with the factor \
		of 2 that is missing in the calculus of the d_dsigma later on. Everyone happy.
	}

	void TransformToFourierSpace(){
		CUFFT_SAFE_CALL(cufftExecR2C(plan_r2c, d_epsilon_dot_r, d_epsilon_dot));
	}

	void Convolution(){
		//TODO: Do this with Thrust.
	/*	thrust::device_ptr<COMPLEX> epsilon_ptr (d_epsilon_dot);
		thrust::device_ptr<REAL> propagator_ptr (d_propagator);
		thrust::device_ptr<COMPLEX> state_ptr (d_dsigma);
		//AND NOW? SAXPY with complex type???

		thrust::transform(epsilon_ptr, epsilon_ptr+NN, propagator_ptr, state_ptr, 
				SCALE * _1 * _2); // placeholder expression
	*/
		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(LX/TILE_X, LY/TILE_Y/2+1);
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);

		Kernel_ComplexPointwiseMulAndScale<<<dimGrid, dimBlock>>>(d_epsilon_dot, d_propagator, d_dsigma, SCALE);
	}

	void AntitransformFromFourierSpace(){
		CUFFT_SAFE_CALL(cufftExecC2R(plan_c2r, d_dsigma, d_dsigma_r));
		#ifdef TRACERS
		CUFFT_SAFE_CALL(cufftExecC2R(plan_c2r, d_deltau_x, d_deltau_x_r));
		CUFFT_SAFE_CALL(cufftExecC2R(plan_c2r, d_deltau_y, d_deltau_y_r));
		#endif
	}
/*
	void Normalize(void){
		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(LX/TILE_X, LY/TILE_Y);
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);
		Kernel_Real2RealScaled<<<dimGrid, dimBlock>>>(d_dsigma_r, SCALE);
	}
*/

	void EulerIntegrationStep(float shear_rate){
		//TODO: If the single other term  in the equation of motion besides the convolution is the shear rate, we \
		can perform Euler directly in fourier, applying gamma to the 0 mode, and then antitransform the new sigma.
		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(LX/TILE_X, LY/TILE_Y);
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);

		Kernel_EulerIntegrationStep<<<dimGrid, dimBlock>>>(d_sigma, d_dsigma_r, shear_rate, TIME_STEP);
	}


	#ifdef ACTIVATED_DYNAMICS
	int CheckForPlasticActivity(){
		thrust::device_ptr<bool> state_ptr (d_state);
		thrust::device_ptr<bool> iter;
		iter = thrust::find(state_ptr, state_ptr+NN,1);
		int position = iter - state_ptr;
		return (position);
	}

	REAL ConstructActivatedPorbabilityTowerChooseOneEventAndUpdate(int time){

		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(LX/TILE_X, LY/TILE_Y);
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);
		Kernel_ConstructActivatedPorbabilityTower<<<dimGrid, dimBlock>>>(d_tower, d_sigma);

		thrust::device_ptr<REAL> tower_ptr (d_tower);

		//REAL totalprobability = thrust::reduce(tower_ptr,tower_ptr+NN);
		thrust::inclusive_scan(tower_ptr,tower_ptr+NN,tower_ptr); //in-place inclusive scan

		//CPY to recover the total sum
		REAL totalprobability;
		CUDA_SAFE_CALL(cudaMemcpy(&totalprobability, &d_tower[NN-1], sizeof(REAL), cudaMemcpyDeviceToHost));

		//thrust::transform(d_tower.begin(),d_tower.end(), \
				thrust::make_constant_iterator(totalprobability), \
				d_tower.begin(),thrust::divides<REAL>());
		//Istead doing the later we will multiply by totalsum the random number

		thrust::device_ptr<REAL> chosen = thrust::lower_bound(tower_ptr,tower_ptr+NN,(random()/RAND_MAX)*totalprobability);
		Kernel_UpdateChosenSite<<<1,1>>>(d_state, chosen-tower_ptr);
		REAL timeincrement = totalprobability/((-1.)*log((REAL)random()/RAND_MAX));

		return timeincrement;
	}
	#endif

	//-----------Calculate Functions---------//

	REAL CalculateSigmaAv(){
		thrust::device_ptr<REAL> sigma_ptr (d_sigma);
		REAL sigma_av = thrust::reduce(sigma_ptr, sigma_ptr+NN);

		return sigma_av;
	}

	#ifdef TRACERS
	void SetTracersAtTW(){
		CUDA_SAFE_CALL(cudaMemcpy(d_tracerstw, d_tracers, sizeof(COMPLEX)*NTRACERS, cudaMemcpyDeviceToDevice));
	}

	void CalculateTrajectoriesStructureFactor(){

		//thrust::device_vector<COMPLEX> d_structure_factor[NTRACERS];
		//COMPLEX *structure_factor_ptr = thrust::raw_pointer_cast(d_structure_factor.data());

		//grid/block N threads
		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(LX/TILE_X/2+1, LY/TILE_Y/2+1);
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);
		Kernel_CalculateTrajectoriesSructureFactor<<<dimGrid, dimBlock>>>(d_tracers, d_tracerstw, \
					d_structure_factor, LX/2+1, LY/2+1);
		//return thrust::reduce(d_structure_factor.begin(),d_structure_factor.end());
	}

	void ComputeStructureFactor(){
		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(LX/TILE_X, LY/TILE_Y/2+1);
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);
		CUDAkernel_ComputeStructureFactor<<<dimGrid, dimBlock>>>(d_tracers, d_tracerstw, d_Sq2d, LX, (LY/2+1));
	}

	void ComputeStructureFactorImage(){
		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(LX/TILE_X, LY/TILE_Y);
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);
		CUDAkernel_ComputeStructureFactorImage<<<dimGrid, dimBlock>>>(d_Sq2d, d_Sq2dimage, LX, LY);
		CUDA_SAFE_CALL(cudaMemcpy(h_Sq2dimage, d_Sq2dimage, sizeof(cufftReal)*LX*LY, cudaMemcpyDeviceToHost));
	}
	#endif

	#ifdef CORRELATION
	//void SaveSigmatw(){
	//	cudaMemcpy(d_sigma_tw, d_sigma, sizeof(REAL)*NN ,cudaMemcpyDeviceToDevice);
	//}

	REAL ComputeSsstress(){
		thrust::device_ptr<REAL> sigma_ptr (d_sigma);
		return thrust::reduce(sigma_ptr,sigma_ptr+NN)/(REAL)NN;
	}

	REAL ComputeFluctuationsAtTw(const REAL gamma_p, const REAL ssstress){

		thrust::device_ptr<REAL> sigma_ptr (d_sigma);
		thrust::device_ptr<REAL> fluct_tw_ptr (d_fluctuations_tw);

		thrust::transform(sigma_ptr, sigma_ptr+NN, thrust::make_constant_iterator(ssstress), \
				fluct_tw_ptr, thrust::minus<REAL>());

		//return thrust::reduce(fluct_tw_ptr,fluct_tw_ptr+NN)/REAL(NN);
		//return ro_zero_square_sum
		return thrust::transform_reduce(fluct_tw_ptr,fluct_tw_ptr+NN, _1*_1, 0.0f, thrust::plus<REAL>());
	}


	REAL ComputeAutoCorrelationFunction(const REAL gamma_p, const REAL ssstress, const REAL ro_zero2){

		thrust::device_vector<REAL> partial_corr(NN);
//		thrust::device_vector<REAL> partial_corrtw(NN);

		thrust::device_ptr<REAL> sigma_ptr (d_sigma);
		thrust::device_ptr<REAL> fluct_tw_ptr (d_fluctuations_tw);

		//const REAL ssstress1 = SSSTRESS(gamma_p);

		//TODO:Make transform iterator
		thrust::transform(sigma_ptr, sigma_ptr+NN, thrust::make_constant_iterator(ssstress), \
				partial_corr.begin(), thrust::minus<REAL>());

//		thrust::transform(sigmatw_ptr, sigmatw_ptr+NN, thrust::make_constant_iterator(ssstress), \
				partial_corrtw.begin(), thrust::minus<REAL>());

		thrust::transform(partial_corr.begin(), partial_corr.end(), fluct_tw_ptr, \
				partial_corr.begin(), thrust::multiplies<REAL>());

		thrust::transform(partial_corr.begin(), partial_corr.end(), \
				thrust::make_constant_iterator(1.0/(ro_zero2)), \
				partial_corr.begin(), thrust::multiplies<REAL>());

		return thrust::reduce(partial_corr.begin(),partial_corr.end());
	}
	#endif


	#ifdef VISUALIZATION
	void Visualization(float gamma_p){
		CpyDeviceToHost();

		char filename[200];
		sprintf(filename, "framesigma%5f.ppm", gamma_p);
		PrintPicture(filename,0);
		sprintf(filename, "framestate%5f.ppm", gamma_p);
		PrintPicture(filename,1);

		char cmd[200];
		sprintf(cmd, "convert framesigma%5f.ppm framestate%5f.ppm +append frame_Sigma-State%5f.ppm", gamma_p, gamma_p, gamma_p);
		system(cmd);
		sprintf(cmd, "rm framesigma%5f.ppm framestate%5f.ppm", gamma_p, gamma_p);
		system(cmd);
	}
	#endif

	//-----------Copy Functions---------//

	/* transfer from GPU to CPU memory */
	void CpyDeviceToHost(){
		size_t size_b = NN * sizeof(bool);
		size_t size_r = NN * sizeof(REAL);
		CUDA_SAFE_CALL(cudaMemcpy(h_state, d_state, size_b, cudaMemcpyDeviceToHost));
		CUDA_SAFE_CALL(cudaMemcpy(h_sigma, d_sigma, size_r, cudaMemcpyDeviceToHost));
	}

	//-----------Print Functions---------//

	void PrintState(ofstream &fstr){
		for(int i=0;i<LX;i++){
			for(int j=0;j<LY;j++){
				int k=i*LY+j;
				fstr << h_state[k] << " ";
			}
			fstr << endl;
		}
		fstr << endl;
	};

	void PrintSigma(ofstream &fstr){
		for(int i=0;i<LX;i++){
			for(int j=0;j<LY;j++){
				int k=i*LY+j;
				fstr << h_sigma[k] << " ";
			}
			fstr << endl;
		}
		fstr << endl;
	};

	void PrintPicture(char *picturename, int who){
		if (who==0) writePPMbinaryImage(picturename, h_sigma);
		if (who==1) writePPMbinaryImage_bool(picturename, h_state);
		//char cmd[200];
		//sprintf(cmd, "convert frame%d.ppm frame%d.jpg", 100000000+x, 100000000+x);
		//system(cmd);
		//sprintf(cmd, "rm frame%d.ppm", 100000000+x);
		//system(cmd);
	};

	void PrintResults(){
		string outputFile = "sigma.dat";
		ofstream out(outputFile.c_str());
		if(out)
		{
			for (int j=0; j<LX; j++) 
			{
			  for (int i=0; i<LX; i++) 
			    out << h_sigma[j*LX+i] << "\t";
			  out << endl;
			}
		}
		else
		cerr<<"ERROR!"<<endl;

		string outputFile2 = "state.dat";
		ofstream out2(outputFile2.c_str());
		if(out2)
		{
			for (int j=0; j<LX; j++) 
			{
			  for (int i=0; i<LX; i++) 
			    out2 << h_state[j*LX+i] << "\t";
			  out2 << endl;
			}
		}
		else
		cerr<<"ERROR!"<<endl;
	}

	#ifdef TRACERS
	void PrintTracers(){
		CUDA_SAFE_CALL(cudaMemcpy(h_tracers, d_tracers, NTRACERS*sizeof(COMPLEX), cudaMemcpyDeviceToHost));
		//cout << h_tracers[0].x << " " << h_tracers[0].y << " " << 0.0 << " " << 0.0;
		for(int i=0;i<NTRACERS;i++){
				cout << h_tracers[i].x << " " << h_tracers[i].y << " " ;
			//	<< (h_tracers[i].x-h_tracers[i-1].x) << " " << h_tracers[i].y-h_tracers[i-1].y;
		}
		cout << endl;
	}

	void PrintStructureFactor(ofstream &fstr, REAL t, REAL t0){
		CUDA_SAFE_CALL(cudaMemcpy(h_structure_factor, d_structure_factor, sizeof(COMPLEX)*(LX)*(LY/2+1), cudaMemcpyDeviceToHost));

/*		for(unsigned int k=0;k<(LX)*(LY/2+1);k++)
		{
		//fstr << 2*sin(M_PI*k/(double) LL) << "  " << h_structure_factor[i].x << "  " << h_structure_factor[i].y << endl;
		fstr << k << "  " << h_structure_factor[k].x << "  " << h_structure_factor[k].y << endl;
		}
		fstr << endl;
*/
		for(unsigned int i=0;i<LX/2+1;i++){
			for(unsigned int j=0;j<LY/2+1;j++){
				REAL qx=2.0*M_PI*double(i)/LX;
				REAL qy=2.0*M_PI*double(j)/LY;
				REAL q2=qx*qx+qy*qy;
				//REAL q2=2.f*(2.f-cosf(qx)+cosf(qy));
				unsigned int k=i*(LY/2+1)+j;
				fstr << q2*(t-t0) << "  " << h_structure_factor[k].x << "  " << h_structure_factor[k].y << " " << k << endl;
			}
		}
		fstr << endl;
	}

	void PrintSmallQStructureFactor(ofstream &fstr, REAL t){
		thrust::host_vector<COMPLEX> sf(3);
		//thrust::host_ptr<COMPLEX> sf_ptr (sf);
		CUDA_SAFE_CALL(cudaMemcpy(&sf[0], d_structure_factor, sizeof(COMPLEX)*2, cudaMemcpyDeviceToHost));

		REAL qx=2.0*M_PI/LX;
		REAL qy=2.0*M_PI/LY;
		REAL q2=qx*qx+qy*qy;
		fstr << t << "  " << sf[1].x << "  " << sf[1].y << " " << q2 << endl;
	}

	void PrintStructureFactorHistogram(ofstream &fstr, REAL t){
		CUDA_SAFE_CALL(cudaMemcpy(h_sf_distr, d_sf_distr, sizeof(REAL)*NOFBINS, cudaMemcpyDeviceToHost));

		for(unsigned int i=0;i<NOFBINS;i++){
			REAL q2 = i*QMAX/(REAL)NOFBINS;
			fstr << q2 << "  " << h_sf_distr[i] << endl;
		}
		fstr << endl;
	}

	void PrintImageStructureFactor(char *picturename, REAL x){
		char cmd[200];
		writePPMbinaryImage(picturename, h_Sq2dimage);
		sprintf(cmd, "convert Sq%.2f.ppm -resize %dx%d\\! Sq%.2f.ppm", 100000000+x, LX, LX, 100000000+x);
		system(cmd);
		#ifdef JPEG
		sprintf(cmd, "convert Sq%.2f.ppm Sq%.2f.jpg", 100000000+x, 100000000+x);
		system(cmd);
		sprintf(cmd, "rm Sq%.2f.ppm", 100000000+x);
		system(cmd);
		#endif
	}


	#endif

	~mesoplastic_model(){
		/* frees CPU memory */
		free(h_state);
		free(h_sigma);
		free(h_propagator);
		#ifdef TRACERS
		free(h_tracers);
		#endif

		/* frees GPU memory */
		cudaFree(d_state);
		cudaFree(d_sigma);
		cudaFree(d_epsilon_dot); cudaFree(d_epsilon_dot_r);
		cudaFree(d_dsigma); cudaFree(d_dsigma_r);
		cudaFree(d_propagator);
		#ifdef TRACERS
		cudaFree(d_deltau_x);
		cudaFree(d_deltau_y);
		cudaFree(d_deltau_x_r);
		cudaFree(d_deltau_y_r);
		cudaFree(d_oseen_x);
		cudaFree(d_oseen_y);
		cudaFree(d_tracers);
		#endif

		cufftDestroy(plan_r2c);
		cufftDestroy(plan_c2r);
	};
};


#endif /*  MESOPLASTIC_LIBRARY_CUH */
